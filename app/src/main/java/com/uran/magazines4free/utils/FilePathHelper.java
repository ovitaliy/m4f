package com.uran.magazines4free.utils;

import android.content.Context;
import android.os.Environment;

import java.io.File;

/**
 * Created by ovitali on 20.05.2015.
 * Project is Magazines4Free
 */
public class FilePathHelper {

    /**
     * Returns folder to store application cache. Currently its in Environment folder.
     *
     * @param context application context
     * @return cache folder
     */
    private static File getFolder(Context context) {
        File file = new File(Environment.getExternalStorageDirectory(), "magazines/cache");
        if (!file.exists() && !file.mkdirs()) {
            throw new RuntimeException("Folder cannot be create for path " + file.getAbsolutePath());
        }
        return file;
    }

    /**
     *
     * @param context application context
     * @param magazineId magazine id
     * @return pdf file
     */
    public static File getPdfFilePath(Context context, int magazineId) {
        String name = String.format("%d.pdf", magazineId);
        return new File(getFolder(context), name);
    }

}
