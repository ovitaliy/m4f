package com.uran.magazines4free.utils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by ovitali on 10.04.2015.
 * Project is Magazines4Free
 */
public class UiUtil {

    public static void setTextValue(View v, int viewId, int value) {
        String sValue = value > 0 ? String.valueOf(value) : "";
        setTextValue(v, viewId, sValue);
    }

    public static void setTextValue(View v, int viewId, String value) {
        TextView view = (TextView) v.findViewById(viewId);
        view.setText(value);
    }

    public static String getTextValue(View v, int viewId) {
        TextView view = (TextView) v.findViewById(viewId);
        return view.getText().toString().trim();
    }

    public static int getIntValue(View v, int viewId) {
        try {
            return Integer.parseInt(getTextValue(v, viewId));
        } catch (Exception ex) {
            return 0;
        }
    }

    public static void removeMeFromParent(View view){
        ViewGroup parent = (ViewGroup) view.getParent();
        parent.removeView(view);
    }

    public static void hideKeyboard(EditText target){
        InputMethodManager imm = (InputMethodManager) target.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(target.getWindowToken(), 0);
    }
}
