package com.uran.magazines4free.listeners;

import android.content.Context;
import android.widget.Spinner;
import android.widget.Toast;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.uran.magazines4free.R;
import com.uran.magazines4free.adapter.CountryAdapter;
import com.uran.magazines4free.model.Country;

/**
 * Created by ovitali on 10.04.2015.
 * Project is Magazines4Free
 */
public final class CountryRequestListener implements RequestListener<Country.CountryList> {

    private Spinner mSpinner;
    private Context mContext;

    public CountryRequestListener(Context context, Spinner spinner) {
        mContext = context;
        mSpinner = spinner;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        Toast.makeText(mContext, "failure", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestSuccess(Country.CountryList countries) {
        Country country = new Country(-1, mContext.getString(R.string.country));
        countries.add(0, country);
        mSpinner.setAdapter(new CountryAdapter(mContext, countries));
    }
}