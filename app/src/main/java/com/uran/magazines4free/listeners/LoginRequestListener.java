package com.uran.magazines4free.listeners;

import com.uran.magazines4free.MagazineApplication;
import com.uran.magazines4free.api.requests.BaseRequestListener;
import com.uran.magazines4free.api.responses.LoginResponse;
import com.uran.magazines4free.model.AppUser;

/**
 * Created by ovitali on 14.04.2015.
 * Project is Magazines4Free
 */
public class LoginRequestListener extends BaseRequestListener<LoginResponse> {

    @Override
    public void onRequestSuccess(LoginResponse loginResponse) {
        MagazineApplication.SESSION = loginResponse.getSessionName() + "=" + loginResponse.getSessionId();
        MagazineApplication.AUTH_TOKEN = loginResponse.getToken();

        AppUser.setToken(MagazineApplication.AUTH_TOKEN);
        AppUser.setSession(MagazineApplication.SESSION);
    }
}
