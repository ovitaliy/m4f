package com.uran.magazines4free.dataSource.loaders;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.uran.magazines4free.dataSource.DatabaseManager;
import com.uran.magazines4free.model.Interest;

import java.sql.SQLException;

/**
 * Created by ovitali on 18.02.2015.
 */
public class InterestsDataSource extends BaseLoader<Interest> {

    public InterestsDataSource(Context context) {
        super(context);
    }

    @Override
    public Dao<Interest, Integer> getDao() throws SQLException {
        return DatabaseManager.getInstance().getHelper().getInterestDao();
    }
}
