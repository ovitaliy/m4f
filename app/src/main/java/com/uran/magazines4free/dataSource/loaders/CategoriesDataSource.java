package com.uran.magazines4free.dataSource.loaders;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.uran.magazines4free.dataSource.DatabaseManager;
import com.uran.magazines4free.model.Category;

import java.sql.SQLException;

/**
 * Created by ovitali on 18.02.2015.
 */
public class CategoriesDataSource extends BaseLoader<Category> {

    public CategoriesDataSource(Context context) {
        super(context);
    }

    @Override
    public Dao<Category, Integer> getDao() throws SQLException {
        return DatabaseManager.getInstance().getHelper().getCategoryDao();
    }
}
