package com.uran.magazines4free.dataSource;

import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.uran.magazines4free.model.Category;

/**
 * Created by ovitali on 11.03.2015.
 */
public class ContentProvider extends android.content.ContentProvider {

    public static String AUTHORITY = "com.uran.magazines4free.db.provider";
    public static Uri URI = Uri.parse("content://" + AUTHORITY);

    public static final UriMatcher URI_MATCHER;
    static {
        int id = 1;
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(AUTHORITY, Category.TABLE_NAME, id++);
        URI_MATCHER.addURI(AUTHORITY, Category.TABLE_NAME+"/#", id++);

    }

    private DatabaseHelper helper;

    @Override
    public boolean onCreate() {
        helper = DatabaseManager.getInstance().getHelper();

        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        queryBuilder.setTables(getTableName(uri));


        return null;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    private String getTableName(Uri uri) {
        String tableName;
        switch (URI_MATCHER.match(uri)) {
            case Category.CODE_ALL:
            case Category.CODE_ID:
                tableName = Category.TABLE_NAME;
                break;
            default:
                throw new IllegalArgumentException("Can't recognize uri!");
        }

        return tableName;
    }
}
