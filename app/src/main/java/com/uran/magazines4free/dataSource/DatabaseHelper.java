package com.uran.magazines4free.dataSource;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.uran.magazines4free.dataSource.dao.DaoCategory;
import com.uran.magazines4free.dataSource.dao.DaoInterest;
import com.uran.magazines4free.dataSource.dao.DaoLanguage;
import com.uran.magazines4free.model.Category;
import com.uran.magazines4free.model.Interest;
import com.uran.magazines4free.model.Language;

import java.sql.SQLException;

/**
 * Created by ovitali on 27.03.2015.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "magazines4free.db";
    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = 6;

    // the DAO object we use to access the Book table
    private DaoCategory categoryDao = null;
    private DaoInterest interestDao = null;
    private DaoLanguage languageDao = null;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * This is called when the database is first created. Usually you should call createTable statements here to create
     * the tables that will store your data.
     */
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, Category.class);
            TableUtils.createTable(connectionSource, Interest.class);
            TableUtils.createTable(connectionSource, Language.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }


    }

    /**
     * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
     * the various data to match the new version number.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion,
                          int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, Interest.class, true);
            TableUtils.dropTable(connectionSource, Category.class, true);
            TableUtils.dropTable(connectionSource, Language.class, true);
            // after we drop the old databases, we create the new ones
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    public DaoCategory getCategoryDao() throws SQLException {
        if (categoryDao == null) {
            categoryDao = new DaoCategory(getConnectionSource());
        }
        return categoryDao;
    }

    public DaoLanguage getLanguageDao() throws SQLException {
        if (languageDao == null) {
            languageDao = new DaoLanguage(getConnectionSource());
        }
        return languageDao;
    }

    public DaoInterest getInterestDao() throws SQLException {
        if (interestDao == null) {
            interestDao = new DaoInterest(getConnectionSource());
        }
        return interestDao;
    }

    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
        super.close();
        categoryDao = null;
    }
}
