package com.uran.magazines4free.dataSource.dao;

import com.j256.ormlite.support.ConnectionSource;
import com.uran.magazines4free.model.Interest;

import java.sql.SQLException;

/**
 * Created by ovitali on 31.03.2015.
 * Project is Magazines4Free
 */
public class DaoInterest extends BaseDao<Interest> {

    public DaoInterest(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, Interest.class);
    }
}
