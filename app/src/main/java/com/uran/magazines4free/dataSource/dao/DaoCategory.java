package com.uran.magazines4free.dataSource.dao;

import com.j256.ormlite.support.ConnectionSource;
import com.uran.magazines4free.model.Category;

import java.sql.SQLException;

/**
 * Created by ovitali on 31.03.2015.
 * Project is Magazines4Free
 */
public class DaoCategory extends BaseDao<Category> {

    public DaoCategory(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, Category.class);
    }




}
