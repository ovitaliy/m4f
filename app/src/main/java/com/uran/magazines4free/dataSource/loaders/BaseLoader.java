package com.uran.magazines4free.dataSource.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by ovitali on 20.04.2015.
 * Project is Magazines4Free
 */
public abstract class BaseLoader<T> extends AsyncTaskLoader<List<T>> {

    PreparedQuery<T> mPreparedQuery;

    public BaseLoader(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public List<T> loadInBackground() {
        try {
            if (mPreparedQuery != null) {
                return getDao().query(mPreparedQuery);
            } else {
                Dao<T, Integer> dao = getDao();
                return dao.queryForAll();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public QueryBuilder<T, Integer> getQueryBuilder() {
        try {
            return getDao().queryBuilder();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void setPreparedQuery(PreparedQuery<T> query) {
        mPreparedQuery = query;
    }

    public abstract Dao<T, Integer> getDao() throws SQLException;


}
