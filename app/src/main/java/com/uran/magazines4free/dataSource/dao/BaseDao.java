package com.uran.magazines4free.dataSource.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.support.ConnectionSource;
import com.uran.magazines4free.MagazineApplication;

import java.lang.reflect.Array;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by ovitali on 23.04.2015.
 * Project is Magazines4Free
 */
public abstract class BaseDao<T> extends BaseDaoImpl<T, Integer> {

    protected BaseDao(ConnectionSource source, Class<T> dataClass) throws SQLException {
        super(source, dataClass);
    }

    public void updateAsync(List<T> list) {
        int n = list.size();
        @SuppressWarnings("unchecked") T[] array = (T[]) Array.newInstance(dataClass, n);
        array = list.toArray(array);

        updateAsync(array);

    }

    @SuppressWarnings("unchecked")
    public void updateAsync(final T... items) {

        final Callable<Void> transactionCall = new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                try {
                    for (T item : items) {
                        update(item);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return null;
            }
        };

        MagazineApplication.execute(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            TransactionManager.callInTransaction(getConnectionSource(), transactionCall);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );
    }

}
