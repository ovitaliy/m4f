package com.uran.magazines4free.widgets.popup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.uran.magazines4free.R;
import com.uran.magazines4free.utils.ConverterUtil;

/**
 * Created by ovitali on 24.04.2015.
 * Project is Magazines4Free
 */
public class SharePopup {


    public static PopupWindow create(Context context, View anchorView, View.OnClickListener clickListener) {
        LinearLayout view = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.popup_share, null);

        view.findViewById(R.id.share_fb).setOnClickListener(clickListener);
        view.findViewById(R.id.share_tw).setOnClickListener(clickListener);
        view.findViewById(R.id.share_email).setOnClickListener(clickListener);
        view.findViewById(R.id.share_gp).setOnClickListener(clickListener);
        view.findViewById(R.id.share_clipboard).setOnClickListener(clickListener);

        float dy = (context.getResources().getDimension(R.dimen.button_square_size)
                + ConverterUtil.dpToPix(context, 5)) * view.getChildCount();

        int[] position = new int[2];
        anchorView.getLocationInWindow(position);
        //y
        position[1] = (int) (position[1] - dy);

        return PopupHelper.createPopup(position[0], position[1], view, anchorView);
    }


}
