package com.uran.magazines4free.widgets.indicators;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by ovitali on 02.03.2015.
 */
public class ViewPagerIndicator extends BaseIndicatorBar {

    private int mPreviousValue = 0;

    public ViewPagerIndicator(Context context) {
        super(context);
    }

    public ViewPagerIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);

        ((ImageView) getChildAt(mPreviousValue)).setImageResource(checked);
    }

    public void setValue(int value) {
        if (mPreviousValue == value) {
            return;
        }
        ((ImageView) getChildAt(mPreviousValue)).setImageResource(unchecked);
        ((ImageView) getChildAt(value)).setImageResource(checked);

        mPreviousValue = value;
    }
}
