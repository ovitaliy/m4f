package com.uran.magazines4free.widgets.indicators;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.uran.magazines4free.R;

public abstract class BaseIndicatorBar extends LinearLayout implements View.OnClickListener {

    protected int checked;
    protected int unchecked;

    protected int value;

    protected int maximum = 5;

    public BaseIndicatorBar(Context context) {
        this(context, null);
    }

    public BaseIndicatorBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        setGravity(Gravity.CENTER);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.IndicatorBar);

        checked = a.getResourceId(R.styleable.IndicatorBar_checked, R.drawable.ic_rating_checked);
        unchecked = a.getResourceId(R.styleable.IndicatorBar_unchecked, R.drawable.ic_rating_unchecked);

        boolean selectable = a.getBoolean(R.styleable.IndicatorBar_selectable, false);

        int size = (int) a.getDimension(R.styleable.IndicatorBar_size, -2);

        if (a.hasValue(R.styleable.IndicatorBar_maximum)) {
            maximum = a.getInt(R.styleable.IndicatorBar_maximum, maximum);

        }
        LayoutParams layoutParams = new LayoutParams(size, size);

        for (int i = 0; i < maximum; i++) {
            ImageView image = new ImageView(context);
            image.setImageResource(unchecked);
            image.setLayoutParams(layoutParams);

            if (selectable) {
                image.setOnClickListener(this);
            }

            addView(image);
        }

        if (a.hasValue(R.styleable.IndicatorBar_rating)) {
            int rating = a.getInt(R.styleable.IndicatorBar_rating, 0);
            setValue(rating);
        }

        a.recycle();
    }

    public abstract void setValue(int value);

    public int getValue() {
        return value;
    }

    @Override
    public void onClick(View v) {
        for (int i = 0; i < maximum; i++) {
            if (getChildAt(i).equals(v)) {
                setValue(i+1);
                return;
            }

        }
    }
}
