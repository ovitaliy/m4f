package com.uran.magazines4free.widgets.items;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.uran.magazines4free.MagazineApplication;
import com.uran.magazines4free.R;
import com.uran.magazines4free.model.Magazine;
import com.uran.magazines4free.utils.ConverterUtil;
import com.uran.magazines4free.widgets.indicators.RatingBar;

/**
 * Created by ovitali on 09.04.2015.
 * Project is Magazines4Free
 */
public class MagazineView extends LinearLayout {
    public static final int LIMIT = 5;

    public static int ITEM_WIDTH = 0;
    public static int ITEM_PADDING = 0;
    public static int ITEM_IMAGE_HEIGHT = 0;
    public static int ITEM_STARS_HEIGHT = 0;

    static {
        ITEM_PADDING = (int) ConverterUtil.dpToPix(MagazineApplication.getInstance(), 10);
        int itemWidth = (MagazineApplication.SCREEN_WIDTH - (ITEM_PADDING * (LIMIT + 1)))
                / (LIMIT + 2);

        ITEM_WIDTH = itemWidth;
        ITEM_IMAGE_HEIGHT = (int) (ITEM_WIDTH * 1.336);
        ITEM_STARS_HEIGHT = (int) ConverterUtil.dpToPix(MagazineApplication.getInstance(), 40);
    }

    private ImageView mImageView;
    private RatingBar mRating;

    private int mPosition;
    private int mId;

    public MagazineView(Context context) {
        super(context);

        setGravity(Gravity.CENTER);
        setOrientation(VERTICAL);

        inflate(context, R.layout.item_magazine, this);

        mImageView = (ImageView) findViewById(R.id.thumb);
        mImageView.getLayoutParams().width = ITEM_WIDTH;
        mImageView.getLayoutParams().height = ITEM_IMAGE_HEIGHT;
        mRating = (RatingBar) findViewById(R.id.rating);
    }

    @Override
    public void setLayoutParams(ViewGroup.LayoutParams params) {
        params.width = ITEM_WIDTH;
        params.height = ITEM_STARS_HEIGHT + ITEM_IMAGE_HEIGHT;
        super.setLayoutParams(params);
    }

    public void setData(int position, Magazine magazine) {
        mPosition = position;
        mId = magazine.getId();
        ImageLoader.getInstance().displayImage(magazine.getPreviewImageUrl(), mImageView);
        mRating.setValue(magazine.getRating().getRating());
    }

    public void changeRatingBarVisibility(boolean visible) {
        mRating.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    public int getPosition() {
        return mPosition;
    }

    public int getId() {
        return mId;
    }
}
