package com.uran.magazines4free.widgets.lists;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.Scroller;
import android.widget.TextView;

import com.uran.magazines4free.MagazineApplication;
import com.uran.magazines4free.R;
import com.uran.magazines4free.adapter.MagazinesAdapter;
import com.uran.magazines4free.model.Magazine;
import com.uran.magazines4free.widgets.items.MagazineView;

import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by ovitali on 06.04.2015.
 * Project is Magazines4Free
 */
public class HistoricHorizontalListView extends BaseHorizontalListView implements View.OnClickListener, View.OnTouchListener {

    public HistoricHorizontalListView(Context context) {
        this(context, null);
    }

    private View mViewAllButton;
    private View mViewAllSmallButton;

    private TreeMap<Integer, MagazineView> mViewsMap;

    private Scroller mScroller;
    private GestureDetector mGestureDetector;

    private MagazineView mSelectedItem = null;
    private float mTouchDownX;

    private RelativeLayout mHolder;

    private MagazinesAdapter mAdapter;

    public HistoricHorizontalListView(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context, R.layout.widget_historic_horizontal_list, this);

        if (isInEditMode()) {
            return;
        }

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HistoricHorizontalListView);

        Drawable headerItemDrawable = a.getDrawable(R.styleable.HistoricHorizontalListView_header_image);
        if (headerItemDrawable == null) {
            headerItemDrawable = ContextCompat.getDrawable(context, R.drawable.hl_category);
        }
        String headerText = a.getString(R.styleable.HistoricHorizontalListView_header_text);
        boolean showSmallButton = a.getBoolean(R.styleable.HistoricHorizontalListView_header_show_bootom, false);
        setUpViewAllButton(headerItemDrawable, headerText, showSmallButton);

        a.recycle();


        mViewsMap = new TreeMap<>();

        mScroller = new Scroller(context, new DecelerateInterpolator());
        mGestureDetector = new GestureDetector(getContext(), getGestureListener());

        mHolder = (RelativeLayout) findViewById(R.id.holder);
        mHolder.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!mGestureDetector.onTouchEvent(event)) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            requestDisallowInterceptTouchEvent(true);
                            mTouchDownX = event.getX();
                            break;


                        case MotionEvent.ACTION_MOVE:
                            if (Math.abs(mTouchDownX - event.getX()) > MagazineView.ITEM_WIDTH * .1) {
                                mSelectedItem = null;
                            }
                            break;

                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:
                            requestDisallowInterceptTouchEvent(false);
                            if (mSelectedItem != null) {
                                selectItem(mSelectedItem);
                            } else if (mScroller.isFinished()) {
                                post(new FlyingRunnable());
                            }
                            break;
                    }
                }

                return true;
            }
        });

        mViewAllButton = findViewById(R.id.view_all);
        mViewAllButton.getLayoutParams().width = MagazineView.ITEM_WIDTH;
        mViewAllButton.getLayoutParams().height = MagazineView.ITEM_IMAGE_HEIGHT;

        mViewAllSmallButton = findViewById(R.id.view_all_small);
        mViewAllSmallButton.getLayoutParams().width = (int) (MagazineView.ITEM_WIDTH * .25);
        mViewAllSmallButton.getLayoutParams().height = MagazineView.ITEM_IMAGE_HEIGHT;
        mViewAllSmallButton.setVisibility(GONE);

        mLeftButton = findViewById(R.id.btn_left);
        mLeftButton.getLayoutParams().height = MagazineView.ITEM_IMAGE_HEIGHT;

        mRightButton = findViewById(R.id.btn_right);
        mRightButton.getLayoutParams().height = MagazineView.ITEM_IMAGE_HEIGHT;

        mLeftButton.setOnClickListener(this);
        mRightButton.setOnClickListener(this);

        // hide items
        mViewAllButton.setVisibility(GONE);
        mRightButton.setVisibility(GONE);
        mLeftButton.setVisibility(GONE);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            getLayoutParams().height = MagazineView.ITEM_IMAGE_HEIGHT
                    + MagazineView.ITEM_STARS_HEIGHT
                    + getPaddingTop() + getPaddingBottom();
        } else {
            getLayoutParams().height = 300;
        }
    }

    private void selectItem(MagazineView v) {
        if (onItemClickListener != null) {
            onItemClickListener.onItemClick(mAdapter.getItem(v.getPosition()));
        }
    }

    @Override
    protected void init() {
        if (initialized)
            return;

        if (mAdapter == null) return;
        initialized = true;
    }

    public void addItems(Magazine[] newItems) {
        int currentCount = getItemsCount();

        if (mAdapter == null)
            mAdapter = new MagazinesAdapter(getContext());

        mAdapter.add(newItems);
        if (newItems.length != 0 && currentCount == 0) {
            addItem(0);
            moveItems();

            // show items
            mViewAllButton.setVisibility(VISIBLE);

            removeView(findViewById(R.id.progress_bar_container));
        }

        mScroller.setFinalX(getMaxScrollValue());
    }

    @Override
    public int getPagesCount() {
        return (int) Math.ceil(getItemsCount() / MagazineView.LIMIT);
    }

    @Override
    public int getItemsCount() {
        if (mAdapter == null)
            return 0;

        return mAdapter.getCount();
    }

    @Override
    public void onClick(View v) {
        int delta = MagazineView.ITEM_WIDTH + MagazineView.ITEM_PADDING;
        switch (v.getId()) {
            case R.id.btn_left:
                delta = -delta;
                break;
        }
        delta = Math.max(-mScroller.getCurrX(), delta);
        delta = Math.min(getMaxScrollValue() - mScroller.getCurrX(), delta);

        if (delta != 0) {
            mScroller.startScroll(mScroller.getCurrX(), 0, delta, 0, 330);
            post(new FlyingRunnable());
        }
    }

    private GestureDetector.SimpleOnGestureListener getGestureListener() {
        return new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                mScroller.forceFinished(true);
                mSelectedItem = null;
                mScroller.computeScrollOffset();
                mScroller.fling(mScroller.getCurrX(), 0, -(int) velocityX, 0,
                        0, getMaxScrollValue(), 0, 0);
                post(new FlyingRunnable());
                return true;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                mScroller.forceFinished(true);
                mSelectedItem = null;
                mScroller.startScroll(mScroller.getCurrX(), 0, (int) distanceX, 0, 0);
                if (mScroller.computeScrollOffset()) {
                    moveItems();
                }

                return true;
            }
        };
    }

    public void moveItems() {
        int scrollerX = mScroller.getCurrX();

        if (mScroller.getCurrX() > 0) {
            mLeftButton.setVisibility(View.VISIBLE);
            mViewAllSmallButton.setVisibility(View.VISIBLE);
            mViewAllButton.setVisibility(View.INVISIBLE);
        } else {
            mLeftButton.setVisibility(View.GONE);
            mViewAllSmallButton.setVisibility(View.INVISIBLE);
            mViewAllButton.setVisibility(View.VISIBLE);
        }

        if (scrollerX < getMaxScrollValue() - MagazineView.ITEM_WIDTH) {
            mRightButton.setVisibility(VISIBLE);
        } else {
            mRightButton.setVisibility(GONE);
        }


        List<MagazineView> toRemoveItems = new LinkedList<>();
        for (int i = 0; i < mHolder.getChildCount(); i++) {
            MagazineView v = (MagazineView) mHolder.getChildAt(i);
            float oldX = v.getX();
            float x = getItemComputedX(v.getPosition(), scrollerX);
            if (x < -MagazineView.ITEM_WIDTH || x > MagazineApplication.SCREEN_WIDTH) {
                toRemoveItems.add(v);
            } else {
                v.setX(x);

                if (oldX > MagazineView.ITEM_WIDTH && x < MagazineView.ITEM_WIDTH && mHolder.indexOfChild(v) != 0) {
                    mHolder.removeView(v);
                    mHolder.addView(v, 0);
                } else if (oldX < MagazineApplication.SCREEN_WIDTH - MagazineView.ITEM_WIDTH * 2 && x > MagazineApplication.SCREEN_WIDTH - MagazineView.ITEM_WIDTH * 2 && mHolder.indexOfChild(v) != 0) {
                    mHolder.removeView(v);
                    mHolder.addView(v, 0);
                }
            }
        }

        for (MagazineView m : toRemoveItems) {
            mViewsMap.remove(m.getPosition());
            mHolder.removeView(m);
            mViewCache.add(m);
        }

        int firstItemPosition = mViewsMap.firstKey();
        while (firstItemPosition >= 0 && (getItemComputedX(firstItemPosition, scrollerX)) >= -MagazineView.ITEM_WIDTH) {
            addItem(firstItemPosition);
            firstItemPosition--;
        }

        int lastItemPosition = mViewsMap.lastKey();
        while (lastItemPosition < getItemsCount() && (getItemComputedX(lastItemPosition, scrollerX)) < MagazineApplication.SCREEN_WIDTH) {
            addItem(lastItemPosition);
            lastItemPosition++;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        mSelectedItem = (MagazineView) v;
        return false;
    }

    public class FlyingRunnable implements Runnable {
        @Override
        public void run() {
            if (mScroller.computeScrollOffset()) {
                moveItems();
                postDelayed(this, 30);
            } else {
                validateScrollPosition(this);
            }
        }
    }

    private void validateScrollPosition(FlyingRunnable flyingRunnable) {
        int scrollX = mScroller.getCurrX();
        int delta;

        if (scrollX < 0)
            delta = scrollX;
        else if (scrollX > getMaxScrollValue())
            delta = scrollX - getMaxScrollValue();
        else delta = scrollX % (MagazineView.ITEM_WIDTH + MagazineView.ITEM_PADDING);

        if (delta != 0) {
            mScroller.startScroll(scrollX, 0, -delta, 0, 1000);
            postDelayed(flyingRunnable, 30);
        } else {
            Log.i("validateScrollPosition", "scrollX: " + scrollX + " : " + getMaxScrollValue());
        }
    }

    private int getMaxScrollValue() {
        int val = (int) (((getItemsCount() + 1) * MagazineView.ITEM_WIDTH
                + (getItemsCount()) * MagazineView.ITEM_PADDING)
                - MagazineApplication.SCREEN_WIDTH + MagazineView.ITEM_WIDTH * 0.75);
        return Math.max(0, val);
    }

    private MagazineView addItem(int position) {
        MagazineView view;

        if (!mViewsMap.containsKey(position)) {
            view = mAdapter.getView(position, getCachedView(), mHolder);
            view.setOnTouchListener(this);
            mHolder.addView(view);
            mViewsMap.put(position, view);
        } else {
            view = mViewsMap.get(position);
            mHolder.removeView(view);
            mHolder.addView(view);
        }
        view.setX(getItemComputedX(position, mScroller.getCurrX()));

        return view;
    }


    private float getItemComputedX(int position, int scrollX) {
        float newX = (float) (MagazineView.ITEM_WIDTH * .75 + MagazineView.ITEM_PADDING + (position) * (MagazineView.ITEM_WIDTH + MagazineView.ITEM_PADDING) - scrollX);

        if (newX < -MagazineView.ITEM_WIDTH * .25) {
            newX = (float) ((newX + MagazineView.ITEM_WIDTH * .25) * .25 - MagazineView.ITEM_WIDTH * .25);
        }

        if (newX > MagazineApplication.SCREEN_WIDTH - MagazineView.ITEM_WIDTH) {
            newX = (float) ((newX - MagazineApplication.SCREEN_WIDTH + MagazineView.ITEM_WIDTH) * .25 + MagazineApplication.SCREEN_WIDTH - MagazineView.ITEM_WIDTH);
        }
        return newX;
    }

    public List<Magazine> getAll() {
        return mAdapter.getAll();
    }

    public void setUpViewAllButton(Drawable headerItemDrawable, String headerText, boolean showSmallButton) {
        TextView headerItem = (TextView) findViewById(R.id.header_item_big);
        headerItem.setText(headerText);
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            headerItem.setBackgroundDrawable(headerItemDrawable);
        } else {
            headerItem.setBackground(headerItemDrawable);
        }
        findViewById(R.id.header_item_small).setVisibility(showSmallButton ? VISIBLE : GONE);
    }


}