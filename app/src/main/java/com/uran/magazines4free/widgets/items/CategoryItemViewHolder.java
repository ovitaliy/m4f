package com.uran.magazines4free.widgets.items;

import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.uran.magazines4free.R;
import com.uran.magazines4free.model.Category;

/**
 * Created by ovitali on 22.05.2015.
 * Project is Magazines4Free
 */
public class CategoryItemViewHolder extends RecyclerView.ViewHolder {

    private TextView mLabel;
    private ImageView mIcon;
    private ImageView mCheckbox;

    private View mView;

    private Category mData;

    private CategoryItemViewHolder(View view) {
        super(view);
        mView = view;
        mLabel = (TextView) view.findViewById(R.id.label);
        mIcon = (ImageView) view.findViewById(R.id.icon);
        mCheckbox = (ImageView) view.findViewById(R.id.checkbox);
    }

    public View getView() {
        return mView;
    }

    public void select(boolean value) {
        mLabel.setSelected(value);
        mCheckbox.setVisibility(value ? View.VISIBLE : View.GONE);
    }

    public View setData(Category data) {
        mData = data;
        boolean selected = data.isSelected();
        mLabel.setSelected(selected);
        mLabel.setText(data.getName());
        mCheckbox.setVisibility(selected ? View.VISIBLE : View.GONE);

        ImageLoader.getInstance().displayImage(data.getIcon(), mIcon);

        return mView;
    }

    public Category getData() {
        return mData;
    }

    public static CategoryItemViewHolder create(LayoutInflater inflater, ViewGroup parent) {
        View v = inflater.inflate(R.layout.item_category, parent, false);

        CategoryItemViewHolder viewHolder = new CategoryItemViewHolder(v);
        v.setTag(viewHolder);

        return viewHolder;
    }

    public static CategoryItemViewHolder createSmall(LayoutInflater inflater, ViewGroup parent) {
        CategoryItemViewHolder viewHolder = create(inflater, parent);
        viewHolder.mLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, viewHolder.mLabel.getTextSize() * 0.75f);
        viewHolder.mIcon.getLayoutParams().width *= 0.75;
        viewHolder.mIcon.getLayoutParams().height *= 0.75;

        return viewHolder;
    }
}
