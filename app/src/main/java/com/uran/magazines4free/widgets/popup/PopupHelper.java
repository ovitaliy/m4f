package com.uran.magazines4free.widgets.popup;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.uran.magazines4free.R;
import com.uran.magazines4free.behaviour.OnRateListener;
import com.uran.magazines4free.utils.ConverterUtil;
import com.uran.magazines4free.widgets.indicators.RatingBar;

/**
 * Created by ovitali on 24.04.2015.
 * Project is Magazines4Free
 */
public class PopupHelper {

    public static PopupWindow showRatePopup(final Context context, final View anchorView, final OnRateListener onRateListener) {
        final View view = View.inflate(context, R.layout.popup_rating, null);


        final float dy = context.getResources().getDimension(R.dimen.button_square_size)
                + ConverterUtil.dpToPix(context, 5);

        final int[] position = new int[2];
        anchorView.getLocationInWindow(position);
        //y
        position[1] = (int) (position[1] - dy);

        view.findViewById(R.id.rate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RatingBar ratingBar = (RatingBar) view.findViewById(R.id.rating_bar);
                int value = ratingBar.getValue();

                onRateListener.onRate(value);

            }
        });

        return createPopup(position[0], position[1], view, anchorView);
    }

    public static PopupWindow createPopup(final int x, final int y, final View view, final View anchorView) {
        return createPopup(x, y, view, anchorView, true);
    }

    public static PopupWindow createPopup(final int x, final int y, final View view, final View anchorView, boolean isSelectable) {


        final int gravity = Gravity.TOP | Gravity.START;

        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        anchorView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });

        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());

        popupWindow.showAtLocation(anchorView, gravity, x, y);


        if (isSelectable) {
            anchorView.setSelected(true);
            popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    anchorView.setSelected(false);
                }
            });
        }


        return popupWindow;
    }

}
