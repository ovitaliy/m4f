package com.uran.magazines4free.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;

import com.uran.magazines4free.R;

/**
 * Created by ovitali on 31.03.2015.
 * Project is Magazines4Free
 */
public class HLinedTextView extends FontTextView {

    private int mLineColor;
    private float mLineWidth;
    private float mLinePadding;

    private Paint mLinePaint;

    private boolean isUpperCased;

    private Rect mTextBounds = new Rect();

    public HLinedTextView(Context context) {
        super(context);
    }

    public HLinedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Localized);
        mLineColor = a.getColor(R.styleable.Localized_lineColor, getResources().getColor(R.color.gray));
        mLineWidth = a.getDimension(R.styleable.Localized_lineWidth, 2);
        mLinePadding = a.getDimension(R.styleable.Localized_linePadding, 20);
        isUpperCased = a.getBoolean(R.styleable.Localized_upperCase, false);

        if (isUpperCased) {
            setText(getText());
        }


        mLinePaint = new Paint();
        mLinePaint.setStrokeWidth(mLineWidth);
        mLinePaint.setColor(mLineColor);
        mLinePaint.setAntiAlias(true);
        mLinePaint.setStyle(Paint.Style.STROKE);
        mLinePaint.setStrokeJoin(Paint.Join.ROUND);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (canvas == null) {
            super.onDraw(canvas);
            return;
        }

        int w = canvas.getWidth();
        if (w <= 0)
            return;

        int h = canvas.getHeight();

        String text = getText().toString();
        Paint textPaint = getPaint();
        textPaint.getTextBounds(text, 0, text.length(), mTextBounds);
        float y = h / 2 - mLineWidth;
        float dx = mTextBounds.width() / 2 + mLinePadding;

        canvas.drawLine(0, y, w / 2 - dx, y, mLinePaint);
        canvas.drawLine(w / 2 + dx, y, w, y, mLinePaint);

        super.onDraw(canvas);
    }


    @Override
    public void setText(CharSequence text, BufferType type) {
        if (isUpperCased)
            text = text.toString().toUpperCase();
        super.setText(text, type);
    }

}
