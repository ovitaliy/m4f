package com.uran.magazines4free.widgets.lists;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.uran.magazines4free.model.Magazine;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by ovitali on 07.04.2015.
 * Project is Magazines4Free
 */
public abstract class BaseHorizontalListView extends RelativeLayout {

    static final int DURATION = 500;

    int mCurrentPage;

    View mLeftButton;
    View mRightButton;

    Queue<View> mViewCache;

    boolean initialized;

    protected abstract void init();

    public abstract int getPagesCount();

    public abstract int getItemsCount();

    protected OnItemClickListener onItemClickListener;

    public BaseHorizontalListView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mViewCache = new LinkedList<>();
    }

    protected View getCachedView() {
        if (mViewCache.size() > 0) {
            return mViewCache.remove();
        }
        return null;
    }


    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    /**
     * Interface definition for a callback to be invoked when an item in this
     * BaseHorizontalListView has been clicked.
     */
    public interface OnItemClickListener {
        void onItemClick(Magazine magazine);
    }


}
