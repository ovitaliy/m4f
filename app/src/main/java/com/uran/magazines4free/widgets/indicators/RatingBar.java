package com.uran.magazines4free.widgets.indicators;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.uran.magazines4free.R;
import com.uran.magazines4free.model.Rating;
import com.uran.magazines4free.utils.ConverterUtil;
import com.uran.magazines4free.widgets.FontTextView;

public class RatingBar extends BaseIndicatorBar {

    private Rating mRating;

    public RatingBar(Context context) {
        super(context);
    }

    public RatingBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setValue(Rating value, boolean toShowViewsCount) {
        mRating = value;
        setValue(value.getRating());

        if (toShowViewsCount) {
            showViewsCount();
        }
    }

    private void showViewsCount() {
        while (getChildCount() > maximum)
            removeViewAt(maximum);
        addSubValue(getContext().getString(R.string.magazine_views, mRating.getVotes()));
    }

    public void setValue(int value) {
        this.value = value;
        for (int i = 0; i < maximum; i++) {
            ((ImageView) getChildAt(i)).setImageResource(i < value ? checked : unchecked);
        }
    }

    public void addSubValue(String value) {
        FontTextView textView = new FontTextView(getContext(), null);
        textView.setText(value);
        textView.setTextColor(getResources().getColor(R.color.edit_text_color));
        addView(textView);


        LinearLayout.MarginLayoutParams lp = (LinearLayout.MarginLayoutParams) textView.getLayoutParams();
        lp.setMargins((int) ConverterUtil.dpToPix(getContext(), 18), 0, 0, 0);
    }

}
