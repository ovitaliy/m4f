package com.uran.magazines4free.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.uran.magazines4free.R;
import com.uran.magazines4free.utils.TypefaceCache;

public class FontButton extends Button {
    public FontButton(Context context) {
        this(context, null);
    }

    public FontButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public FontButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        if (isInEditMode()) return;

        String fontName = "GillSans";
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Localized);
        if (a.hasValue(R.styleable.Localized_font)) {
            fontName = a.getString(R.styleable.Localized_font);
            Typeface typeface = TypefaceCache.getTypeface(context, fontName);
            setTypeface(typeface);
        } else {
            Typeface typeface = TypefaceCache.getTypeface(context, fontName);
            setTypeface(typeface);
        }
        a.recycle();
    }
}
