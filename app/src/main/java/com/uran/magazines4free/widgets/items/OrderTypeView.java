package com.uran.magazines4free.widgets.items;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.uran.magazines4free.R;
import com.uran.magazines4free.model.OrderType;
import com.uran.magazines4free.utils.ConverterUtil;
import com.uran.magazines4free.widgets.FontTextView;

/**
 * Created by ovitali on 25.05.2015.
 * Project is Magazines4Free
 */
public class OrderTypeView extends FontTextView {

    private String mType;

    public OrderTypeView(Context context) {
        this(context, null);
    }

    public OrderTypeView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setGravity(Gravity.CENTER_VERTICAL);
        setLayoutParams(new LinearLayout.LayoutParams((int) ConverterUtil.dpToPix(context, 300), ViewGroup.LayoutParams.WRAP_CONTENT));
        int p = (int) ConverterUtil.dpToPix(context, 10);
        setPadding(p, p, p, p);

        setTextColor(getResources().getColor(R.color.edit_text_color));

        setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
    }

    public void setData(OrderType.OrderItem orderItem) {
        setText(orderItem.getTitle());
        mType = orderItem.getType();

        setSelected(orderItem.isSelected());
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);

        setTypeface(selected ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);

        if (selected) {
            setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_checkbox, 0);
        } else {
            setCompoundDrawables(null, null, null, null);
        }
    }

    public String getType() {
        return mType;
    }
}
