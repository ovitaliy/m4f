package com.uran.magazines4free.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.widget.ImageButton;

import com.uran.magazines4free.R;

/**
 * Created by ovitali on 23.04.2015.
 * Project is Magazines4Free
 */
public class ColorImageButton extends ImageButton {

    private int mFilterColor;
    private int mSelectedColor;

    public ColorImageButton(Context context) {
        super(context);
    }

    public ColorImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ColorImageButton);
        mFilterColor = a.getColor(R.styleable.ColorImageButton_filterColor, Color.WHITE);
        mSelectedColor = a.getColor(R.styleable.ColorImageButton_filterSelectedColor, getResources().getColor(R.color.orange));
        a.recycle();

        setColorFilter(mFilterColor, PorterDuff.Mode.SRC_IN);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        if (selected)
            setColorFilter(mSelectedColor, PorterDuff.Mode.SRC_IN);
        else
            setColorFilter(mFilterColor, PorterDuff.Mode.SRC_IN);
    }
}
