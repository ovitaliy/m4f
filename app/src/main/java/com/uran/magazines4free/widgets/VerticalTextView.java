package com.uran.magazines4free.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

/**
 * Created by ovitali on 06.04.2015.
 * Project is Magazines4Free
 */
public class VerticalTextView extends FontTextView {

    public VerticalTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        //animate().rotation(270);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int w = (int) (getLayoutParams().width);
        int h = getLayoutParams().height;
        canvas.rotate(270);
        super.onDraw(canvas);
    }
}
