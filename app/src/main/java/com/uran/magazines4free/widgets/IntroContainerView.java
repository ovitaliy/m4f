package com.uran.magazines4free.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by ovitali on 03.03.2015.
 */
public class IntroContainerView extends LinearLayout {

    public IntroContainerView(Context context) {
        super(context);
    }

    public IntroContainerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        double size = Math.min(width, height);

       /* if (width > height) {*/
            width = (int) (size * 0.8d);
            height = (int) (size * 0.8d);
        /*}else{
            width = (int) (size * 0.6d);
            height = (int) (size * 0.8d);
        }*/

        super.onMeasure(
                MeasureSpec.makeMeasureSpec(width, MeasureSpec.getMode(widthMeasureSpec)),
                MeasureSpec.makeMeasureSpec(height, MeasureSpec.getMode(heightMeasureSpec))
        );
    }
}
