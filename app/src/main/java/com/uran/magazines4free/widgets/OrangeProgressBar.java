package com.uran.magazines4free.widgets;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.uran.magazines4free.R;

/**
 * Created by ovitali on 17.02.2015.
 */
public class OrangeProgressBar extends ProgressBar {
    public OrangeProgressBar(Context context) {
        super(context);
    }

    public OrangeProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OrangeProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        getIndeterminateDrawable().setColorFilter(getContext().getResources().getColor(R.color.orange), PorterDuff.Mode.SRC_IN);
    }
}
