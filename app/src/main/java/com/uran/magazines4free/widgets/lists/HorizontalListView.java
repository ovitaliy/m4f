package com.uran.magazines4free.widgets.lists;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.uran.magazines4free.MagazineApplication;
import com.uran.magazines4free.R;
import com.uran.magazines4free.model.Magazine;
import com.uran.magazines4free.utils.ConverterUtil;
import com.uran.magazines4free.widgets.items.MagazineView;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by ovitali on 02.04.2015.
 * Project is Magazines4Free
 */
public class HorizontalListView extends BaseHorizontalListView implements View.OnClickListener {

    static final int LIMIT = 5;

    private int itemContainerWidth;

    public HorizontalListView(Context context) {
        this(context, null);
    }

    private int mItemPadding;

    private RecyclerView mHolder;

    private ArrayList<Magazine> mMagazines;

    public HorizontalListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.widget_horizontal_list, this);

        mMagazines = new ArrayList<>();

        mItemPadding = (int) (MagazineApplication.SCREEN_WIDTH
                - MagazineView.ITEM_WIDTH * 5
                - ConverterUtil.dpToPix(getResources(), getResources().getDimension(R.dimen.search_dialog_horizontal_margin))) / 5;

        itemContainerWidth = MagazineView.ITEM_WIDTH * 5 + mItemPadding * 5;


        mHolder = (RecyclerView) findViewById(R.id.holder);
        mHolder.getLayoutParams().width = itemContainerWidth;
        mHolder.getLayoutParams().height = MagazineView.ITEM_IMAGE_HEIGHT + MagazineView.ITEM_STARS_HEIGHT;

        mHolder.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        mHolder.setAdapter(new Adapter());

        mHolder.addOnScrollListener(new RecyclerView.OnScrollListener() {

            float scrollX;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int dx = (int) (scrollX % (itemContainerWidth));
                    if (dx != 0) {
                        float p = scrollX / (float) (itemContainerWidth);
                        p = Math.round(p);
                        int d = (int) (itemContainerWidth * p - scrollX);
                        recyclerView.smoothScrollBy(d, 0);
                    } else {
                        publishOnPageChanged(scrollX);
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                scrollX += dx;
            }
        });


        mLeftButton = findViewById(R.id.btn_left);
        mRightButton = findViewById(R.id.btn_right);

        mLeftButton.setOnClickListener(this);
        mRightButton.setOnClickListener(this);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        getLayoutParams().height = MagazineView.ITEM_IMAGE_HEIGHT + MagazineView.ITEM_STARS_HEIGHT;

        if (!initialized) {
            init();
        }
    }

    private void publishOnPageChanged(float scrollX) {
        int currentPage = (int) (scrollX / itemContainerWidth);

        mLeftButton.setVisibility(currentPage > 0 ? View.VISIBLE : View.INVISIBLE);
        mRightButton.setVisibility(currentPage < getPagesCount() - 1 ? View.VISIBLE : View.INVISIBLE);
    }


    protected void init() {
        if (initialized)
            return;

        if (mHolder == null)
            return;

        initialized = true;
    }

    @Override
    public int getPagesCount() {
        return (int) Math.ceil(mMagazines.size() / 5f);
    }

    @Override
    public int getItemsCount() {
        return mMagazines.size();
    }


    @Override
    public void onClick(View v) {
        int dx = itemContainerWidth;
        switch (v.getId()) {
            case R.id.btn_left:
                dx *= -1;
            case R.id.btn_right:
                mHolder.smoothScrollBy(dx, 0);
                break;

            default:
                MagazineView magazineView = (MagazineView) v;
                onItemClickListener.onItemClick(mMagazines.get(magazineView.getPosition()));
                break;
        }


    }

    private class Adapter extends RecyclerView.Adapter<ViewHolder> {
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RelativeLayout holder = new RelativeLayout(getContext());

            holder.setLayoutParams(new RecyclerView.LayoutParams(itemContainerWidth, MagazineView.ITEM_IMAGE_HEIGHT + MagazineView.ITEM_STARS_HEIGHT));

            int x = mItemPadding / 2;
            int dx = MagazineView.ITEM_WIDTH + mItemPadding;

            MagazineView[] magazineViews = new MagazineView[5];

            for (int i = 0; i < 5; i++) {
                MagazineView magazineView = new MagazineView(getContext());
                magazineView.setOnClickListener(HorizontalListView.this);
                magazineView.setX(x);
                holder.addView(magazineView);

                magazineViews[i] = magazineView;

                x += dx;
            }

            return new ViewHolder(holder, magazineViews);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            int totalItemsCount = getItemsCount();

            for (int i = 0; i < 5; i++) {
                MagazineView magazineView = holder.mMagazines[i];
                int realPosition = position * 5 + i;
                if (realPosition < totalItemsCount) {
                    magazineView.setVisibility(View.VISIBLE);
                    magazineView.setData(realPosition, mMagazines.get(realPosition));
                } else {
                    magazineView.setVisibility(View.INVISIBLE);
                }
            }
        }

        @Override
        public int getItemCount() {
            return getPagesCount();
        }
    }


    private static class ViewHolder extends RecyclerView.ViewHolder {

        final MagazineView[] mMagazines;

        public ViewHolder(View itemView, MagazineView[] magazineViews) {
            super(itemView);
            mMagazines = magazineViews;
        }
    }

    public void addItems(Magazine[] newItems) {
        Collections.addAll(mMagazines, newItems);
        mHolder.getAdapter().notifyDataSetChanged();
    }

    public void setItems(Magazine[] newItems) {
        mMagazines.clear();
        mLeftButton.setVisibility(View.INVISIBLE);
        Collections.addAll(mMagazines, newItems);
        mHolder.getAdapter().notifyDataSetChanged();
    }
}
