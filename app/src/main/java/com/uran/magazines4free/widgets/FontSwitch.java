package com.uran.magazines4free.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Switch;

import com.uran.magazines4free.utils.TypefaceCache;

/**
 * Created by ovitali on 21.04.2015.
 * Project is Magazines4Free
 */
public class FontSwitch extends Switch {

    public FontSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (isInEditMode())
            return;

        String fontName = "GillSans";
        Typeface typeface = TypefaceCache.getTypeface(context, fontName);
        setTypeface(typeface);
    }
}
