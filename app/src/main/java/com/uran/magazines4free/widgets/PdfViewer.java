package com.uran.magazines4free.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.uran.magazines4free.MagazineApplication;
import com.uran.magazines4free.R;
import com.uran.magazines4free.utils.ConverterUtil;

import org.vudroid.pdfdroid.codec.PdfContext;
import org.vudroid.pdfdroid.codec.PdfDocument;
import org.vudroid.pdfdroid.codec.PdfPage;

import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ovitali on 20.05.2015.
 * Project is Magazines4Free
 */
public class PdfViewer extends RecyclerView {

    public PdfViewer(Context context) {
        this(context, null);
    }

    private Hashtable<ImageView, Integer> mLoadingPages;
    private PdfDocument mPdfDocument;
    private Handler mHandler;
    private PageLoader mPageLoader;
    private PublishRunnable mPublishRunnable;

    private ExecutorService mExecutorService;

    private int mPagesCount;

    private int mShowingPages = 2;

    private int mPageWidth;
    private int mPageHeight;

    private OnPageChangeListener mOnPageChangeListener;
    private int mLastPage = -1;

    private final Object mSyncFlag = new Object();

    public PdfViewer(Context context, AttributeSet attrs) {
        super(context, attrs);

        addOnScrollListener(new OnScrollListener() {

            float scrollX;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == SCROLL_STATE_IDLE) {
                    int dx = (int) (scrollX % (mPageWidth));
                    if (dx != 0) {
                        float p = scrollX / (float) (mPageWidth);
                        p = Math.round(p);
                        int d = (int) (mPageWidth * p - scrollX);
                        recyclerView.smoothScrollBy(d, 0);
                    } else {
                        publishOnPageChanged(scrollX);
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                scrollX += dx;
            }
        });
    }

    public void loadPdf(File file) {
        if (mOnPageChangeListener == null)
            throw new NullPointerException("OnPageChangeListener == null");

        String url = file.getAbsolutePath();
        mLoadingPages = new Hashtable<>(5);

        mPageLoader = new PageLoader();
        mPublishRunnable = new PublishRunnable();

        mHandler = getHandler();
        if (mHandler == null) {
            mHandler = new Handler();
        }
        new PdfLoader().execute(url);
    }

    private void invalidateSizes(int pageWidth, int pageHeight) {
        int height = getHeight();
        int width = getWidth();

        if (height == 0)
            height = (int) (MagazineApplication.SCREEN_HEIGHT - ConverterUtil.dpToPix(getContext(), 70));
        if (width == 0)
            width = MagazineApplication.SCREEN_WIDTH;

        int pagesWidth = pageWidth * mShowingPages;

        while (pagesWidth < width && pageHeight < height) {
            pagesWidth *= 1.1;
            pageHeight *= 1.1;
        }

        while (pagesWidth > width || pageHeight > height) {
            pagesWidth *= 0.95;
            pageHeight *= 0.95;
        }

        mPageWidth = pagesWidth / mShowingPages;
        mPageHeight = pageHeight;

        post(new Runnable() {
            @Override
            public void run() {
                getLayoutParams().width = mPageWidth * mShowingPages;
                getLayoutParams().height = mPageHeight;
            }
        });
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mLoadingPages.clear();
        if (mExecutorService != null)
            mExecutorService.shutdown();

        clearOnScrollListeners();
    }

    /**
     * Smooth scroll by given amount of pages
     *
     * @param page positive value scroll to next, negative otherwise
     */
    public void smoothScrollByPage(int page) {
        smoothScrollBy(page * mPageWidth, 0);
    }

    public void setOnPageChangeListener(OnPageChangeListener onPageChangeListner) {
        mOnPageChangeListener = onPageChangeListner;
    }

    private void publishOnPageChanged(float scrollX) {
        int currentPage = (int) (scrollX / mPageWidth);

        if (currentPage == mLastPage) return;

        boolean isLast = currentPage + mShowingPages >= mPagesCount;

        mOnPageChangeListener.onPageChanged(currentPage, mPagesCount, isLast);
        mLastPage = currentPage;

    }

    public class PdfLoader extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            try {
                mPdfDocument = (PdfDocument) new PdfContext().openDocument(params[0]);
                mPagesCount = mPdfDocument.getPageCount();

                PdfPage page = (PdfPage) mPdfDocument.getPage(0);
                invalidateSizes(page.getWidth(), page.getHeight());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (mPdfDocument != null && mPagesCount > 0) {
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                setLayoutManager(linearLayoutManager);
                setAdapter(new PdfViewer.Adapter());

                publishOnPageChanged(0);
            }
        }
    }


    private class Adapter extends RecyclerView.Adapter<PdfViewer.ViewHolder> {

        @Override
        public PdfViewer.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(getContext()).inflate(R.layout.item_reader, parent, false);

            LayoutParams layoutParams = new LayoutParams(mPageWidth, mPageHeight);
            relativeLayout.setLayoutParams(layoutParams);

            return new PdfViewer.ViewHolder(relativeLayout);
        }

        @Override
        public void onBindViewHolder(PdfViewer.ViewHolder holder, int position) {
            holder.image.setImageBitmap(null);
            loadPage(position, holder.image);
        }

        @Override
        public int getItemCount() {
            return mPagesCount;
        }
    }

    private ExecutorService getExecutor() {
        if (mExecutorService == null) {
            mExecutorService = Executors.newSingleThreadExecutor();
        }
        return mExecutorService;
    }

    void loadPage(int pageNumber, ImageView image) {
        mLoadingPages.put(image, pageNumber);
        getExecutor().submit(mPageLoader);
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;

        public ViewHolder(RelativeLayout layout) {
            super(layout);
            image = (ImageView) layout.findViewById(R.id.pageImage);
        }
    }


    private class PageLoader implements Runnable {
        @Override
        public void run() {
            while (!mLoadingPages.isEmpty()) {
                synchronized (mSyncFlag) {
                    Enumeration<ImageView> keys = mLoadingPages.keys();
                    ImageView image = keys.nextElement();
                    int pageNumber = mLoadingPages.remove(image);
                    PdfPage page = (PdfPage) mPdfDocument.getPage(pageNumber);
                    Bitmap bitmap = page.renderBitmap(page.getWidth(), page.getHeight(), new RectF(0, 0, 1, 1));

                    mHandler.post(mPublishRunnable.publish(bitmap, image));
                }
            }
        }
    }

    private class PublishRunnable implements Runnable {

        private Bitmap mBitmap;
        private ImageView mImageView;

        public PublishRunnable publish(Bitmap bitmap, ImageView imageView) {
            mBitmap = bitmap;
            mImageView = imageView;
            return this;
        }

        @Override
        public void run() {
            mImageView.setImageBitmap(mBitmap);
        }
    }

    public interface OnPageChangeListener {
        void onPageChanged(int currentPage, int countPage, boolean isLast);
    }

    @Override
    protected void finalize() throws Throwable {
        Log.i(PdfViewer.class.getSimpleName(), "finalize");
        super.finalize();

    }
}
