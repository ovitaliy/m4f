package com.uran.magazines4free.widgets.popup;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.uran.magazines4free.MagazineApplication;
import com.uran.magazines4free.R;
import com.uran.magazines4free.behaviour.OnOrderChangeListener;
import com.uran.magazines4free.model.OrderType;
import com.uran.magazines4free.utils.ConverterUtil;
import com.uran.magazines4free.widgets.items.OrderTypeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ovitali on 22.05.2015.
 * Project is Magazines4Free
 */
public class SelectOrderPopup {

    public static PopupWindow create(final Context context, final View anchorView, final OnOrderChangeListener onOrderChangeListener) {
        LinearLayout popupView = (LinearLayout) LinearLayout.inflate(context, R.layout.popup_select_order, null);

        OrderType.OrderItem[] items = OrderType.getOrdersList();

        final List<OrderTypeView> viewsList = new ArrayList<>(5);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderTypeView itemView = (OrderTypeView) v;

                for (OrderTypeView item : viewsList) {
                    if (!item.equals(itemView)) {
                        item.setSelected(false);
                    } else {
                        item.setSelected(true);
                        onOrderChangeListener.onOrderChanged(item.getType());
                    }
                }
            }
        };

        for (OrderType.OrderItem orderItem : items) {
            OrderTypeView itemView = new OrderTypeView(context);
            itemView.setData(orderItem);
            itemView.setOnClickListener(onClickListener);

            viewsList.add(itemView);
            popupView.addView(itemView);

        }

        final int[] position = new int[2];
        final float dy = ConverterUtil.dpToPix(context, 65);
        final float dx = ConverterUtil.dpToPix(context, 405);

        //x
        position[0] = (int) (MagazineApplication.SCREEN_WIDTH - dx);
        //y
        position[1] = (int) (dy);

        return PopupHelper.createPopup(position[0], position[1], popupView, anchorView, false);
    }

}
