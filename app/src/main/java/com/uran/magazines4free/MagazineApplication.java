package com.uran.magazines4free;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.octo.android.robospice.SpiceManager;
import com.uran.magazines4free.api.ApiSpiceService;
import com.uran.magazines4free.dataSource.DatabaseManager;
import com.uran.magazines4free.utils.PrefHelper;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.fabric.sdk.android.Fabric;

/**
 * Created by ovitali on 18.02.2015.
 */
public class MagazineApplication extends MultiDexApplication {

    public static String AUTH_TOKEN = null;
    public static String SESSION = null;

    public static int SCREEN_WIDTH = -1;
    public static int SCREEN_HEIGHT = -1;

    private static MagazineApplication instance;
    private static ExecutorService sExecutorService;

    private static SpiceManager sSpiceManager;

    public static MagazineApplication getInstance() {
        return instance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(cookieManager);

        instance = this;

        if (!BuildConfig.DEBUG)
            Fabric.with(this, new Crashlytics());

        PrefHelper.init(this);

        DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(displayImageOptions)
                .build();
        ImageLoader.getInstance().init(config);

        DatabaseManager.getInstance().init(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        DatabaseManager.getInstance().release();
    }

    private static ExecutorService getExecutorService() {
        if (sExecutorService == null) {
            sExecutorService = Executors.newFixedThreadPool(1);
        }
        return sExecutorService;
    }

    public static void execute(Runnable task) {
        getExecutorService().submit(task);
    }

    /**
     * returns SpiceManager not bound to any activity/fragment
     */
    public static SpiceManager getSpiceManager() {
        if (sSpiceManager == null) {
            sSpiceManager = new SpiceManager(ApiSpiceService.class);
            sSpiceManager.start(instance);
        }

        return sSpiceManager;
    }


}
