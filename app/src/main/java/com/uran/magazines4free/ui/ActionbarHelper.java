package com.uran.magazines4free.ui;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uran.magazines4free.R;
import com.uran.magazines4free.behaviour.OnMenuSelectionListener;
import com.uran.magazines4free.model.MenuItem;
import com.uran.magazines4free.utils.ConverterUtil;
import com.uran.magazines4free.widgets.FontTextView;

/**
 * Created by ovitali on 03.04.2015.
 * Project is Magazines4Free
 */
public class ActionbarHelper {

    private AppCompatActivity mActionBarActivity;
    private FrameLayout mRootView;
    private ImageView mLogoImage;
    private TextView mTitleView;

    private OnMenuSelectionListener mListener;

    public ActionbarHelper(AppCompatActivity actionBarActivity, Toolbar toolbar) {
        mActionBarActivity = actionBarActivity;

        if (actionBarActivity instanceof OnMenuSelectionListener)
            mListener = (OnMenuSelectionListener) actionBarActivity;

        mRootView = (FrameLayout) toolbar.findViewById(R.id.action_bar_root_view);
        mLogoImage = (ImageView) mRootView.findViewById(R.id.title_image);
        mTitleView = (TextView) mRootView.findViewById(R.id.title);
    }


    public void setTitle() {
        mLogoImage.setVisibility(View.VISIBLE);
        mTitleView.setVisibility(View.INVISIBLE);

        while (mRootView.getChildCount() > 2) {
            mRootView.removeViewAt(2);
        }
    }

    public void setTitle(int title) {
        setTitle(mActionBarActivity.getString(title));
    }

    public void setTitle(CharSequence title) {
        mTitleView.setText(title);
        mLogoImage.setVisibility(View.INVISIBLE);
        mTitleView.setVisibility(View.VISIBLE);

        while (mRootView.getChildCount() > 2) {
            mRootView.removeViewAt(2);
        }
    }

    public void setTitle(int selectedPosition, MenuItem... items) {
        while (mRootView.getChildCount() > 2) {
            mRootView.removeViewAt(2);
        }

        mLogoImage.setVisibility(View.INVISIBLE);
        mTitleView.setVisibility(View.INVISIBLE);


        LinearLayout toggleView = new LinearLayout(mActionBarActivity);
        toggleView.setOrientation(LinearLayout.HORIZONTAL);

        FrameLayout.LayoutParams toggleViewLayoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        toggleViewLayoutParams.gravity = Gravity.CENTER;

        toggleView.setLayoutParams(toggleViewLayoutParams);

        Resources resources = mActionBarActivity.getResources();

        int width = (int) ConverterUtil.dpToPix(mActionBarActivity, 150);
        int padding = (int) ConverterUtil.dpToPix(mActionBarActivity, 10);

        for (int i = 0; i < items.length; i++) {
            FontTextView toggleItem = new FontTextView(mActionBarActivity);
            toggleItem.setId(items[i].getId());
            toggleItem.setText(items[i].getTitleId());

            LinearLayout.LayoutParams itemLayoutParams = new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT);
            toggleItem.setLayoutParams(itemLayoutParams);
            toggleItem.setGravity(Gravity.CENTER);

            toggleItem.setPadding(padding, padding, padding, padding);

            toggleItem.setTextColor(resources.getColorStateList(R.color.toggle_text));

            if (i == 0) {
                toggleItem.setBackgroundResource(R.drawable.toggle_left_item);
            } else if (i == items.length - 1) {
                toggleItem.setBackgroundResource(R.drawable.toggle_right_item);
            } else {
                toggleItem.setBackgroundResource(R.drawable.toggle_center_item);
            }

            if (i == selectedPosition) {
                toggleItem.setSelected(true);
            }
            toggleItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onMenuSelected(v.getId());
                }
            });

            toggleView.addView(toggleItem);
        }

        mRootView.addView(toggleView);
    }

    public void changeSizes(int itemsCount) {
        float itemsWidth = ConverterUtil.dpToPix(mActionBarActivity, 48) * (itemsCount - 1);
        mRootView.setPadding((int) itemsWidth, 0, 0, 0);
    }
}
