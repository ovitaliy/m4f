package com.uran.magazines4free.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.uran.magazines4free.R;

/**
 * Created by ovitali on 19.02.2015.
 */

@Deprecated
public class ListHeaderHelper {

    public static View createHeader(Context context) {
        View header = LayoutInflater.from(context).inflate(R.layout.item_magazine, null);
        ((ImageView) header.findViewById(R.id.thumb)).setImageResource(R.drawable.item_header);
        header.findViewById(R.id.rating).setVisibility(View.INVISIBLE);
       /* AbsHListView.LayoutParams layoutParams = new AbsHListView.LayoutParams(R.dimen.item_magazine_image_width, R.dimen.item_magazine_image_height);
        header.setLayoutParams(layoutParams);
*/
        return header;
    }

}
