package com.uran.magazines4free.fragments.base;

import android.support.v4.app.Fragment;

import com.octo.android.robospice.SpiceManager;
import com.uran.magazines4free.api.ApiSpiceService;

/**
 * Created by ovitali on 12.03.2015.
 */
public abstract class BaseSpiceFragment extends Fragment {

    private SpiceManager spiceManager = new SpiceManager(ApiSpiceService.class);

    @Override
    public void onStart() {
        spiceManager.start(getActivity());
        super.onStart();
    }

    @Override
    public void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }
}
