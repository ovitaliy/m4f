package com.uran.magazines4free.fragments.auth;

import android.app.Activity;

import com.uran.magazines4free.behaviour.OnAuthListener;
import com.uran.magazines4free.fragments.base.BaseTitleFragment;

/**
 * Created by ovitali on 25.03.2015.
 */
public abstract class BaseAuthFragment extends BaseTitleFragment {

    protected OnAuthListener onAuthListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onAuthListener = (OnAuthListener)activity;
    }
}
