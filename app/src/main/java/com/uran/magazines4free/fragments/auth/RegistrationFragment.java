package com.uran.magazines4free.fragments.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.uran.magazines4free.MagazineApplication;
import com.uran.magazines4free.R;
import com.uran.magazines4free.api.requests.BaseRequestListener;
import com.uran.magazines4free.api.requests.CountriesRequest;
import com.uran.magazines4free.api.requests.LoginRequest;
import com.uran.magazines4free.api.requests.RegistrationRequest;
import com.uran.magazines4free.api.responses.RegistrationResponse;
import com.uran.magazines4free.dialogs.DialogUtil;
import com.uran.magazines4free.dialogs.ProgressDialog;
import com.uran.magazines4free.listeners.CountryRequestListener;
import com.uran.magazines4free.listeners.LoginRequestListener;
import com.uran.magazines4free.model.AppUser;
import com.uran.magazines4free.model.Country;

import java.util.List;

/**
 * Created by ovitali on 26.02.2015.
 */
public class RegistrationFragment extends BaseAuthFragment implements View.OnClickListener, Validator.ValidationListener {

    public static RegistrationFragment newInstance() {
        return new RegistrationFragment();
    }


    @NotEmpty
    private EditText mNickEditText;

    @NotEmpty
    @Email
    private EditText mEmailEditText;

    @Password(min = 6)
    private EditText mPasswordEditText;

    @ConfirmPassword
    private EditText mPasswordConfirmEditText;

    private Validator mValidator;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration, container, false);

        view.findViewById(R.id.register).setOnClickListener(this);

        mNickEditText = (EditText) view.findViewById(R.id.nickname);
        mEmailEditText = (EditText) view.findViewById(R.id.email);
        mPasswordEditText = (EditText) view.findViewById(R.id.password);
        mPasswordConfirmEditText = (EditText) view.findViewById(R.id.password_confirm);


        //init validator
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);

        getSpiceManager().execute(new CountriesRequest(), "CountriesRequest", DurationInMillis.ONE_HOUR, new CountryRequestListener(getActivity(), (Spinner) view.findViewById(R.id.country)));

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register:
                mValidator.validate();
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {
        View v = getView();

        RegistrationRequest registrationRequest = new RegistrationRequest(v);

        Spinner spinner = (Spinner) v.findViewById(R.id.country);
        if (spinner != null) {
            Country country = (Country) spinner.getSelectedItem();
            if (country != null && country.getId() != -1) {
                registrationRequest.setCountry(country.getId());
            }
        }


        AppUser appUser = new AppUser(mNickEditText.getText().toString(), mEmailEditText.getText().toString());

        getSpiceManager().execute(registrationRequest, new RegistrationResponseListener(appUser));

        DialogUtil.show(getActivity().getSupportFragmentManager(), new ProgressDialog());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void setTitle() {
        // we don't need title in this fragment
    }

    // ============================================================================================
    // INNER CLASSES
    // ============================================================================================
    public final class RegistrationResponseListener extends BaseRequestListener<RegistrationResponse> {

        private AppUser mAppUser;

        public RegistrationResponseListener(AppUser user) {
            mAppUser = user;
        }

        @Override
        public void onRequestSuccess(RegistrationResponse registrationResponse) {
            mAppUser.setUid(registrationResponse.getUid());

            LoginRequest loginRequest = new LoginRequest()
                    .setEmail(mEmailEditText.getText().toString())
                    .setPassword(mPasswordEditText.getText().toString());

            MagazineApplication.getSpiceManager().execute(loginRequest, new LoginRequestListener());

            onAuthListener.onAuthComplete(mAppUser);

            DialogUtil.cancel(getActivity().getSupportFragmentManager());
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            DialogUtil.cancel(getActivity().getSupportFragmentManager());
            super.onRequestFailure(spiceException);
        }
    }
}
