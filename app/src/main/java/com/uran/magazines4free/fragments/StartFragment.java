package com.uran.magazines4free.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.j256.ormlite.stmt.PreparedQuery;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.uran.magazines4free.Constants;
import com.uran.magazines4free.R;
import com.uran.magazines4free.api.requests.BaseRequestListener;
import com.uran.magazines4free.api.requests.GetMagazinesListRequest;
import com.uran.magazines4free.behaviour.DefaultOnSeekBarChangeListener;
import com.uran.magazines4free.behaviour.OnMagazineSelectListener;
import com.uran.magazines4free.behaviour.OnMenuSelectionListener;
import com.uran.magazines4free.dataSource.loaders.InterestsDataSource;
import com.uran.magazines4free.fragments.base.BaseMenuBindSpiceFragment;
import com.uran.magazines4free.model.AppUser;
import com.uran.magazines4free.model.Interest;
import com.uran.magazines4free.model.Magazine;
import com.uran.magazines4free.model.OrderType;
import com.uran.magazines4free.widgets.indicators.ViewPagerIndicator;
import com.uran.magazines4free.widgets.lists.BaseHorizontalListView;
import com.uran.magazines4free.widgets.lists.HistoricHorizontalListView;

import java.util.List;

public class StartFragment extends BaseMenuBindSpiceFragment implements
        BaseHorizontalListView.OnItemClickListener,
        View.OnClickListener,
        GridView.OnItemClickListener {

    public static StartFragment newInstance() {
        return new StartFragment();
    }

    private OnMagazineSelectListener mListener;

    private ViewPagerIndicator mViewPagerIndicator;

    private RelativeLayout mTextsContainer;
    private TextView[] mTexts;
    private int mGrayColor;
    private int mOrangeColor;
    private HistoricHorizontalListView mHorizontalListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start, container, false);

        mTextsContainer = (RelativeLayout) view.findViewById(R.id.text_container);

        SeekBar seekBar = (SeekBar) view.findViewById(R.id.seek_bar);
        seekBar.setOnSeekBarChangeListener(new DefaultOnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                setSeekBarValueChanged(progress);
            }
        });

        mHorizontalListView = (HistoricHorizontalListView) view.findViewById(R.id.horizontal_list_view);
        mHorizontalListView.setOnItemClickListener(this);

        getSpiceManager().execute(new GetMagazinesListRequest().setCategory(72).setOrder(OrderType.BEST),
                "CategoryName" + 72, DurationInMillis.ONE_MINUTE,
                new MagazineLoaderRequestListener());

        return view;
    }

    @Override
    protected int getMenuBindId() {
        return R.id.drawer_menu_item_start;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (AppUser.getAuthorized())
            getLoaderManager().initLoader(Constants.LOADERS.INTERESTS, Bundle.EMPTY, new InterestsLoaderCallbacks());
        else
            invalidateTopView(false);
    }

    /**
     * @param showViewPager if true shows view pager, otherwise shows layout with invite to set interests
     */
    private void invalidateTopView(boolean showViewPager) {
        View view = getView();
        if (view == null) {
            return;
        }
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        mViewPagerIndicator = (ViewPagerIndicator) view.findViewById(R.id.view_pager_indicator);

        View navigateToInterestContainer = view.findViewById(R.id.navigate_to_interests_container);

        if (showViewPager) {
            viewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager()));
            viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    super.onPageSelected(position);
                    mViewPagerIndicator.setValue(position);
                }
            });

            viewPager.setVisibility(View.VISIBLE);
            mViewPagerIndicator.setVisibility(View.VISIBLE);
            navigateToInterestContainer.setVisibility(View.GONE);
        } else {
            viewPager.setVisibility(View.GONE);
            mViewPagerIndicator.setVisibility(View.GONE);
            navigateToInterestContainer.setVisibility(View.VISIBLE);

            view.findViewById(R.id.drawer_menu_item_interests).setOnClickListener(this);
            view.findViewById(R.id.close).setOnClickListener(this);
        }
    }

    @Override
    protected void setTitle() {
        getTitledActivity().setTitle();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnMagazineSelectListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setSeekBarValueChanged(int value) {
        int index = 0;
        if (value > 5 && value <= 20) {
            index = 1;
        } else if (value > 20 && value <= 35) {
            index = 2;
        } else if (value > 35 && value <= 50) {
            index = 3;
        } else if (value > 50) {
            index = 4;
        }

        if (mTexts == null) {
            mTexts = new TextView[mTextsContainer.getChildCount()];
            for (int i = 0; i < mTextsContainer.getChildCount(); i++) {
                mTexts[i] = ((TextView) mTextsContainer.getChildAt(i));
            }
            mGrayColor = getResources().getColor(R.color.gray_text);
            mOrangeColor = getResources().getColor(R.color.orange);
        }

        for (int i = 0; i < mTextsContainer.getChildCount(); i++) {
            if (i != index)
                mTexts[i].setTextColor(mGrayColor);
        }
        mTexts[index].setTextColor(mOrangeColor);
    }

    @Override
    public void onItemClick(Magazine magazine) {
        mListener.onMagazineSelected(magazine);
    }

    @Override
    public void onItemClick(android.widget.AdapterView<?> parent, View view, int position, long id) {
        Object magazine = parent.getAdapter().getItem(position);
        mListener.onMagazineSelected((com.uran.magazines4free.model.Magazine) magazine);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawer_menu_item_interests:
                if (AppUser.getAuthorized())
                    ((OnMenuSelectionListener) getActivity()).onMenuSelected(R.id.drawer_menu_item_interests);
                else
                    ((OnMenuSelectionListener) getActivity()).onMenuSelected(R.id.drawer_menu_item_login);
                break;

            case R.id.close:
                View view = getView();
                if (view != null) {
                    View container = view.findViewById(R.id.navigate_to_interests_container);
                    ((ViewGroup) container.getParent()).removeView(container);
                }

                break;
        }
    }


    public class ViewPagerAdapter extends FragmentPagerAdapter {
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new AdItemFragment();
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    // ============================================================================================
    // INNER CLASSES
    // ============================================================================================
    private class InterestsLoaderCallbacks implements LoaderManager.LoaderCallbacks<List<Interest>> {
        @Override
        public Loader<List<Interest>> onCreateLoader(int id, Bundle args) {
            try {
                InterestsDataSource interestsDataSource = new InterestsDataSource(getActivity());
                PreparedQuery<Interest> query = interestsDataSource.getQueryBuilder().where().eq("selected", true).prepare();
                interestsDataSource.setPreparedQuery(query);

                return interestsDataSource;
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            return null;
        }

        @Override
        public void onLoadFinished(Loader<List<Interest>> loader, List<Interest> data) {
            invalidateTopView(data.size() > 0);
        }

        @Override
        public void onLoaderReset(Loader<List<Interest>> loader) {

        }
    }


    private final class MagazineLoaderRequestListener extends BaseRequestListener<Magazine[]> {
        @Override
        public void onRequestSuccess(Magazine[] magazines) {
            Log.d(this.toString(), magazines.toString());
            mHorizontalListView.addItems(magazines);
        }
    }
}
