package com.uran.magazines4free.fragments.intro;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uran.magazines4free.Constants;


public class IntroFragment extends Fragment {


    public static IntroFragment newInstance(int viewId) {
        IntroFragment fragment = new IntroFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.PARAMS.INTRO_VIEW_ID, viewId);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getArguments().getInt(Constants.PARAMS.INTRO_VIEW_ID), container, false);
        return view;
    }


}
