package com.uran.magazines4free.fragments.magazine;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.uran.magazines4free.Constants;
import com.uran.magazines4free.R;
import com.uran.magazines4free.adapter.MagazineCategoryAdapter;
import com.uran.magazines4free.api.requests.BaseRequestListener;
import com.uran.magazines4free.api.requests.GetMagazineRequest;
import com.uran.magazines4free.api.requests.GetMagazinesSameRequest;
import com.uran.magazines4free.api.requests.RateRequest;
import com.uran.magazines4free.behaviour.OnCategorySelectListener;
import com.uran.magazines4free.behaviour.OnMagazineSelectListener;
import com.uran.magazines4free.behaviour.OnRateListener;
import com.uran.magazines4free.dialogs.ErrorDialog;
import com.uran.magazines4free.model.AppUser;
import com.uran.magazines4free.model.Category;
import com.uran.magazines4free.model.Language;
import com.uran.magazines4free.model.Magazine;
import com.uran.magazines4free.model.Rating;
import com.uran.magazines4free.model.Tag;
import com.uran.magazines4free.utils.UiUtil;
import com.uran.magazines4free.widgets.indicators.RatingBar;
import com.uran.magazines4free.widgets.items.CategoryItemViewHolder;
import com.uran.magazines4free.widgets.lists.BaseHorizontalListView;
import com.uran.magazines4free.widgets.lists.HistoricHorizontalListView;
import com.uran.magazines4free.widgets.popup.PopupHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ViewMagazineFragment extends BaseMagazineFragment implements
        BaseHorizontalListView.OnItemClickListener,
        GridView.OnItemClickListener,
        OnRateListener {


    public static ViewMagazineFragment newInstance(int magazineId) {
        ViewMagazineFragment fragment = new ViewMagazineFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.PARAMS.MAGAZINE_ID, magazineId);
        fragment.setArguments(args);
        return fragment;
    }

    private int mMagazineId;
    private OnMagazineSelectListener mOnMagazineSelectListener;
    private OnCategorySelectListener mOnCategorySelectListener;

    private HistoricHorizontalListView mSameMagazinesListView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mMagazineId = getArguments().getInt(Constants.PARAMS.MAGAZINE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_view_magazine, container, false);

        view.findViewById(R.id.view).setOnClickListener(this);
        view.findViewById(R.id.magazine_image).setOnClickListener(this);
        view.findViewById(R.id.add_reading).setOnClickListener(this);
        view.findViewById(R.id.add_my_list).setOnClickListener(this);
        view.findViewById(R.id.download).setOnClickListener(this);
        view.findViewById(R.id.share).setOnClickListener(this);
        view.findViewById(R.id.rate).setOnClickListener(this);

        mSameMagazinesListView = (HistoricHorizontalListView) view.findViewById(R.id.horizontal_list_view);
        mSameMagazinesListView.setOnItemClickListener(this);

        if (savedInstanceState == null) {
            getSpiceManager().execute(new GetMagazineRequest(mMagazineId), "magazine_data" + mMagazineId, DurationInMillis.ONE_MINUTE, new MagazineResponseListener());
            getSpiceManager().execute(new GetMagazinesSameRequest(mMagazineId), "same" + mMagazineId, DurationInMillis.ONE_MINUTE, new MagazinesSameResponseListener());
        } else {
            magazine = (Magazine) savedInstanceState.getSerializable(Constants.PARAMS.MAGAZINE);
            ArrayList<Magazine> list = savedInstanceState.getParcelableArrayList(Constants.PARAMS.MAGAZINE_LIST);

            Magazine[] magazines = new Magazine[list.size()];
            magazines = list.toArray(magazines);

            mSameMagazinesListView.addItems(magazines);
        }

        if (magazine != null) {
            fillMagazineData(view, magazine);
        }
        return view;
    }

    @Override
    protected void setTitle() {
        getActivity().setTitle(R.string.title_details);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mOnMagazineSelectListener = (OnMagazineSelectListener) activity;
            mOnCategorySelectListener = (OnCategorySelectListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public void fillMagazineData(View view, Magazine magazine) {
        this.magazine = magazine;

        View progressBar = view.findViewById(R.id.progress_bar_container);
        if (progressBar != null){

            UiUtil.removeMeFromParent(progressBar);

            view.findViewById(R.id.magazine_image).setVisibility(View.VISIBLE);
            view.findViewById(R.id.magazine_data_container).setVisibility(View.VISIBLE);
        }


        UiUtil.setTextValue(view, R.id.magazine_name, magazine.getName());
        UiUtil.setTextValue(view, R.id.magazine_description, magazine.getDescription());
        UiUtil.setTextValue(view, R.id.magazine_author, magazine.getAuthor());

        UiUtil.setTextValue(view, R.id.magazine_published_at, Constants.DATE_FORMAT.format(new Date(magazine.getDate() * 1000)));
        UiUtil.setTextValue(view, R.id.magazine_publisher, magazine.getPublisher());
        UiUtil.setTextValue(view, R.id.magazine_original_uri, magazine.getOriginalUrl());

        setCategories(view, magazine.getCategories());
        setLanguages(view, magazine.getLanguages());
        setTags(view, magazine.getTags());

        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.magazine_rating);
        ratingBar.setValue(magazine.getRating(), true);

        ImageView imageView = (ImageView) view.findViewById(R.id.magazine_image);
        ImageLoader.getInstance().displayImage(magazine.getPreviewImageUrl(), imageView);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mOnMagazineSelectListener = null;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.view:
            case R.id.magazine_image:
                mOnMagazineSelectListener.onViewMagazineSelected(magazine);
                break;
            case R.id.rate:
                rate(v);
                break;
        }
    }

    private void rate(View view) {
        if (AppUser.getAuthorized()) {
            mPopup = PopupHelper.showRatePopup(getActivity(), view, this);
        } else {
            ErrorDialog.show(getActivity(), R.string.error_login_to_vote);
        }
    }

    @Override
    public void onRate(int value) {
        mPopup.dismiss();
        getSpiceManager().execute(new RateRequest(mMagazineId, value), new RateResponseListener());
    }

    @Override
    public void onItemClick(Magazine magazine) {
        mOnMagazineSelectListener.onMagazineSelected(magazine);
    }

    @Override
    public void onItemClick(android.widget.AdapterView<?> parent, View view, int position, long id) {
        Magazine magazine = (Magazine) parent.getAdapter().getItem(position);
        mOnMagazineSelectListener.onMagazineSelected(magazine);
    }

    public void setCategories(View view, Category[] categories) {
        if (categories == null) {
            return;
        }

        List<Category> list = new ArrayList<>();
        Collections.addAll(list, categories);

        RecyclerView layout = (RecyclerView) view.findViewById(R.id.categories_container);
        layout.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        layout.setAdapter(new MagazineCategoryAdapter(getActivity(), list, getCategoryClickListener()));
    }

    public void setLanguages(View view, Language[] languageList) {
        if (languageList == null) return;
        String language = "";
        for (Language l : languageList) {
            language += l.getName() + " ";
        }
        UiUtil.setTextValue(view, R.id.magazine_language, language);
    }

    public void setTags(View view, Tag[] tagsList) {
        if (tagsList == null) return;

        String tags = "";
        for (Tag t : tagsList) {
            tags += t.getName() + " ";
        }

        UiUtil.setTextValue(view, R.id.magazine_tags, tags);
    }

    private View.OnClickListener getCategoryClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CategoryItemViewHolder viewHolder = (CategoryItemViewHolder) v.getTag();
                mOnCategorySelectListener.onSelectCategory(viewHolder.getData().getId(), viewHolder.getData().getName());
            }
        };
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (magazine != null)
            outState.putSerializable(Constants.PARAMS.MAGAZINE, magazine);

        if (mSameMagazinesListView.getItemsCount() > 0) {
            outState.putParcelableArrayList(Constants.PARAMS.MAGAZINE, new ArrayList<>(mSameMagazinesListView.getAll()));
        }
    }

    private class MagazineResponseListener extends BaseRequestListener<Magazine> {
        @Override
        public void onRequestSuccess(Magazine magazine) {
            fillMagazineData(getView(), magazine);
        }
    }

    private class MagazinesSameResponseListener extends BaseRequestListener<Magazine[]> {
        @Override
        public void onRequestSuccess(Magazine magazine[]) {
            mSameMagazinesListView.addItems(magazine);
        }
    }

    private class RateResponseListener extends BaseRequestListener<Rating> {
        @Override
        public void onRequestFailure(SpiceException spiceException) {
            super.onRequestFailure(spiceException);

            ErrorDialog.show(getActivity(), R.string.error_735_user_already_voted);
        }

        @Override
        public void onRequestSuccess(Rating rating) {
            View v = getView();
            if (v != null) {
                RatingBar ratingBar = (RatingBar) v.findViewById(R.id.magazine_rating);
                magazine.setRating(rating);
                ratingBar.setValue(rating, true);
            }
        }


    }
}
