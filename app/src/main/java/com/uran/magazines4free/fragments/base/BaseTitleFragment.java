package com.uran.magazines4free.fragments.base;

import android.app.Activity;

import com.uran.magazines4free.behaviour.ITitledActivity;

/**
 * Created by ovitali on 17.04.2015.
 * Project is Magazines4Free
 */
public abstract class BaseTitleFragment extends BaseSpiceFragment {

    protected abstract void setTitle();

    private ITitledActivity mTitledActivity = null;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ITitledActivity)
            mTitledActivity = (ITitledActivity) activity;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mTitledActivity != null)
            setTitle();
    }

    public ITitledActivity getTitledActivity() {
        return mTitledActivity;
    }
}
