package com.uran.magazines4free.fragments;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;
import com.uran.magazines4free.R;
import com.uran.magazines4free.behaviour.DefaultImageLoadingListener;
import com.uran.magazines4free.behaviour.OnMenuBingFragmentStarted;
import com.uran.magazines4free.behaviour.OnMenuSelectionListener;
import com.uran.magazines4free.model.AppUser;
import com.uran.magazines4free.utils.ConverterUtil;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

public class NavigationDrawerFragment extends Fragment implements View.OnClickListener {

    private OnMenuSelectionListener mCallbacks;

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mDrawerToggle;

    public DrawerLayout mDrawerLayout;
    private View mFragmentContainerView;

    private int mCurrentButton;

    private LinearLayout mItemsHolder;

    public NavigationDrawerFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

        TextView headerText = (TextView) view.findViewById(R.id.label);

        // setup footer
        if (!AppUser.getAuthorized()) {
            headerText.setText(R.string.drawer_menu_header_guest_user);
            headerText.setGravity(Gravity.CENTER);

            TextView footerTextView = (TextView) view.findViewById(R.id.drawer_menu_item_login);
            com.uran.magazines4free.model.MenuItem menuItem = getToLoginItem();
            footerTextView.setText(menuItem.getTitleId());

            footerTextView.setOnClickListener(this);

            view.findViewById(R.id.drawer_menu_item_logout).setVisibility(View.GONE);
        } else {
            AppUser appUser = AppUser.getUser();
            String userName;
            if (appUser.getFirstName() != null) {
                userName = appUser.getFirstName() + " ";
                if (appUser.getLastName() != null) {
                    userName += appUser.getLastName();
                }
            } else {
                userName = appUser.getLogin();
            }
            headerText.setText(Html.fromHtml(getString(R.string.drawer_menu_header_app_user, userName)));

            TextView footerTextView = (TextView) view.findViewById(R.id.drawer_menu_item_logout);
            com.uran.magazines4free.model.MenuItem menuItem = getItem("drawer_menu_item_logout");
            footerTextView.setText(menuItem.getTitleId());

            footerTextView.setOnClickListener(this);

            view.findViewById(R.id.drawer_menu_item_login).setVisibility(View.GONE);
        }

        List<com.uran.magazines4free.model.MenuItem> items = getMenuItems();

        // init list view and set adapter
        mItemsHolder = (LinearLayout) view.findViewById(R.id.drawer_items_container);
        fillItemsContainer(items);

        return view;
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    private void fillItemsContainer(List<com.uran.magazines4free.model.MenuItem> items) {
        mItemsHolder.removeAllViews();

        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());

        final int size = (int) ConverterUtil.dpToPix(getActivity(), 25);
        DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(false)
                .cacheOnDisk(false)
                .postProcessor(new BitmapProcessor() {
                    @Override
                    public Bitmap process(Bitmap bitmap) {
                        return Bitmap.createScaledBitmap(bitmap, size, size, true);
                    }
                })
                .build();


        View menuButton;
        TextView label;

        for (int i = 0; i < items.size(); i++) {
            com.uran.magazines4free.model.MenuItem item = items.get(i);
            menuButton = layoutInflater.inflate(R.layout.drawer_menu_item, mItemsHolder, false);

            label = (TextView) menuButton.findViewById(R.id.label);

            int type = i % 2;

            if (type == 0) {
                menuButton.setBackgroundResource(R.drawable.drawer_menu_item_even);
            } else {
                menuButton.setBackgroundResource(R.drawable.drawer_menu_item_odd);
            }

            label.setText(item.getTitleId());
            ImageLoader.getInstance().loadImage("drawable://" + item.getIconId(), new ImageSize(size, size), displayImageOptions, new ImageLoaderListener(label));
            menuButton.setOnClickListener(this);
            menuButton.setTag(item);
            mItemsHolder.addView(menuButton);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(OnMenuBingFragmentStarted event) {
        int id = event.getItemId();

        if (id == 0)
            return;

        if (mCurrentButton == id)
            return;

        int n = mItemsHolder.getChildCount();

        for (int i = 0; i < n; i++) {
            View v = mItemsHolder.getChildAt(i);

            com.uran.magazines4free.model.MenuItem item = (com.uran.magazines4free.model.MenuItem) v.getTag();

            TextView label = (TextView) v.findViewById(R.id.label);
            label.setTextColor(getResources().getColor(item.getId() == id ? R.color.orange : R.color.white));
        }


        mCurrentButton = id;

    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                (Toolbar) getActivity().findViewById(R.id.toolbar),
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        );

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (OnMenuSelectionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (mDrawerLayout != null && isDrawerOpen()) {
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    /**
     * Per the navigation drawer design guidelines, updates the action bar to show the global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setTitle(R.string.app_name);
    }

    private ActionBar getActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();
    }


    private List<com.uran.magazines4free.model.MenuItem> getMenuItems() {
        List<com.uran.magazines4free.model.MenuItem> menuItems = new ArrayList<>(12);

        if (AppUser.getAuthorized()) {
            menuItems.add(getItem("drawer_menu_item_start"));
            menuItems.add(getItem("drawer_menu_item_categories"));
            menuItems.add(getItem("drawer_menu_item_theme"));
            menuItems.add(getItem("drawer_menu_item_search"));
            menuItems.add(getItem("drawer_menu_item_my_profile"));
            menuItems.add(getItem("drawer_menu_item_my_list"));
            menuItems.add(getItem("drawer_menu_item_my_reading_list"));
            menuItems.add(getItem("drawer_menu_item_notifications"));
            menuItems.add(getItem("drawer_menu_item_help"));
            menuItems.add(getItem("drawer_menu_item_about_us"));
            menuItems.add(getItem("drawer_menu_item_contacts"));
        } else {
            menuItems.add(getItem("drawer_menu_item_start"));
            menuItems.add(getItem("drawer_menu_item_categories"));
            menuItems.add(getItem("drawer_menu_item_theme"));
            menuItems.add(getItem("drawer_menu_item_search"));
            menuItems.add(getItem("drawer_menu_item_help"));
            menuItems.add(getItem("drawer_menu_item_about_us"));
            menuItems.add(getItem("drawer_menu_item_contacts"));
        }

        return menuItems;
    }

    public com.uran.magazines4free.model.MenuItem getItem(String itemId) {
        int id = getResources().getIdentifier(itemId, "id", getActivity().getPackageName());
        int drawableId = getResources().getIdentifier(itemId, "drawable", getActivity().getPackageName());
        int stringId = getResources().getIdentifier(itemId, "string", getActivity().getPackageName());

        return new com.uran.magazines4free.model.MenuItem(id, drawableId, stringId);
    }

    public com.uran.magazines4free.model.MenuItem getToLoginItem() {
        int id = getResources().getIdentifier("drawer_menu_item_login", "id", getActivity().getPackageName());
        int drawableId = getResources().getIdentifier("drawer_menu_item_logout", "drawable", getActivity().getPackageName());
        int stringId = getResources().getIdentifier("drawer_menu_item_login_register", "string", getActivity().getPackageName());

        return new com.uran.magazines4free.model.MenuItem(id, drawableId, stringId);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawer_menu_item_login:
            case R.id.drawer_menu_item_logout:
                mCallbacks.onMenuSelected(v.getId());
                break;

            default:
                com.uran.magazines4free.model.MenuItem item = (com.uran.magazines4free.model.MenuItem) v.getTag();
                mCallbacks.onMenuSelected(item.getId());
        }
    }

    public static class ImageLoaderListener extends DefaultImageLoadingListener {
        TextView labelTextView;

        public ImageLoaderListener(TextView textView) {
            labelTextView = textView;
        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            labelTextView.setCompoundDrawablesWithIntrinsicBounds(new BitmapDrawable(labelTextView.getContext().getResources(), loadedImage), null, null, null);
        }
    }
}
