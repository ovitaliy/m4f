package com.uran.magazines4free.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uran.magazines4free.Constants;
import com.uran.magazines4free.R;
import com.uran.magazines4free.adapter.CategoryAdapter;
import com.uran.magazines4free.behaviour.OnCategorySelectListener;
import com.uran.magazines4free.dataSource.loaders.CategoriesDataSource;
import com.uran.magazines4free.fragments.base.BaseMenuBindSpiceFragment;
import com.uran.magazines4free.model.Category;
import com.uran.magazines4free.widgets.items.CategoryItemViewHolder;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoriesTitleFragment extends BaseMenuBindSpiceFragment
        implements LoaderManager.LoaderCallbacks<List<Category>> {

    public static CategoriesTitleFragment newInstance() {
        return new CategoriesTitleFragment();
    }

    private CategoryAdapter mCategoryAdapter;
    private RecyclerView mRecyclerView;

    private OnCategorySelectListener mCategorySelectListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categories, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.categories_list);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 4);
        mRecyclerView.setLayoutManager(gridLayoutManager);

        getLoaderManager().initLoader(Constants.LOADERS.CATEGORY, Bundle.EMPTY, this);

        return view;
    }

    private View.OnClickListener getOnItemClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CategoryItemViewHolder item = (CategoryItemViewHolder) v.getTag();
                Category category = item.getData();
                mCategorySelectListener.onSelectCategory(category.getId(), category.getName());
            }
        };
    }

    @Override
    public Loader<List<Category>> onCreateLoader(int id, Bundle args) {
        return new CategoriesDataSource(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<List<Category>> loader, List<Category> data) {
        for (Category c : data) {
            c.setSelected(false);
        }
        mCategoryAdapter = new CategoryAdapter(getActivity(), data, getOnItemClickListener());
        mRecyclerView.setAdapter(mCategoryAdapter);
    }

    @Override
    public void onLoaderReset(Loader<List<Category>> loader) {
    }

    @Override
    protected void setTitle() {
        getTitledActivity().setTitle(R.string.drawer_menu_item_categories);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCategorySelectListener = (OnCategorySelectListener) activity;
    }

    @Override
    protected int getMenuBindId() {
        return R.id.drawer_menu_item_categories;
    }
}
