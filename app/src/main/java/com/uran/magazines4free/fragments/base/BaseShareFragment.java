package com.uran.magazines4free.fragments.base;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.google.android.gms.plus.PlusShare;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;
import com.uran.magazines4free.Constants;
import com.uran.magazines4free.R;
import com.uran.magazines4free.model.Magazine;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by ovitali on 24.04.2015.
 * Project is Magazines4Free
 */
public abstract class BaseShareFragment extends BaseMenuBindSpiceFragment {

    private UiLifecycleHelper uiHelper;

    private TwitterAuthClient mTwitterAuthClient;

    protected Magazine magazine;


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (uiHelper != null)
            uiHelper.onActivityResult(requestCode, resultCode, data);

        if (mTwitterAuthClient != null)
            mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * share via Facebook
     */
    protected void shareFb() {
        uiHelper = new UiLifecycleHelper(getActivity(), getFbCallback());
        Session.openActiveSession(getActivity(), true, getFbCallback());
    }

    private Session.StatusCallback mSessionStatusCallback;

    private Session.StatusCallback getFbCallback() {
        if (mSessionStatusCallback == null)
            mSessionStatusCallback = new Session.StatusCallback() {
                @Override
                public void call(Session session, SessionState state, Exception exception) {
                    if (state.isOpened() && exception == null) {

                        if (session.getPermissions().contains("publish_actions")) {
                            Bundle params = new Bundle();
                            params.putString("message", magazine.getName());
                            params.putString("link", magazine.getOriginalUrl());

                            Request request = new Request(Session.getActiveSession(), "me/feed", params, HttpMethod.POST);
                            request.setCallback(new Request.Callback() {
                                @Override
                                public void onCompleted(Response response) {
                                    if (response.getError() == null) {
                                        Toast.makeText(getActivity(), R.string.share_done, Toast.LENGTH_SHORT).show();
                                        uiHelper = null;
                                    }
                                }
                            });
                            request.executeAsync();
                        } else {
                            List<String> permissions = new ArrayList<>();
                            permissions.addAll(session.getPermissions());
                            permissions.add("publish_actions");
                            session.requestNewPublishPermissions(new Session.NewPermissionsRequest(getActivity(), permissions));
                        }
                    }
                }
            };

        return mSessionStatusCallback;
    }

    /**
     * share via Twitter
     */
    protected void shareTw() {
        TwitterAuthConfig authConfig = new TwitterAuthConfig(Constants.SOCIAL_AUTH.TWITTER_KEY, Constants.SOCIAL_AUTH.TWITTER_SECRET);
        Fabric.with(getActivity(), new Twitter(authConfig));

        mTwitterAuthClient = new TwitterAuthClient();
        mTwitterAuthClient.authorize(getActivity(), new Callback<TwitterSession>() {
                    @Override
                    public void success(Result<TwitterSession> twitterSessionResult) {
                        if (!isAdded())
                            return;
                        TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient(twitterSessionResult.data);
                        StatusesService statusesService = twitterApiClient.getStatusesService();

                        statusesService.update(magazine.getName() + " " + magazine.getOriginalUrl(), null, null, null, null, null, null, null, new Callback<Tweet>() {
                            @Override
                            public void success(Result<Tweet> tweetResult) {
                                Toast.makeText(getActivity(), R.string.share_done,
                                        Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void failure(TwitterException e) {
                                Toast.makeText(getActivity(), "Ошибка при отправке твита",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });

                    }

                    @Override
                    public void failure(TwitterException e) {
                        Log.i("LoginFragment", "TwitterException", e);
                    }
                }

        );
    }


    /**
     * Share via Google+
     */
    protected void shareGp(){
        Intent shareIntent = new PlusShare.Builder(getActivity())
                .setType("text/plain")
                .setText(magazine.getName())
                .setContentUrl(Uri.parse(magazine.getOriginalUrl()))
                .getIntent();

        startActivityForResult(shareIntent, 0);
    }

    protected void shareEmail(){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        intent.putExtra(Intent.EXTRA_TEXT, magazine.getName() + " " + magazine.getOriginalUrl());
        startActivity(Intent.createChooser(intent, "Send Email"));
    }

}
