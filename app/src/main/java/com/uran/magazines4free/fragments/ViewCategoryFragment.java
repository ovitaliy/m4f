package com.uran.magazines4free.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.octo.android.robospice.persistence.DurationInMillis;
import com.uran.magazines4free.Constants;
import com.uran.magazines4free.R;
import com.uran.magazines4free.adapter.MagazinesAdapter;
import com.uran.magazines4free.adapter.MagazinesGridViewAdapter;
import com.uran.magazines4free.api.requests.BaseRequestListener;
import com.uran.magazines4free.api.requests.GetMagazinesListRequest;
import com.uran.magazines4free.behaviour.OnMagazineSelectListener;
import com.uran.magazines4free.fragments.base.BaseMenuBindSpiceFragment;
import com.uran.magazines4free.model.Magazine;
import com.uran.magazines4free.model.OrderType;
import com.uran.magazines4free.widgets.items.MagazineView;
import com.uran.magazines4free.widgets.lists.BaseHorizontalListView;
import com.uran.magazines4free.widgets.lists.HistoricHorizontalListView;

import in.srain.cube.views.GridViewWithHeaderAndFooter;

/**
 * Created by ovitali on 12.05.2015.
 * Project is Magazines4Free
 */
public class ViewCategoryFragment extends BaseMenuBindSpiceFragment implements
        BaseHorizontalListView.OnItemClickListener,
        GridView.OnItemClickListener {

    public static ViewCategoryFragment newInstance(int categoryId, String categoryName) {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.PARAMS.CATEGORY_ID, categoryId);
        bundle.putString(Constants.PARAMS.CATEGORY_NAME, categoryName);

        ViewCategoryFragment fragment = new ViewCategoryFragment();
        fragment.setArguments(bundle);

        return fragment;
    }


    private int mCategoryId;
    private String mCategoryName;

    private HistoricHorizontalListView mHorizontalListView;
    private MagazinesAdapter mGridAdapter;

    private OnMagazineSelectListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCategoryId = getArguments().getInt(Constants.PARAMS.CATEGORY_ID);
        mCategoryName = getArguments().getString(Constants.PARAMS.CATEGORY_NAME);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_category, container, false);

        mHorizontalListView = new HistoricHorizontalListView(getActivity());
        mHorizontalListView.setUpViewAllButton(
                ContextCompat.getDrawable(getActivity(), R.drawable.hl_redactor),
                getString(R.string.horizontal_list_title_redactors),
                false
        );
        mHorizontalListView.setOnItemClickListener(this);

        GridViewWithHeaderAndFooter gridView = (GridViewWithHeaderAndFooter) view.findViewById(R.id.grid_view);
        gridView.addHeaderView(mHorizontalListView);
        gridView.setColumnWidth(MagazineView.ITEM_WIDTH);
        mGridAdapter = new MagazinesGridViewAdapter(getActivity());
        gridView.setAdapter(mGridAdapter);
        gridView.setOnItemClickListener(this);

        getSpiceManager().execute(new GetMagazinesListRequest().setCategory(mCategoryId), new MagazinesRequestListener());

        getSpiceManager().execute(
                new GetMagazinesListRequest().setCategory(mCategoryId).setOrder(OrderType.BEST),
                "CategoryName" + mCategoryName, DurationInMillis.ONE_MINUTE,
                new BestMagazinesRequestListener());

        return view;
    }

    @Override
    protected void setTitle() {
        getTitledActivity().setTitle(mCategoryName);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (OnMagazineSelectListener) activity;
    }

    @Override
    protected int getMenuBindId() {
        return R.id.drawer_menu_item_categories;
    }

    private final class MagazinesRequestListener extends BaseRequestListener<Magazine[]> {
        @Override
        public void onRequestSuccess(Magazine[] magazines) {
            mGridAdapter.add(magazines);
        }
    }

    private final class BestMagazinesRequestListener extends BaseRequestListener<Magazine[]> {
        @Override
        public void onRequestSuccess(Magazine[] magazines) {
            mHorizontalListView.addItems(magazines);
        }
    }

    @Override
    public void onItemClick(Magazine magazine) {
        mListener.onMagazineSelected(magazine);
    }

    @Override
    public void onItemClick(android.widget.AdapterView<?> parent, View view, int position, long id) {
        Object magazine = parent.getAdapter().getItem(position);
        mListener.onMagazineSelected((com.uran.magazines4free.model.Magazine) magazine);
    }
}
