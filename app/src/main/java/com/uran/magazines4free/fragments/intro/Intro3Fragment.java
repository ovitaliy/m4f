package com.uran.magazines4free.fragments.intro;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uran.magazines4free.R;


public class Intro3Fragment extends Fragment {


    public static Intro3Fragment newInstance() {
        Intro3Fragment fragment = new Intro3Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_intro_3, container, false);
        return view;
    }


}
