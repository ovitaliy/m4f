package com.uran.magazines4free.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uran.magazines4free.R;
import com.uran.magazines4free.behaviour.OnMagazineSelectListener;
import com.uran.magazines4free.fragments.base.BaseTitleFragment;
import com.uran.magazines4free.model.Magazine;
import com.uran.magazines4free.widgets.lists.BaseHorizontalListView;


public class MainListsFragment extends BaseTitleFragment implements BaseHorizontalListView.OnItemClickListener {

    private OnMagazineSelectListener mListener;

    public static MainListsFragment newInstance() {
        return new MainListsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_lists, container, false);

        //initList(view, R.id.horizontal_list_view_1);
     /*   HorizontalListView horizontalListView = (HorizontalListView) view.findViewById(R.id.horizontal_list_view_1);
        horizontalListView.setAdapter(new MagazinesAdapter(getActivity()));

        initList(view, R.id.horizontal_list_view_2);
        initList(view, R.id.horizontal_list_view_3);
        initList(view, R.id.horizontal_list_view_4);
        initList(view, R.id.horizontal_list_view_5);
        initList(view, R.id.horizontal_list_view_6);*/

        return view;
    }

    private void initList(View view, int listId) {
      /*  HistoricHorizontalListView hListView = (HistoricHorizontalListView) view.findViewById(listId);
        hListView.setAdapter(new MagazinesAdapter(getActivity()));
        hListView.setOnItemClickListener(this);*/
    }


    @Override
    protected void setTitle() {
        getTitledActivity().setTitle();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnMagazineSelectListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onItemClick(Magazine magazine) {
        mListener.onMagazineSelected(magazine);
    }

}
