package com.uran.magazines4free.fragments.auth;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.uran.magazines4free.Constants;
import com.uran.magazines4free.R;
import com.uran.magazines4free.activity.auth.RegistrationActivity;
import com.uran.magazines4free.activity.base.BaseSpiceFragmentActivity;
import com.uran.magazines4free.api.requests.LoginRequest;
import com.uran.magazines4free.api.requests.LoginSocialRequest;
import com.uran.magazines4free.api.responses.LoginResponse;
import com.uran.magazines4free.dialogs.DialogUtil;
import com.uran.magazines4free.dialogs.ProgressDialog;
import com.uran.magazines4free.dialogs.TwitterGetEmailDialog;
import com.uran.magazines4free.listeners.LoginRequestListener;
import com.uran.magazines4free.model.AppUser;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by ovitali on 26.02.2015.
 */
public class LoginFragment extends BaseAuthFragment implements
        View.OnClickListener,
        Validator.ValidationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    // A magic number we will use to know that our sign-in error
    // resolution activity has completed.
    private static final int OUR_REQUEST_CODE = 49404;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }


    @NotEmpty
    @Email
    private EditText mEmailEditText;

    @Password(min = 6)
    private EditText mPasswordEditText;

    private Validator mValidator;

    private UiLifecycleHelper uiHelper;


    // We can store the connection result from a failed connect()
    // attempt in order to make the application feel a bit more
    // responsive for the user.
    private ConnectionResult mConnectionResult;
    private GoogleApiClient mPlusClient;
    private boolean mResolveOnFail = true;

    private TwitterAuthClient mTwitterAuthClient;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        view.findViewById(R.id.login).setOnClickListener(this);
        view.findViewById(R.id.login_social_facebook).setOnClickListener(this);
        view.findViewById(R.id.login_social_gp).setOnClickListener(this);
        view.findViewById(R.id.login_social_tweeter).setOnClickListener(this);

        TextView textView = (TextView) view.findViewById(R.id.to_registration);
        textView.setText(Html.fromHtml(getString(R.string.navigate_to_registration)));

        view.findViewById(R.id.to_registration).setOnClickListener(this);

        mEmailEditText = (EditText) view.findViewById(R.id.email);
        mPasswordEditText = (EditText) view.findViewById(R.id.password);


        //init validator
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.to_registration:
                RegistrationActivity.startNewInstance(getActivity());
                break;
            case R.id.login:
                // validate fields before send request
                mValidator.validate();
                break;
            case R.id.login_social_facebook:
                loginWithFacebook();
                break;
            case R.id.login_social_tweeter:
                loginWithTwitter();
                break;
            case R.id.login_social_gp:
                loginWithGP();
                break;
        }

        getActivity().setProgressBarIndeterminateVisibility(true);
    }

    private void loginWithGP() {
        mPlusClient = new GoogleApiClient.Builder(getActivity(), this, this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();
        mPlusClient.connect();
    }

    @Override
    public void onValidationSucceeded() {
        LoginRequest loginRequest = new LoginRequest()
                .setEmail(mEmailEditText.getText().toString())
                .setPassword(mPasswordEditText.getText().toString());

        getSpiceManager().execute(loginRequest, new LoginResponseListener());

        DialogUtil.show(getFragmentManager(), new ProgressDialog());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void loginWithTwitter() {
        TwitterAuthConfig authConfig = new TwitterAuthConfig(Constants.SOCIAL_AUTH.TWITTER_KEY, Constants.SOCIAL_AUTH.TWITTER_SECRET);
        Fabric.with(getActivity(), new Twitter(authConfig));

        mTwitterAuthClient = new TwitterAuthClient();
        mTwitterAuthClient.authorize(getActivity(), new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> twitterSessionResult) {
                if (!isAdded())
                    return;

                final String username = twitterSessionResult.data.getUserName();

                TwitterAuthClient authClient = new TwitterAuthClient();
                authClient.requestEmail(twitterSessionResult.data, new Callback<String>() {
                    @Override
                    public void success(Result<String> stringResult) {
                        String email = stringResult.data;
                        socialLogin(email, username);
                    }

                    @Override
                    public void failure(TwitterException e) {
                        Log.i("LoginFragment", "TwitterException", e);

                        TwitterGetEmailDialog twitterGetEmailDialog = TwitterGetEmailDialog.newInstance(new TwitterGetEmailDialog.OnTwitterEmailEnterListener() {
                            @Override
                            public void onTwitterEmailEntered(String email) {
                                socialLogin(email, username);
                            }
                        });
                        twitterGetEmailDialog.show(getActivity().getSupportFragmentManager(), "twitterGetEmailDialog");
                    }
                });

            }

            @Override
            public void failure(TwitterException e) {
                Log.i("LoginFragment", "TwitterException", e);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (uiHelper != null)
            uiHelper.onActivityResult(requestCode, resultCode, data);

        if (mPlusClient != null) {
            if (requestCode == OUR_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                mResolveOnFail = true;
                mPlusClient.connect();
            } else if (requestCode == OUR_REQUEST_CODE && resultCode != Activity.RESULT_OK) {
            }
        }

        if (mTwitterAuthClient != null)
            mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
    }


    private void loginWithFacebook() {
        uiHelper = new UiLifecycleHelper(getActivity(), getFbCallback());
        Session.openActiveSession(getActivity(), true, getFbPermissions(), getFbCallback());
    }

    private Session.StatusCallback getFbCallback() {
        return new Session.StatusCallback() {
            @Override
            public void call(Session session, SessionState state, Exception exception) {
                if (state.isOpened()) {
                    getFbMe(session);
                    session.removeCallback(this);
                }
            }
        };
    }

    public ArrayList<String> getFbPermissions() {
        ArrayList<String> permissions = new ArrayList<>(1);
        permissions.add("email");
        return permissions;
    }

    private void getFbMe(final Session session) {
        Request.newMeRequest(session, new Request.GraphUserCallback() {
            @Override
            public void onCompleted(GraphUser graphUser, Response response) {
                String email = graphUser.getProperty("email").toString();
                String name = graphUser.getFirstName() + " " + graphUser.getLastName();
                socialLogin(email, name);
            }
        }).executeAsync();
    }


    @Override
    public void onConnected(Bundle bundle) {
        String email = Plus.AccountApi.getAccountName(mPlusClient);
        String name = Plus.PeopleApi.getCurrentPerson(mPlusClient).getDisplayName();
        Log.i("LoginFragment", email);
        socialLogin(email, name);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    public void socialLogin(String email, String name) {
        LoginSocialRequest loginRequest = new LoginSocialRequest()
                .setEmail(email)
                .setName(name);

        getSpiceManager().execute(loginRequest, new LoginResponseListener());

        DialogUtil.show(getFragmentManager(), new ProgressDialog());
    }

    /**
     * A helper method to flip the mResolveOnFail flag and start the resolution
     * of the ConnenctionResult from the failed connect() call.
     */
    private void startResolution() {
        try {
            mResolveOnFail = false;
            mConnectionResult.startResolutionForResult(getActivity(), OUR_REQUEST_CODE);
        } catch (IntentSender.SendIntentException e) {
            mPlusClient.connect();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v("LoginFragment", "ConnectionFailed");
        // Most of the time, the connection will fail with a
        // user resolvable result. We can store that in our
        // mConnectionResult property ready for to be used
        // when the user clicks the sign-in button.
        if (connectionResult.hasResolution()) {
            mConnectionResult = connectionResult;
            if (mResolveOnFail) {
                startResolution();
            }
        }
    }

    @Override
    protected void setTitle() {
        // leave it blank.
    }


    // ============================================================================================
    // INNER CLASSES
    // ============================================================================================
    public final class LoginResponseListener extends LoginRequestListener {

        @Override
        public void onRequestSuccess(LoginResponse loginResponse) {
            super.onRequestSuccess(loginResponse);

            AppUser appUser = loginResponse.getUser();
            onAuthListener.onAuthComplete(appUser);
            DialogUtil.cancel(getFragmentManager());
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            DialogUtil.cancel(getFragmentManager());
            ((BaseSpiceFragmentActivity)getActivity()).obtainServerResponseError(spiceException);
        }
    }
}
