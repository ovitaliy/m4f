package com.uran.magazines4free.fragments.magazine;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.View;
import android.widget.PopupWindow;

import com.uran.magazines4free.R;
import com.uran.magazines4free.fragments.base.BaseShareFragment;
import com.uran.magazines4free.widgets.popup.SharePopup;

/**
 * Created by ovitali on 19.05.2015.
 * Project is Magazines4Free
 */
public abstract class BaseMagazineFragment extends BaseShareFragment implements View.OnClickListener{


    PopupWindow mPopup;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view:
            case R.id.add_my_list:
            case R.id.add_reading:
            case R.id.download:
                break;

            case R.id.share:
                share(v);
                break;
        }
    }

    private void share(View view) {
        mPopup = SharePopup.create(getActivity(), view, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.share_fb:
                        shareFb();
                        break;

                    case R.id.share_tw:
                        shareTw();
                        break;

                    case R.id.share_email:
                        shareEmail();
                        break;

                    case R.id.share_gp:
                        shareGp();
                        break;

                    case R.id.share_clipboard:
                        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText(magazine.getName(), magazine.getOriginalUrl());
                        clipboard.setPrimaryClip(clip);

                        break;
                }

                mPopup.dismiss();
            }
        });
    }

    @Override
    protected int getMenuBindId() {
        return 0;
    }

}
