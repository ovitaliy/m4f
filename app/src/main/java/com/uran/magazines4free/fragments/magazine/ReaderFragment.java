package com.uran.magazines4free.fragments.magazine;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestProgress;
import com.octo.android.robospice.request.listener.RequestProgressListener;
import com.octo.android.robospice.request.simple.BigBinaryRequest;
import com.uran.magazines4free.Constants;
import com.uran.magazines4free.R;
import com.uran.magazines4free.activity.MainActivity;
import com.uran.magazines4free.api.requests.BaseRequestListener;
import com.uran.magazines4free.fragments.StartFragment;
import com.uran.magazines4free.model.Magazine;
import com.uran.magazines4free.utils.ConverterUtil;
import com.uran.magazines4free.utils.FilePathHelper;
import com.uran.magazines4free.utils.UiUtil;
import com.uran.magazines4free.widgets.PdfViewer;

import java.io.File;
import java.io.InputStream;
import java.text.DecimalFormat;

/**
 * Created by ovitali on 19.05.2015.
 * Project is Magazines4Free
 */
public class ReaderFragment extends BaseMagazineFragment implements PdfViewer.OnPageChangeListener {

    public static ReaderFragment newInstance(Magazine magazine) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.PARAMS.MAGAZINE, magazine);
        ReaderFragment readerFragment = new ReaderFragment();
        readerFragment.setArguments(bundle);
        return readerFragment;
    }

    @Override
    protected void setTitle() {
        getTitledActivity().setTitle(magazine.getName());
    }

    private PdfViewer mPdfViewer;

    private View mLeftButton;
    private View mRightButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        magazine = (Magazine) getArguments().getSerializable(Constants.PARAMS.MAGAZINE);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reader, container, false);

        view.findViewById(R.id.navigate_to_start).setOnClickListener(this);
        view.findViewById(R.id.add_reading).setOnClickListener(this);
        view.findViewById(R.id.add_my_list).setOnClickListener(this);
        view.findViewById(R.id.download).setOnClickListener(this);
        view.findViewById(R.id.share).setOnClickListener(this);
        view.findViewById(R.id.close).setOnClickListener(this);

        mLeftButton = view.findViewById(R.id.btn_left);
        mRightButton = view.findViewById(R.id.btn_right);

        mLeftButton.setOnClickListener(this);
        mRightButton.setOnClickListener(this);

        mPdfViewer = (PdfViewer) view.findViewById(R.id.pdfView);
        mPdfViewer.setVisibility(View.INVISIBLE);
        mPdfViewer.setOnPageChangeListener(this);

        File file = FilePathHelper.getPdfFilePath(getActivity(), magazine.getId());

        if (file.exists()) {
            showMagazine();
            ((RelativeLayout) view).removeView(view.findViewById(R.id.progress_bar_container));
        } else {
            BigBinaryRequest bigBinaryRequest = new BigBinaryRequest(magazine.getOriginalUrl(), file);
            getSpiceManager().execute(bigBinaryRequest, "m" + magazine.getId(), DurationInMillis.ONE_DAY, new MagazineLoaderListener());
        }

        return view;
    }

    private void showMagazine() {
        View v = getView();
        if (v != null) {
            ((RelativeLayout) v).removeView(v.findViewById(R.id.progress_bar_container));
        }

        mPdfViewer.setVisibility(View.VISIBLE);
        mPdfViewer.loadPdf(FilePathHelper.getPdfFilePath(getActivity(), magazine.getId()));
    }

    @Override
    public void onPageChanged(int currentPage, int countPage, boolean isLast) {
        mLeftButton.setVisibility(currentPage != 0 ? View.VISIBLE : View.GONE);
        mRightButton.setVisibility(!isLast ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.btn_left:
                mPdfViewer.smoothScrollByPage(-1);
                break;
            case R.id.btn_right:
                mPdfViewer.smoothScrollByPage(1);
                break;
            case R.id.close:
                getActivity().onBackPressed();
                break;

            case R.id.navigate_to_start:
                ((MainActivity) getActivity()).startFragment(StartFragment.newInstance(), true, true);
        }
    }

    // --------------------------- inner classes
    private class MagazineLoaderListener extends BaseRequestListener<InputStream> implements RequestProgressListener {

        private long mLastProgressUpdate;

        @Override
        public void onRequestSuccess(InputStream inputStream) {
            showMagazine();
        }

        @Override
        public void onRequestProgressUpdate(RequestProgress progress) {
            if (getView() != null && System.currentTimeMillis() - mLastProgressUpdate > 30) {
                TextView textView = (TextView) getView().findViewById(R.id.progress_bar_progress);
                textView.setText(new DecimalFormat("#.##").format(progress.getProgress() * 100.0f) + "%");
                mLastProgressUpdate = System.currentTimeMillis();
            }
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            super.onRequestFailure(spiceException);
            if (getView() != null) {
                TextView textView = (TextView) getView().findViewById(R.id.progress_bar_progress);
                String errorText = getString(R.string.error_pdf_load_error_occurred);
                textView.getLayoutParams().width = (int) ConverterUtil.dpToPix(getActivity(), 300);

                if (!URLUtil.isValidUrl(magazine.getOriginalUrl())) {
                    errorText += "\n";
                    errorText += "Url is not valid: \"" + magazine.getOriginalUrl() + "\"";
                }

                textView.setText(errorText);

                View view = getView().findViewById(R.id.progress_bar);
                UiUtil.removeMeFromParent(view);


            }
        }
    }
}
