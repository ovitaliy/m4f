package com.uran.magazines4free.fragments;


import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.LinearLayout;

import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.uran.magazines4free.Constants;
import com.uran.magazines4free.MagazineApplication;
import com.uran.magazines4free.R;
import com.uran.magazines4free.adapter.InterestAdapter;
import com.uran.magazines4free.adapter.LanguageAdapter;
import com.uran.magazines4free.api.requests.EditUserCategoriesAndLanguages;
import com.uran.magazines4free.api.requests.GetLanguagesRequest;
import com.uran.magazines4free.api.requests.GetUserCategoriesAndLanguages;
import com.uran.magazines4free.dataSource.DatabaseManager;
import com.uran.magazines4free.dataSource.loaders.InterestsDataSource;
import com.uran.magazines4free.dataSource.loaders.LanguagesDataSource;
import com.uran.magazines4free.fragments.base.BaseMenuBindSpiceFragment;
import com.uran.magazines4free.model.AppUser;
import com.uran.magazines4free.model.Interest;
import com.uran.magazines4free.model.Language;
import com.uran.magazines4free.model.MenuItem;
import com.uran.magazines4free.model.UserLanguagesAndCategories;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InterestsFragment extends BaseMenuBindSpiceFragment {

    public static InterestsFragment newInstance() {
        return new InterestsFragment();
    }

    private GridLayout mInterestContainer;
    private LinearLayout mLanguagesContainer;

    private InterestAdapter mInterestAdapter;
    private LanguageAdapter mLanguageAdapter;

    private AppUser mUser;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_interests, container, false);

        mUser = AppUser.getUser();

        mInterestContainer = (GridLayout) view.findViewById(R.id.interest_container);
        mInterestContainer.setUseDefaultMargins(true);
        mLanguagesContainer = (LinearLayout) view.findViewById(R.id.languages_container);

        getLoaderManager().initLoader(Constants.LOADERS.INTERESTS, Bundle.EMPTY, new InterestLoaderCallbacks());
        getLoaderManager().initLoader(Constants.LOADERS.LANGUAGES, Bundle.EMPTY, new LanguagesLoaderCallbacks());

        getSpiceManager().execute(new GetUserCategoriesAndLanguages().setId(mUser.getUid()), "GetUserCategoriesAndLanguages", DurationInMillis.ONE_MINUTE, new GetLanguagesRequestListener());

        return view;
    }

    private void fillInterests(List<Interest> data) {
        mInterestAdapter = new InterestAdapter(getActivity(), data);
        mInterestContainer.removeAllViews();
        for (int i = 0; i < data.size(); i++) {
            View v = mInterestAdapter.getView(i, null, mInterestContainer);
            mInterestContainer.addView(v);
            int column = i % 4;
            int row = i / 4;


            GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(GridLayout.spec(row), GridLayout.spec(column));
            layoutParams.setGravity(Gravity.CENTER);
            layoutParams.width = MagazineApplication.SCREEN_WIDTH / 4;
            layoutParams.setMargins(0, 0, 0, 0);
            v.setLayoutParams(layoutParams);


            v.setOnClickListener(new View.OnClickListener() {
                                     @Override
                                     public void onClick(View v) {
                                         for (int i = 0; i < mInterestContainer.getChildCount(); i++) {
                                             View child = mInterestContainer.getChildAt(i);


                                             if (child.equals(v)) {
                                                 mInterestAdapter.selectItem(i, v);
                                                 break;
                                             }
                                         }
                                     }
                                 }

            );
        }
    }

    /**
     * fill container with data retrieved from database. If data null get it from api
     *
     * @param data list of languages
     */
    private void fillLanguages(List<Language> data) {
        if (data != null && data.size() > 0) {
            mLanguageAdapter = new LanguageAdapter(getActivity(), data);
            mLanguagesContainer.removeAllViews();
            for (int i = 0; i < data.size(); i++) {
                View v = mLanguageAdapter.getView(i, null, mLanguagesContainer);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                v.setLayoutParams(layoutParams);
                mLanguagesContainer.addView(v);
                v.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {
                                             for (int i = 0; i < mLanguagesContainer.getChildCount(); i++) {
                                                 View child = mLanguagesContainer.getChildAt(i);
                                                 if (child.equals(v)) {
                                                     mLanguageAdapter.selectItem(i, v);
                                                     break;
                                                 }
                                             }
                                         }
                                     }

                );
            }
        } else {
            getSpiceManager().execute(new GetLanguagesRequest(), "GetLanguagesRequest", 1, new LanguageRequestListener());
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        try {
            DatabaseManager.getInstance().getHelper().getLanguageDao().updateAsync(mLanguageAdapter.getAll());
            DatabaseManager.getInstance().getHelper().getInterestDao().updateAsync(mInterestAdapter.getAll());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        int uid = mUser.getUid();
        MagazineApplication.getSpiceManager().execute(
                new EditUserCategoriesAndLanguages(uid)
                        .setIds(mLanguageAdapter.getSelectedIds())
                        .setType("languages"),
                null
        );

        MagazineApplication.getSpiceManager().execute(
                new EditUserCategoriesAndLanguages(uid)
                        .setIds(mInterestAdapter.getSelectedIds())
                        .setType("interests"),
                null
        );

    }

    @Override
    protected void setTitle() {
        getTitledActivity().setTitle(1,
                new MenuItem(R.id.drawer_menu_item_my_profile, R.string.view_my_profile),
                new MenuItem(R.id.drawer_menu_item_interests, R.string.interests));
    }

    @Override
    protected int getMenuBindId() {
        return R.id.drawer_menu_item_my_profile;
    }

    /**
     * Loader callback for loading interests from database
     */
    public class InterestLoaderCallbacks implements LoaderManager.LoaderCallbacks<List<Interest>> {
        @Override
        public Loader<List<Interest>> onCreateLoader(int id, Bundle args) {
            return new InterestsDataSource(getActivity());
        }

        @Override
        public void onLoadFinished(Loader<List<Interest>> loader, List<Interest> data) {
            fillInterests(data);
        }

        @Override
        public void onLoaderReset(Loader<List<Interest>> loader) {

        }
    }


    public class LanguagesLoaderCallbacks implements LoaderManager.LoaderCallbacks<List<Language>> {
        @Override
        public Loader<List<Language>> onCreateLoader(int id, Bundle args) {
            return new LanguagesDataSource(getActivity());
        }

        @Override
        public void onLoadFinished(Loader<List<Language>> loader, List<Language> data) {
            mLanguagesContainer.removeAllViews();
            fillLanguages(data);
        }

        @Override
        public void onLoaderReset(Loader<List<Language>> loader) {

        }
    }


    public class LanguageRequestListener implements RequestListener<Language[]> {
        @Override
        public void onRequestFailure(SpiceException spiceException) {
        }

        @Override
        public void onRequestSuccess(Language[] languages) {
            if (languages != null && languages.length > 0) {
                List<Language> languageList = new ArrayList<>(languages.length);
                Collections.addAll(languageList, languages);
                fillLanguages(languageList);
            }
        }
    }

    public class GetLanguagesRequestListener implements RequestListener<UserLanguagesAndCategories> {
        @Override
        public void onRequestFailure(SpiceException spiceException) {
        }

        @Override
        public void onRequestSuccess(UserLanguagesAndCategories userLanguagesAndCategories) {
            getLoaderManager().restartLoader(Constants.LOADERS.INTERESTS, Bundle.EMPTY, new InterestLoaderCallbacks());
            getLoaderManager().restartLoader(Constants.LOADERS.LANGUAGES, Bundle.EMPTY, new LanguagesLoaderCallbacks());
        }
    }


}
