package com.uran.magazines4free.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.uran.magazines4free.R;
import com.uran.magazines4free.activity.base.BaseSpiceFragmentActivity;
import com.uran.magazines4free.adapter.CountryAdapter;
import com.uran.magazines4free.api.requests.CountriesRequest;
import com.uran.magazines4free.api.requests.EditProfileRequest;
import com.uran.magazines4free.api.requests.GetUserRequest;
import com.uran.magazines4free.dialogs.DialogUtil;
import com.uran.magazines4free.dialogs.ProgressDialog;
import com.uran.magazines4free.fragments.base.BaseMenuBindSpiceFragment;
import com.uran.magazines4free.model.AppUser;
import com.uran.magazines4free.model.Country;
import com.uran.magazines4free.model.MenuItem;
import com.uran.magazines4free.utils.UiUtil;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileFragment extends BaseMenuBindSpiceFragment implements View.OnClickListener {

    public static EditProfileFragment newInstance() {
        return new EditProfileFragment();
    }


    private AppUser mAppUser;

    @NotEmpty
    private EditText mNickEditText;

    @NotEmpty
    @Email
    private EditText mEmailEditText;

    @Password(min = 6)
    private EditText mPasswordEditText;

    @ConfirmPassword
    private EditText mPasswordConfirmEditText;

    private Validator mValidator;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);

        mNickEditText = (EditText) view.findViewById(R.id.nickname);
        mEmailEditText = (EditText) view.findViewById(R.id.email);
        mPasswordEditText = (EditText) view.findViewById(R.id.password);
        mPasswordConfirmEditText = (EditText) view.findViewById(R.id.password_confirm);

        getSpiceManager().execute(new CountriesRequest(), "CountriesRequest", DurationInMillis.ONE_HOUR, new CountryRequestListener());

        mAppUser = AppUser.getUser();

        fillProfile(view, mAppUser);

        getSpiceManager().execute(new GetUserRequest().setUid(mAppUser.getUid()), new GetUserRequestListener());

        view.findViewById(R.id.action_save).setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_save:
                saveProfile();
                break;
        }
    }

    /*
    * runs validator and send profile information to server if validation succeed
     */
    public void saveProfile() {
        //init validator
        mValidator = new Validator(this);
        mValidator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                View v = getView();

                EditProfileRequest editProfileRequest = new EditProfileRequest(mAppUser.getUid(), v);

                Spinner spinner = (Spinner) v.findViewById(R.id.country);
                if (spinner != null) {
                    Country country = (Country) spinner.getSelectedItem();
                    if (country != null && country.getId() != -1) {
                        editProfileRequest.setCountry(country.getId());
                    }
                }
                getSpiceManager().execute(editProfileRequest, new SaveProfileRequestListener());

                DialogUtil.show(getFragmentManager(), new ProgressDialog());
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                for (ValidationError error : errors) {
                    View view = error.getView();
                    String message = error.getCollatedErrorMessage(getActivity());

                    if (view instanceof EditText) {
                        ((EditText) view).setError(message);
                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        if (mPasswordEditText.getText().length() == 0) {
            mValidator.removeRules(mPasswordEditText);
            mValidator.removeRules(mPasswordConfirmEditText);
        }

        mValidator.validate();
    }

    public void fillProfile(View view, AppUser user) {
        UiUtil.setTextValue(view, R.id.nickname, user.getLogin());
        UiUtil.setTextValue(view, R.id.email, user.getEmail());

        UiUtil.setTextValue(view, R.id.first_name, user.getFirstName());
        UiUtil.setTextValue(view, R.id.last_name, user.getLastName());
        UiUtil.setTextValue(view, R.id.zip_code, user.getZip());
        UiUtil.setTextValue(view, R.id.street, user.getStreet());
        UiUtil.setTextValue(view, R.id.house_number, user.getHouse());
        UiUtil.setTextValue(view, R.id.city, user.getCity());

        Spinner spinner = (Spinner) view.findViewById(R.id.country);

        if (mAppUser.getCountry() != 0 && spinner.getAdapter() != null) {
            for (int i = 0; i < spinner.getAdapter().getCount(); i++) {
                if (((Country) spinner.getAdapter().getItem(i)).getId() == mAppUser.getCountry()) {
                    spinner.setSelection(i);
                    return;
                }
            }
        }
    }

    @Override
    protected void setTitle() {
        getTitledActivity().setTitle(0,
                new MenuItem(R.id.drawer_menu_item_my_profile, R.string.view_my_profile),
                new MenuItem(R.id.drawer_menu_item_interests, R.string.interests));
    }

    @Override
    protected int getMenuBindId() {
        return R.id.drawer_menu_item_my_profile;
    }

    ///----
    private class GetUserRequestListener implements RequestListener<AppUser> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {

        }

        @Override
        public void onRequestSuccess(AppUser appUser) {
            mAppUser = appUser;
            AppUser.setUser(appUser);
            fillProfile(getView(), appUser);
        }
    }

    private class CountryRequestListener implements RequestListener<Country.CountryList> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Toast.makeText(getActivity(), "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Country.CountryList countries) {
            Country country = new Country(-1, getActivity().getString(R.string.country));
            countries.add(0, country);

            Spinner spinner = (Spinner) getView().findViewById(R.id.country);
            spinner.setAdapter(new CountryAdapter(getActivity(), countries));

            if (mAppUser.getCountry() != 0) {
                for (int i = 0; i < countries.size(); i++) {
                    if (countries.get(i).getId() == mAppUser.getCountry()) {
                        spinner.setSelection(i);
                        return;
                    }
                }
            }
        }
    }

    public class SaveProfileRequestListener implements RequestListener<Object> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            ((BaseSpiceFragmentActivity) getActivity()).obtainServerResponseError(spiceException);
            DialogUtil.cancel(getFragmentManager());
        }

        @Override
        public void onRequestSuccess(Object o) {
            DialogUtil.cancel(getFragmentManager());
            //getActivity().onBackPressed();
        }
    }
}
