package com.uran.magazines4free.fragments.overlay;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.uran.magazines4free.R;
import com.uran.magazines4free.api.requests.BaseRequestListener;
import com.uran.magazines4free.api.requests.SearchMagazinesRequest;
import com.uran.magazines4free.behaviour.OnMagazineSelectListener;
import com.uran.magazines4free.fragments.base.BaseSpiceFragment;
import com.uran.magazines4free.model.Magazine;
import com.uran.magazines4free.utils.UiUtil;
import com.uran.magazines4free.widgets.lists.BaseHorizontalListView;
import com.uran.magazines4free.widgets.lists.HorizontalListView;

/**
 * Created by ovitali on 03.04.2015.
 * Project is Magazines4Free
 */
public class SearchFragment extends BaseSpiceFragment implements View.OnClickListener, BaseHorizontalListView.OnItemClickListener {

    private EditText mSearchTextView;
    private HorizontalListView mHorizontalListView;

    private OnMagazineSelectListener mOnMagazineSelectListener;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_search, container, false);

        view.findViewById(R.id.search_result_container).setVisibility(View.GONE);

        mSearchTextView = (EditText) view.findViewById(R.id.search_text);

        view.findViewById(R.id.search_button).setOnClickListener(this);

        mHorizontalListView = (HorizontalListView) view.findViewById(R.id.horizontal_list_view);
        mHorizontalListView.setOnItemClickListener(this);

        view.findViewById(R.id.search_close_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mOnMagazineSelectListener = (OnMagazineSelectListener) activity;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_button:
                UiUtil.hideKeyboard(mSearchTextView);
                String query = mSearchTextView.getText().toString();
                getSpiceManager().execute(new SearchMagazinesRequest(query), new SearchRequestListener());
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        UiUtil.hideKeyboard(mSearchTextView);
    }

    @Override
    public void onItemClick(Magazine magazine) {
        getActivity().onBackPressed();
        mOnMagazineSelectListener.onMagazineSelected(magazine);
    }

    //--------------------------------
    private class SearchRequestListener extends BaseRequestListener<Magazine[]> {
        @Override
        public void onRequestSuccess(Magazine[] magazines) {
            if (magazines != null && magazines.length > 0) {
                mHorizontalListView.setItems(magazines);

                View v = getView();
                if (v != null)
                    v.findViewById(R.id.search_result_container).setVisibility(View.VISIBLE);
            }
        }
    }
}
