package com.uran.magazines4free.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uran.magazines4free.R;
import com.uran.magazines4free.fragments.base.BaseMenuBindSpiceFragment;

/**
 * Created by ovitali on 21.04.2015.
 * Project is Magazines4Free
 */
public class NotificationSettingsFragment extends BaseMenuBindSpiceFragment {

    public static NotificationSettingsFragment newInstance() {
        return new NotificationSettingsFragment();
    }

    @Override
    protected void setTitle() {
        getTitledActivity().setTitle(R.string.drawer_menu_item_notifications);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification_settings, container, false);
        return view;
    }

    @Override
    protected int getMenuBindId() {
        return R.id.drawer_menu_item_notifications;
    }
}
