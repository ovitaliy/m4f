package com.uran.magazines4free.fragments.intro;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.uran.magazines4free.Constants;
import com.uran.magazines4free.R;
import com.uran.magazines4free.activity.IntroActivity;
import com.uran.magazines4free.adapter.CategoryIntroAdapter;
import com.uran.magazines4free.dataSource.DatabaseManager;
import com.uran.magazines4free.dataSource.loaders.CategoriesDataSource;
import com.uran.magazines4free.model.Category;

import java.util.List;


public class Intro4Fragment extends Fragment implements View.OnClickListener,
        LoaderManager.LoaderCallbacks<List<Category>> {

    public static Intro4Fragment newInstance() {
        Intro4Fragment fragment = new Intro4Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private ListView mCategoryListView;
    private CategoryIntroAdapter mCategoryIntroAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_intro_4, container, false);

        getLoaderManager().initLoader(Constants.LOADERS.CATEGORY, null, this);

        mCategoryListView = (ListView) view.findViewById(R.id.categories_list);
        mCategoryListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);

        mCategoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mCategoryIntroAdapter.selectItem(position, view);
            }
        });

        view.findViewById(R.id.btn_next).setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View v) {
        try {
            DatabaseManager.getInstance().getHelper().getCategoryDao().updateAsync(mCategoryIntroAdapter.getAll());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        ((IntroActivity) getActivity()).next(v);
    }

    @Override
    public Loader<List<Category>> onCreateLoader(int id, Bundle args) {
        return new CategoriesDataSource(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<List<Category>> loader, List<Category> data) {
        mCategoryIntroAdapter = new CategoryIntroAdapter(getActivity(), data);
        mCategoryListView.setAdapter(mCategoryIntroAdapter);
    }

    @Override
    public void onLoaderReset(Loader<List<Category>> loader) {
    }
}
