package com.uran.magazines4free.fragments.base;

import com.octo.android.robospice.SpiceManager;
import com.uran.magazines4free.api.ApiSpiceService;
import com.uran.magazines4free.behaviour.OnMenuBingFragmentStarted;

import de.greenrobot.event.EventBus;

/**
 * Created by ovitali on 12.03.2015.
 */
public abstract class BaseMenuBindSpiceFragment extends BaseTitleFragment {

    private SpiceManager spiceManager = new SpiceManager(ApiSpiceService.class);

    protected abstract int getMenuBindId();

    @Override
    public void onStart() {
        spiceManager.start(getActivity());
        super.onStart();

        sendEventOnFragmentStart();
    }

    protected void sendEventOnFragmentStart() {
        EventBus.getDefault().post(new OnMenuBingFragmentStarted(getMenuBindId()));
    }

    @Override
    public void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    public SpiceManager getSpiceManager() {
        return spiceManager;
    }
}
