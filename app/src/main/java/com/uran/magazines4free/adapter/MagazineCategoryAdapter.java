package com.uran.magazines4free.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.uran.magazines4free.model.Category;
import com.uran.magazines4free.utils.ConverterUtil;
import com.uran.magazines4free.widgets.items.CategoryItemViewHolder;

import java.util.List;

/**
 * Created by ovitali on 03.03.2015.
 */
public class MagazineCategoryAdapter extends BaseCategoryAdapter {

    int rightMargin;

    public MagazineCategoryAdapter(Context context, List<Category> list, View.OnClickListener onClickListener) {
        super(context, list, onClickListener);
        rightMargin = (int) ConverterUtil.dpToPix(context, 5);
    }

    @Override
    CategoryItemViewHolder createCategoryItemViewHolder(ViewGroup parent) {
        CategoryItemViewHolder viewHolder = CategoryItemViewHolder.createSmall(layoutInflater, parent);

        ViewGroup.MarginLayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.MarginLayoutParams.WRAP_CONTENT, ViewGroup.MarginLayoutParams.WRAP_CONTENT);
        lp.setMargins(0, 0, rightMargin, 0);

        viewHolder.getView().setLayoutParams(lp);

        return viewHolder;
    }
}
