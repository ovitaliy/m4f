package com.uran.magazines4free.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.uran.magazines4free.R;
import com.uran.magazines4free.model.Interest;

import java.util.List;

/**
 * Created by ovitali on 03.03.2015.
 */
public class InterestAdapter extends BaseSelectableArrayAdapter<Interest> {

    public InterestAdapter(Context context, List<Interest> list) {
        super(context, list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder viewHolder;

        if (convertView == null) {
            view = layoutInflater.inflate(R.layout.item_category, parent, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Interest item = getItem(position);

        boolean selected = item.isSelected();
        viewHolder.label.setSelected(selected);
        viewHolder.label.setText(item.getName());
        viewHolder.checkbox.setVisibility(selected ? View.VISIBLE : View.GONE);

        ImageLoader.getInstance().displayImage(item.getIcon(), viewHolder.icon);

        return view;
    }

    public void selectItem(int position, View view) {
        Interest category = getItem(position);
        boolean selected = !category.isSelected();
        category.setSelected(selected);

        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.label.setSelected(selected);
        viewHolder.checkbox.setVisibility(selected ? View.VISIBLE : View.GONE);
    }

    private class ViewHolder {
        TextView label;
        ImageView icon;
        ImageView checkbox;

        public ViewHolder(View view) {
            label = (TextView) view.findViewById(R.id.label);
            icon = (ImageView) view.findViewById(R.id.icon);
            checkbox = (ImageView) view.findViewById(R.id.checkbox);
        }
    }
}
