package com.uran.magazines4free.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.uran.magazines4free.model.Magazine;
import com.uran.magazines4free.widgets.items.MagazineView;

import java.util.ArrayList;

/**
 * Created by ovitali on 19.02.2015.
 */
public class MagazinesAdapter extends BaseArrayAdapter<Magazine> {


    public MagazinesAdapter(Context context, ArrayList<Magazine> list) {
        super(context, list);
    }
    public MagazinesAdapter(Context context) {
        super(context, new ArrayList<Magazine>());
    }

    @Override
    public MagazineView getView(int position, View convertView, ViewGroup parent) {
        MagazineView magazineView;
        if (convertView == null) {
            magazineView = new MagazineView(context);
        } else {
            magazineView = (MagazineView) convertView;
        }
        magazineView.setData(position, getItem(position));

        return magazineView;
    }
}
