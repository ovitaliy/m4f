package com.uran.magazines4free.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

import com.uran.magazines4free.model.BaseModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by ovitali on 03.03.2015.
 */
public abstract class BaseArrayAdapter<T extends BaseModel> extends BaseAdapter {

    protected List<T> mList;
    protected LayoutInflater layoutInflater;
    protected Context context;

    public BaseArrayAdapter(Context context, List<T> list) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public T getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    public List<T> getAll() {
        return mList;
    }

    public void add(List<T> newItems) {
        mList.addAll(newItems);
        notifyDataSetChanged();
    }

    public void add(T[] newItems) {
        Collections.addAll(mList, newItems);
        notifyDataSetChanged();
    }
}
