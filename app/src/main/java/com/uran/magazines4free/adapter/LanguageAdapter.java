package com.uran.magazines4free.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uran.magazines4free.R;
import com.uran.magazines4free.model.Language;

import java.util.List;

/**
 * Created by ovitali on 03.03.2015.
 */
public class LanguageAdapter extends BaseSelectableArrayAdapter<Language> {

    public LanguageAdapter(Context context, List<Language> list) {
        super(context, list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder viewHolder;

        if (convertView == null) {
            view = layoutInflater.inflate(R.layout.item_language, parent, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Language item = getItem(position);

        boolean selected = item.isSelected();
        viewHolder.label.setSelected(selected);
        viewHolder.label.setText(item.getName());

        return view;
    }

    public void selectItem(int position, View view) {
        Language category = getItem(position);
        boolean selected = !category.isSelected();
        category.setSelected(selected);

        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.label.setSelected(selected);
    }

    private class ViewHolder {
        TextView label;

        public ViewHolder(View view) {
            label = (TextView) view.findViewById(R.id.label);
        }
    }
}
