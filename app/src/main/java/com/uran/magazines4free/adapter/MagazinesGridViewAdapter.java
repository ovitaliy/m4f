package com.uran.magazines4free.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.uran.magazines4free.MagazineApplication;
import com.uran.magazines4free.model.Magazine;
import com.uran.magazines4free.utils.ConverterUtil;
import com.uran.magazines4free.widgets.items.MagazineView;

import java.util.ArrayList;

/**
 * Created by ovitali on 19.02.2015.
 */
public class MagazinesGridViewAdapter extends MagazinesAdapter {

    private int mLeftItemPadding;
    private int mHorizontalSpacing;

    public MagazinesGridViewAdapter(Context context, ArrayList<Magazine> list) {
        super(context, list);
    }

    public MagazinesGridViewAdapter(Context context) {
        super(context);
        mHorizontalSpacing = (int) ConverterUtil.dpToPix(context, 15);
        mLeftItemPadding = (MagazineApplication.SCREEN_WIDTH
                - MagazineView.ITEM_WIDTH * 5
                - mHorizontalSpacing * 4
        ) / 2;
    }

    @Override
    public MagazineView getView(int position, View convertView, ViewGroup parent) {
        MagazineView magazineView = super.getView(position, convertView, parent);
        magazineView.setX(mLeftItemPadding
                        + (MagazineApplication.SCREEN_WIDTH + mHorizontalSpacing) * position % 5

        );
        return magazineView;
    }
}
