package com.uran.magazines4free.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uran.magazines4free.model.Category;
import com.uran.magazines4free.widgets.items.CategoryItemViewHolder;

import java.util.List;

/**
 * Created by ovitali on 03.03.2015.
 */
public abstract class BaseCategoryAdapter extends RecyclerView.Adapter<CategoryItemViewHolder> {

    protected LayoutInflater layoutInflater;
    private List<Category> mList;
    private View.OnClickListener mOnClickListener = null;

    public BaseCategoryAdapter(Context context, List<Category> list, View.OnClickListener onClickListener) {
        layoutInflater = LayoutInflater.from(context);
        mList = list;
        mOnClickListener = onClickListener;
    }


    public void selectItem(int position, View view) {
        Category category = mList.get(position);
        boolean selected = !category.isSelected();
        category.setSelected(selected);

        CategoryItemViewHolder viewHolder = (CategoryItemViewHolder) view.getTag();
        viewHolder.select(selected);
    }

    @Override
    public CategoryItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CategoryItemViewHolder viewHolder = createCategoryItemViewHolder(parent);
        viewHolder.getView().setOnClickListener(mOnClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CategoryItemViewHolder holder, int position) {
        holder.setData(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    // -------------
    abstract CategoryItemViewHolder createCategoryItemViewHolder(ViewGroup parent);
}
