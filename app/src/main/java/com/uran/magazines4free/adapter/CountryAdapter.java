package com.uran.magazines4free.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uran.magazines4free.R;
import com.uran.magazines4free.model.Country;

import java.util.List;

/**
 * Created by ovitali on 12.03.2015.
 */
public class CountryAdapter extends BaseArrayAdapter<Country> {
    public CountryAdapter(Context context, List<Country> list) {
        super(context, list);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return createView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = createView(position, convertView, parent);
        if (position == 0)
            ((TextView) convertView).setTextColor(convertView.getContext().getResources().getColor(R.color.gray));
        return convertView;
    }

    private View createView(int position, View convertView, ViewGroup parent) {
        Country country = getItem(position);

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_country, parent, false);
        }

        ((TextView) convertView).setText(country.getName());

        return convertView;
    }
}
