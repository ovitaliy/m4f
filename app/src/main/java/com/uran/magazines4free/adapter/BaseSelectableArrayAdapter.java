package com.uran.magazines4free.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

import com.uran.magazines4free.model.BaseSelectableModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ovitali on 03.03.2015.
 */
public abstract class BaseSelectableArrayAdapter<T extends BaseSelectableModel> extends BaseAdapter {

    protected List<T> mList;
    protected LayoutInflater layoutInflater;
    protected Context context;

    public BaseSelectableArrayAdapter(Context context, List<T> list) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        mList = list;

    }

    @Override
    public T getItem(int position) {
        return mList.get(position);
    }

    public List<T> getSelected() {
        List<T> list = new ArrayList<>();

        int n = getCount();

        for (int i = 0; i < n; i++) {
            if (getItem(i).isSelected())
                list.add(getItem(i));
        }

        return list;
    }

    public List<Integer> getSelectedIds() {
        List<T> selectedItems = getSelected();
        List<Integer> list = new ArrayList<>(selectedItems.size());

        for (T item : selectedItems) {
            list.add(item.getId());
        }

        return list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    public List<T> getAll() {
        return mList;
    }


}
