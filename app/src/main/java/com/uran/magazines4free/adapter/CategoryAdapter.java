package com.uran.magazines4free.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.uran.magazines4free.model.Category;
import com.uran.magazines4free.widgets.items.CategoryItemViewHolder;

import java.util.List;

/**
 * Created by ovitali on 03.03.2015.
 */
public class CategoryAdapter extends BaseCategoryAdapter {

    public CategoryAdapter(Context context, List<Category> list, View.OnClickListener onClickListener) {
        super(context, list, onClickListener);
    }

    @Override
    CategoryItemViewHolder createCategoryItemViewHolder(ViewGroup parent) {
        return CategoryItemViewHolder.create(layoutInflater, parent);
    }
}
