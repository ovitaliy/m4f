package com.uran.magazines4free.activity.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.uran.magazines4free.R;
import com.uran.magazines4free.fragments.auth.LoginFragment;
import com.uran.magazines4free.ui.ActionbarHelper;

/**
 * Created by ovitali on 27.02.2015.
 */
public class LoginActivity extends BaseAuthActivity {

    public static void startNewInstance(Context context) {
        context.startActivity(new Intent(context, LoginActivity.class));
    }

    private LoginFragment mLoginFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base_fragmen_container);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionbarHelper mActionbarHelper = new ActionbarHelper(this, mToolbar);
        mActionbarHelper.setTitle();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        try {
            android.content.pm.PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    android.content.pm.PackageManager.GET_SIGNATURES);
            String hashes = "";
            for (android.content.pm.Signature signature : info.signatures) {
                java.security.MessageDigest md = java.security.MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hash = android.util.Base64.encodeToString(md.digest(), android.util.Base64.DEFAULT);
                Log.d("KeyHash:", hash);
                hashes += hash + " : ";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        mLoginFragment = new LoginFragment();
        startFragment(mLoginFragment, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mLoginFragment.onActivityResult(requestCode, resultCode, data);
    }
}
