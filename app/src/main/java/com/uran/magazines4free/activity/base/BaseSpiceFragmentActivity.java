package com.uran.magazines4free.activity.base;

import android.os.Bundle;

import com.google.api.client.http.HttpResponseException;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.PendingRequestListener;
import com.uran.magazines4free.MagazineApplication;
import com.uran.magazines4free.R;
import com.uran.magazines4free.api.ApiSpiceService;
import com.uran.magazines4free.api.requests.GetCategoriesRequest;
import com.uran.magazines4free.api.requests.TokenRequest;
import com.uran.magazines4free.api.responses.TokenResponse;
import com.uran.magazines4free.dialogs.ErrorDialog;
import com.uran.magazines4free.model.AppUser;
import com.uran.magazines4free.model.Category;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by ovitali on 11.03.2015.
 */
public abstract class BaseSpiceFragmentActivity extends BaseFragmentActivity {

    private SpiceManager spiceManager = new SpiceManager(ApiSpiceService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSpiceManager().addListenerIfPending(TokenResponse.class, "token", new TokenRequestListener());
        if (MagazineApplication.AUTH_TOKEN == null) {
            if ((MagazineApplication.AUTH_TOKEN = AppUser.getToken()) == null)
                getSpiceManager().execute(new TokenRequest(), "token", DurationInMillis.ONE_SECOND, new TokenRequestListener());
        }

        if (MagazineApplication.SESSION == null) {
            MagazineApplication.SESSION = AppUser.getSession();
        }

        getSpiceManager().addListenerIfPending(Category.CategoryList.class, "categories", null);
    }

    @Override
    protected void onStart() {
        spiceManager.start(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    public void obtainServerResponseError(SpiceException spiceException) {
        if (spiceException.getCause() instanceof HttpResponseException) {
            HttpResponseException ex = (HttpResponseException) spiceException.getCause();

            String result = ex.getContent();

            try {
                JSONObject responseJson = new JSONObject(result);
                if (responseJson.has("error")) {
                    String packageName = getPackageName();
                    JSONArray errors = responseJson.getJSONArray("error");
                    for (int i = 0; i < errors.length(); i++) {
                        String error = "error_" + errors.getString(i);
                        int errorId = getResources().getIdentifier(error, "string", packageName);
                        if (errorId > 0)
                            ErrorDialog.show(this, errorId);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (spiceException.getCause() instanceof IOException) {
            IOException ex = (IOException) spiceException.getCause();

            String errorMessage = ex.getLocalizedMessage();
            if (errorMessage != null && "No authentication challenges found".equals(errorMessage)) {
                ErrorDialog.show(this, R.string.error_702_wrong_pass_name);
            }
        }
    }

    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }


    public final class TokenRequestListener implements PendingRequestListener<TokenResponse> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {

        }

        @Override
        public void onRequestSuccess(TokenResponse result) {
            MagazineApplication.AUTH_TOKEN = result.getToken();
            AppUser.setToken(result.getToken());

            getSpiceManager().execute(new GetCategoriesRequest(), "categories", DurationInMillis.ONE_HOUR, null);
        }

        @Override
        public void onRequestNotFound() {
        }
    }

}
