package com.uran.magazines4free.activity.auth;

import android.content.Intent;
import android.view.MenuItem;

import com.uran.magazines4free.activity.MainActivity;
import com.uran.magazines4free.activity.base.BaseSpiceFragmentActivity;
import com.uran.magazines4free.behaviour.OnAuthListener;
import com.uran.magazines4free.model.AppUser;

/**
 * Created by ovitali on 25.03.2015.
 */
public class BaseAuthActivity extends BaseSpiceFragmentActivity implements OnAuthListener {

    @Override
    public void onAuthComplete(AppUser appUser) {
        AppUser.setAuthorized(true);
        AppUser.setUser(appUser);

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
