package com.uran.magazines4free.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;
import com.uran.magazines4free.R;
import com.uran.magazines4free.activity.auth.LoginActivity;
import com.uran.magazines4free.activity.auth.RegistrationActivity;
import com.uran.magazines4free.activity.base.BaseSpiceFragmentActivity;
import com.uran.magazines4free.fragments.intro.Intro3Fragment;
import com.uran.magazines4free.fragments.intro.Intro4Fragment;
import com.uran.magazines4free.fragments.intro.IntroFragment;
import com.uran.magazines4free.model.AppUser;
import com.uran.magazines4free.widgets.indicators.ViewPagerIndicator;


public class IntroActivity extends BaseSpiceFragmentActivity {

    private ViewPagerIndicator mViewPagerIndicator;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (AppUser.getAuthorized()) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
            return;
        }

        try {
            ProviderInstaller.installIfNeeded(this);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }


        setContentView(R.layout.activity_intro);

        mViewPagerIndicator = (ViewPagerIndicator) findViewById(R.id.view_pager_indicator);

        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mViewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                mViewPagerIndicator.setValue(position);
            }
        });

        TextView textView = ((TextView) findViewById(R.id.login));
        textView.setText(Html.fromHtml(getString(R.string.intro_footer_login)));

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.startNewInstance(IntroActivity.this);
            }
        });

    }

    public void next(View view) {
        AppUser.setStartScreenWasShown(true);
        startActivity(new Intent(this, MainActivity.class));
    }

    public void navigateToRegistration(View view) {
        RegistrationActivity.startNewInstance(this);
    }

    public void skipStep(View view) {
        mViewPager.setCurrentItem(3);
    }


    private class ViewPagerAdapter extends FragmentPagerAdapter {
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return IntroFragment.newInstance(R.layout.fragment_intro_1);
                case 1:
                    return IntroFragment.newInstance(R.layout.fragment_intro_2);
                case 2:
                    return Intro3Fragment.newInstance();
                case 3:
                    return Intro4Fragment.newInstance();

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

}
