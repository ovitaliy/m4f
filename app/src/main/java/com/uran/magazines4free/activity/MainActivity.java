package com.uran.magazines4free.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.octo.android.robospice.persistence.DurationInMillis;
import com.uran.magazines4free.MagazineApplication;
import com.uran.magazines4free.R;
import com.uran.magazines4free.activity.auth.LoginActivity;
import com.uran.magazines4free.activity.base.BaseSpiceFragmentActivity;
import com.uran.magazines4free.api.requests.GetCategoriesRequest;
import com.uran.magazines4free.api.requests.LogoutRequest;
import com.uran.magazines4free.behaviour.ITitledActivity;
import com.uran.magazines4free.behaviour.OnCategorySelectListener;
import com.uran.magazines4free.behaviour.OnMagazineSelectListener;
import com.uran.magazines4free.behaviour.OnMenuSelectionListener;
import com.uran.magazines4free.behaviour.OnOrderChangeListener;
import com.uran.magazines4free.fragments.CategoriesTitleFragment;
import com.uran.magazines4free.fragments.EditProfileFragment;
import com.uran.magazines4free.fragments.InterestsFragment;
import com.uran.magazines4free.fragments.NavigationDrawerFragment;
import com.uran.magazines4free.fragments.NotificationSettingsFragment;
import com.uran.magazines4free.fragments.StartFragment;
import com.uran.magazines4free.fragments.ViewCategoryFragment;
import com.uran.magazines4free.fragments.base.BaseShareFragment;
import com.uran.magazines4free.fragments.magazine.ReaderFragment;
import com.uran.magazines4free.fragments.magazine.ViewMagazineFragment;
import com.uran.magazines4free.fragments.overlay.SearchFragment;
import com.uran.magazines4free.model.AppUser;
import com.uran.magazines4free.model.Magazine;
import com.uran.magazines4free.model.OrderType;
import com.uran.magazines4free.ui.ActionbarHelper;
import com.uran.magazines4free.widgets.popup.PopupHelper;
import com.uran.magazines4free.widgets.popup.SelectOrderPopup;
import com.uran.magazines4free.widgets.popup.SharePopup;

public class MainActivity extends BaseSpiceFragmentActivity
        implements ITitledActivity,
        OnMenuSelectionListener,
        OnMagazineSelectListener,
        OnCategorySelectListener,
        Toolbar.OnMenuItemClickListener {

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private ActionbarHelper mActionbarHelper;
    private Toolbar mToolbar;

    private Fragment mSearchFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.inflateMenu(R.menu.menu_main);

        mActionbarHelper = new ActionbarHelper(this, mToolbar);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        startFragment(StartFragment.newInstance(), false);

        getSpiceManager().execute(new GetCategoriesRequest(), "CategoriesRequest", DurationInMillis.ALWAYS_EXPIRED, null);
    }

    @Override
    public void onMenuSelected(int id) {
        removeSearchFragment();
        mNavigationDrawerFragment.mDrawerLayout.closeDrawers();
        switch (id) {
            case R.id.drawer_menu_item_start:
                startFragment(StartFragment.newInstance(), true);
                break;

            case R.id.drawer_menu_item_login:
                LoginActivity.startNewInstance(this);
                break;
            case R.id.drawer_menu_item_logout:
                logout();
                break;

            case R.id.drawer_menu_item_categories:
                startFragment(CategoriesTitleFragment.newInstance(), true);
                break;

            case R.id.drawer_menu_item_my_profile:
                startFragment(EditProfileFragment.newInstance(), true, true);
                break;
            case R.id.drawer_menu_item_interests:
                startFragment(InterestsFragment.newInstance(), true, true);
                break;

            case R.id.drawer_menu_item_notifications:
                startFragment(NotificationSettingsFragment.newInstance(), true);
                break;

            case R.id.drawer_menu_item_search:
                openSearchFragment();
                break;

           /* case R.id.drawer_menu_my_list:
                startFragment(MainListsFragment.newInstance(), true);
                break;



            case R.id.drawer_menu_categories:
            case R.id.drawer_menu_themes:
                startFragment(DummyFragment.newInstance(R.drawable.dummy_categories), true);
                break;



            case R.id.drawer_menu_contact_us:
            case R.id.drawer_menu_contacts:
                startFragment(DummyFragment.newInstance(R.drawable.dummy_contact_us), true);
                break;*/
        }
    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen()) {
            mNavigationDrawerFragment.mDrawerLayout.closeDrawers();
        } else if (!removeSearchFragment()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (currentFragment instanceof BaseShareFragment) {
            currentFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void logout() {
        AppUser.setAuthorized(false);
        AppUser.setUser(null);
        AppUser.setToken(null);
        AppUser.setSession(null);

        MagazineApplication.getSpiceManager().execute(new LogoutRequest(MagazineApplication.SESSION, MagazineApplication.AUTH_TOKEN), null);

        MagazineApplication.SESSION = null;
        MagazineApplication.AUTH_TOKEN = null;

        finish();
        Intent intent = new Intent(this, IntroActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.menu_main, menu);

        mActionbarHelper.changeSizes(menu.size());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mNavigationDrawerFragment.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.action_search:
                openSearchFragment();
                break;

            case R.id.action_order:
                SelectOrderPopup.create(this, mToolbar, new OnOrderChangeListener() {
                    @Override
                    public void onOrderChanged(String order) {
                        OrderType.setOrder(order);
                    }
                });
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMagazineSelected(Magazine magazine) {
        startFragment(ViewMagazineFragment.newInstance(magazine.getId()), true);
    }

    @Override
    public void onViewMagazineSelected(Magazine magazine) {
        startFragment(ReaderFragment.newInstance(magazine), true);
    }

    @Override
    public void onSelectCategory(int id, String name) {
        startFragment(ViewCategoryFragment.newInstance(id, name), true);
    }

    private Fragment startOverlayFragment(Fragment fragment) {
        return startFragment(R.id.overlayContainer, fragment, false, false);
    }

    // set title ----------------->>>
    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        mActionbarHelper.setTitle(title);
    }

    @Override
    public void setTitle(int title) {
        super.setTitle(title);
        mActionbarHelper.setTitle(title);
    }

    @Override
    public void setTitle(int selectedPosition, com.uran.magazines4free.model.MenuItem... items) {
        mActionbarHelper.setTitle(selectedPosition, items);
    }

    @Override
    public void setTitle() {
        mActionbarHelper.setTitle();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearchFragment();
                return true;
        }

        return false;
    }


    // set title <<<-----------------

    private void openSearchFragment() {
        if (mSearchFragment == null) {
            mSearchFragment = startOverlayFragment(new SearchFragment());
        }
    }

    /**
     * @return true if search was open
     */
    private boolean removeSearchFragment() {
        if (mSearchFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mSearchFragment).commit();
            mSearchFragment = null;
            return true;
        }

        return false;
    }
}

