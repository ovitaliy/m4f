package com.uran.magazines4free.activity.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.uran.magazines4free.R;
import com.uran.magazines4free.fragments.auth.RegistrationFragment;
import com.uran.magazines4free.ui.ActionbarHelper;

/**
 * Created by ovitali on 27.02.2015.
 */
public class RegistrationActivity extends BaseAuthActivity {

    public static void startNewInstance(Context context) {
        context.startActivity(new Intent(context, RegistrationActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_fragmen_container);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionbarHelper mActionbarHelper = new ActionbarHelper(this, mToolbar);
        mActionbarHelper.setTitle();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        startFragment(RegistrationFragment.newInstance(), false);
    }


}
