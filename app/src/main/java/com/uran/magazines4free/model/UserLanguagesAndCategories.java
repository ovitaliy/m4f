package com.uran.magazines4free.model;

import com.google.api.client.util.Key;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ovitali on 20.04.2015.
 * Project is Magazines4Free
 */
public class UserLanguagesAndCategories {


    @SerializedName("interests")
    @Key("interests")
    List<Integer> categories;

    @Key
    List<Integer> languages;

    public List<Integer> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Integer> languages) {
        this.languages = languages;
    }

    public List<Integer> getCategories() {
        return categories;
    }

    public void setCategories(List<Integer> categories) {
        this.categories = categories;
    }
}
