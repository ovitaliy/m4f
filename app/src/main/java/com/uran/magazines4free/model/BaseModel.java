package com.uran.magazines4free.model;

import com.google.api.client.util.Key;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

/**
 * Created by ovitali on 03.03.2015.
 */
public class BaseModel implements Serializable{

    @SerializedName("tid")
    @Key("tid")
    @DatabaseField(id = true, columnName = "_id")
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
