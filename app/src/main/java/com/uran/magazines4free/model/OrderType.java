package com.uran.magazines4free.model;

import com.uran.magazines4free.R;
import com.uran.magazines4free.utils.PrefHelper;

/**
 * Created by ovitali on 12.05.2015.
 * Project is Magazines4Free
 */
public class OrderType {
    private static final String ORDER = "Order";

    public static final String DATE = "date";
    public static final String BEST = "best";
    public static final String TITLE = "title";


    public static OrderItem[] getOrdersList() {
        String current =  getOrder();

        return new OrderItem[]{
                new OrderItem(TITLE, R.string.order_alphabet, TITLE.equals(current)),
                new OrderItem(BEST, R.string.order_rating, BEST.equals(current)),
                new OrderItem(DATE, R.string.order_date, DATE.equals(current)),
        };
    }

    public static String getOrder() {
        return PrefHelper.getStringPref(ORDER, DATE);
    }

    public static void setOrder(String order) {
        PrefHelper.setStringPref(ORDER, order);
    }

    public static class OrderItem {
        String type;
        int title;
        boolean selected;

        public OrderItem(String type, int title, boolean selected) {
            this.type = type;
            this.title = title;
            this.selected = selected;
        }

        public String getType() {
            return type;
        }

        public int getTitle() {
            return title;
        }

        public boolean isSelected() {
            return selected;
        }
    }


}
