package com.uran.magazines4free.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.api.client.util.Key;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by ovitali on 18.02.2015.
 */
public class Magazine extends BaseModel implements Parcelable {

    public Magazine() {
    }

    @SerializedName("nid")
    @Key("nid")
    @DatabaseField(id = true, columnName = "_id")
    int id;

    @Key("name")
    private String name;

    @Key("rating")
    private Rating rating;

    @Key("image")
    @SerializedName("image")
    private String previewImageUrl;

    @Key("categories")
    private Category[] categories;


    @Key("language_magazine")
    @SerializedName("language_magazine")
    private Language[] languages;

    @Key("body")
    @SerializedName("body")
    private String description;

    @Key("date_posted")
    @SerializedName("date_posted")
    private long date;

    @Key("authors")
    @SerializedName("authors")
    private String author;

    @Key
    private Tag[] tags;

    @Key("link")
    @SerializedName("link")
    private String originalUrl;

    @Key
    private String publisher;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Category[] getCategories() {
        return categories;
    }

    public void setCategories(Category[] categories) {
        this.categories = categories;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Tag[] getTags() {
        return tags;
    }

    public void setTags(Tag[] tags) {
        this.tags = tags;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Rating getRating() {
        if (rating == null) {
            return new Rating();
        }
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public String getPreviewImageUrl() {
        return previewImageUrl;
    }

    public void setPreviewImageUrl(String previewImageUrl) {
        this.previewImageUrl = previewImageUrl;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Language[] getLanguages() {
        return languages;
    }

    public void setLanguages(Language[] languages) {
        this.languages = languages;
    }

    //------------------- Parcelable -------------------
    public Magazine(Parcel parcel) {
        id = parcel.readInt();
        rating = parcel.readParcelable(Rating.class.getClassLoader());
        previewImageUrl = parcel.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeParcelable(rating, flags);
        dest.writeString(previewImageUrl);
    }

    public static final Parcelable.Creator<Magazine> CREATOR = new Parcelable.Creator<Magazine>() {
        public Magazine createFromParcel(Parcel in) {
            return new Magazine(in);
        }

        public Magazine[] newArray(int size) {
            return new Magazine[size];
        }
    };
}
