package com.uran.magazines4free.model;

import android.net.Uri;

import com.google.api.client.util.Key;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.uran.magazines4free.dataSource.ContentProvider;

/**
 * Created by ovitali on 03.03.2015.
 */

@DatabaseTable
public class Language extends BaseSelectableModel {

    public static final String TABLE_NAME = "Language";
    public static final int CODE_ALL = 100;
    public static final int CODE_ID = 101;
    public static final Uri URI = ContentProvider.URI.buildUpon().appendPath(TABLE_NAME).build();

    @Key
    @DatabaseField(columnDefinition = "name")
    String name;

    @DatabaseField(columnName = "selected")
    boolean selected;

    public Language() {
    }

    public Language(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
