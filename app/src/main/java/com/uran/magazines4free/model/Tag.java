package com.uran.magazines4free.model;

import com.google.api.client.util.Key;

/**
 * Created by ovitali on 25.05.2015.
 * Project is Magazines4Free
 */
public class Tag extends BaseModel {

    @Key
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
