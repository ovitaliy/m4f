package com.uran.magazines4free.model;

import android.net.Uri;

import com.google.api.client.util.Key;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.uran.magazines4free.dataSource.ContentProvider;


/**
 * Created by ovitali on 03.03.2015.
 */

@DatabaseTable
public class Interest extends BaseSelectableModel {

    public static final String TABLE_NAME = "Interest";
    public static final int CODE_ALL = 100;
    public static final int CODE_ID = 101;
    public static final Uri URI = ContentProvider.URI.buildUpon().appendPath(TABLE_NAME).build();

    @DatabaseField
    @Key
    String name;

    @DatabaseField
    @SerializedName("field_logo_term")
    @Key(value = "field_logo_term")
    String icon;

    @DatabaseField
    boolean selected;

    public Interest() {
    }

    public Interest(int id, String name, String icon) {
        this.id = id;
        this.name = name;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
