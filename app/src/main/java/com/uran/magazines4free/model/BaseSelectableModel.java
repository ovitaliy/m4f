package com.uran.magazines4free.model;

import com.uran.magazines4free.behaviour.ISelectableItem;

/**
 * Created by ovitali on 03.03.2015.
 */
public abstract class BaseSelectableModel extends BaseModel implements ISelectableItem {

}
