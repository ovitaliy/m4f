package com.uran.magazines4free.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.api.client.util.Key;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ovitali on 15.05.2015.
 * Project is Magazines4Free
 */
public class Rating implements Parcelable {

    public Rating() {
    }

    @Key("count")
    @SerializedName("count")
    int votes;

    @Key("stars")
    @SerializedName("stars")
    int rating;

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }


    //------------------- Parcelable -------------------
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(rating);
        dest.writeInt(votes);
    }

    private Rating(Parcel in) {
        rating = in.readInt();
        votes = in.readInt();
    }

    public static final Parcelable.Creator<Rating> CREATOR = new Parcelable.Creator<Rating>() {
        public Rating createFromParcel(Parcel in) {
            return new Rating(in);
        }

        public Rating[] newArray(int size) {
            return new Rating[size];
        }
    };
}
