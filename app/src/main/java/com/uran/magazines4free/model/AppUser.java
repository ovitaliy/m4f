package com.uran.magazines4free.model;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;
import com.uran.magazines4free.utils.PrefHelper;

import java.lang.reflect.Type;

/**
 * Created by ovitali on 25.03.2015.
 */

public class AppUser {

    public static final String START_SCREEN_WAS_SHOWN = "START_SCREEN_WAS_SHOWN";
    public static final String AUTHORIZED = "AUTHORIZED";
    private static final String USER = "user";
    private static final String SESSION_INFO = "session_info";
    private static final String TOKEN = "token";


    private int uid;

    @SerializedName("name")
    private String login;

    @SerializedName("mail")
    private String email;

    private String firstName;
    private String lastName;
    private String city;
    private int country;

    public int getHouse() {
        return house;
    }

    public void setHouse(int house) {
        this.house = house;
    }

    private int house;
    private int zip;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public int getCountry() {
        return country;
    }

    public void setCountry(int country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private String street;


    public AppUser() {
    }

    public AppUser(String login, String email) {
        this.login = login;
        this.email = email;
    }

    public AppUser(int uid, String login, String email) {
        this.uid = uid;
        this.login = login;
        this.email = email;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static class Deserializer implements JsonDeserializer<AppUser> {

        @Override
        public AppUser deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            AppUser appUser = new Gson().fromJson(json, AppUser.class);

            JsonObject jsonObject = json.getAsJsonObject();

            appUser.setFirstName(getStringValue(jsonObject, "field_first_name"));
            appUser.setLastName(getStringValue(jsonObject, "field_last_name"));
            appUser.setStreet(getStringValue(jsonObject, "field_street"));
            appUser.setCity(getStringValue(jsonObject, "field_city"));
            appUser.setZip(getIntValue(jsonObject, "field_zip"));
            appUser.setHouse(getIntValue(jsonObject, "field_number_house"));
            appUser.setCountry(getCountryValue(jsonObject, "field_country"));

            return appUser;
        }

        private String getStringValue(JsonObject json, String key) {
            if (!json.has(key))
                return null;

            if (!json.get(key).isJsonObject())
                return null;

            json = json.getAsJsonObject(key);

            if (!json.has("und"))
                return null;


            JsonArray jsonArray = json.getAsJsonArray("und");
            if (jsonArray.size() == 0) {
                return null;
            }

            json = jsonArray.get(0).getAsJsonObject();

            if (json.get("value").isJsonNull())
                return null;

            return json.get("value").getAsString();
        }

        private int getIntValue(JsonObject json, String key) {
            int val = 0;
            try {
                String value = getStringValue(json, key);
                if (value != null) {
                    val = Integer.parseInt(value);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return val;
        }

        private int getCountryValue(JsonObject json, String key) {
            if (!json.has(key))
                return 0;

            if (!json.get(key).isJsonObject())
                return 0;

            json = json.getAsJsonObject(key);

            if (!json.has("und"))
                return 0;


            JsonArray jsonArray = json.getAsJsonArray("und");
            if (jsonArray.size() == 0) {
                return 0;
            }

            json = jsonArray.get(0).getAsJsonObject();

            return json.get("tid").getAsInt();
        }

    }

    //---------------- preference
    public static void setAuthorized(boolean value) {
        PrefHelper.setBooleanPref(AUTHORIZED, value);
    }

    public static boolean getAuthorized() {
        return PrefHelper.getBooleanPref(AUTHORIZED);
    }

    public static void setStartScreenWasShown(boolean value) {
        PrefHelper.setBooleanPref(START_SCREEN_WAS_SHOWN, value);
    }

    public static boolean getStartScreenWasShown() {
        return PrefHelper.getBooleanPref(START_SCREEN_WAS_SHOWN);
    }

    public static AppUser getUser() {
        AppUser user = null;
        String userGson = PrefHelper.getStringPref(USER);
        if (userGson != null)
            user = new Gson().fromJson(userGson, AppUser.class);
        return user;
    }

    public static void setUser(AppUser user) {
        String userGson = new Gson().toJson(user);
        PrefHelper.setStringPref(USER, userGson);
    }

    public static String getSession() {
        return PrefHelper.getStringPref(SESSION_INFO);
    }

    public static void setSession(String session) {
        PrefHelper.setStringPref(SESSION_INFO, session);
    }

    public static String getToken() {
        return PrefHelper.getStringPref(TOKEN);
    }

    public static void setToken(String session) {
        PrefHelper.setStringPref(TOKEN, session);
    }
}
