package com.uran.magazines4free.model;

import com.google.api.client.util.Key;

import java.util.ArrayList;

/**
 * Created by ovitali on 11.03.2015.
 */
public class Country extends BaseModel {
    public Country() {
    }

    public Country(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Key
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static class CountryList extends ArrayList<Country> {
    }
}
