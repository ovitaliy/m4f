package com.uran.magazines4free.model;

/**
 * Created by ovitali on 25.02.2015.
 */
public class MenuItem extends BaseModel {
    private int iconId;
    private int titleId;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    private boolean selected;

    public MenuItem(int id, int iconId, int titleId) {
        this.id = id;
        this.iconId = iconId;
        this.titleId = titleId;
    }

    public MenuItem(int id, int titleId) {
        this.id = id;
        this.titleId = titleId;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public int getTitleId() {
        return titleId;
    }

    public void setTitleId(int titleId) {
        this.titleId = titleId;
    }
}
