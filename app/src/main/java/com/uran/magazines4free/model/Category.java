package com.uran.magazines4free.model;

import android.net.Uri;

import com.google.api.client.util.Key;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.uran.magazines4free.dataSource.ContentProvider;

import java.util.ArrayList;

/**
 * Created by ovitali on 03.03.2015.
 */

@DatabaseTable
public class Category extends BaseSelectableModel {

    public static final String TABLE_NAME = "Category";
    public static final int CODE_ALL = 100;
    public static final int CODE_ID = 101;
    public static final Uri URI = ContentProvider.URI.buildUpon().appendPath(TABLE_NAME).build();

    @DatabaseField(columnDefinition = "name")
    @Key
    String name;

    @DatabaseField(columnName = "icon")
    @Key("field_logo_term")
    @SerializedName("field_logo_term")
    String icon;

    @DatabaseField(columnName = "selected")
    boolean selected;

    public Category() {
    }

    public Category(int id, String name, String icon) {
        this.id = id;
        this.name = name;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public static class CategoryList extends ArrayList<Category> {
        public CategoryList() {
        }
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
