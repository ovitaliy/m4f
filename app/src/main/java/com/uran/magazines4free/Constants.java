package com.uran.magazines4free;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by ovitali on 18.02.2015.
 */
public class Constants {

    public static final int LIMIT = 30;


    public static final class SOCIAL_AUTH {
        // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
        public static final String TWITTER_KEY = "wBV6qvfNH0Ud5k1TS4zm7d6RJ";
        public static final String TWITTER_SECRET = "Lm1cZFFRogoZ8D4H0u041pc2LzNSQS5Hw12oCKDKh33wRr1anu";

        public static final String RELEASE_SHA1 = "B4:CF:93:2F:77:90:D2:26:42:DB:DB:43:DF:32:77:55:08:72:12:36";
    }

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());

    public static final class LOADERS {
        public static final int CATEGORY = 1;
        public static final int INTERESTS = 2;
        public static final int LANGUAGES = 3;
    }

    public static final class PARAMS {
        public static final String VIEW_ID = "view_id";
        public static final String CATEGORY_ID = "category_id";
        public static final String CATEGORY_NAME = "category_name";
        public static final String MAGAZINE = "magazine";
        public static final String MAGAZINE_LIST = "magazine_list";
        public static final String MAGAZINE_ID = "magazine_id";
        public static final String INTRO_VIEW_ID = "intro_view_id";

        public static final String ERROR = "error";
    }


    public static final class REQUESTS {
        public static final String BASE_URL = "http://lc-m4f.cloudapp.net/rest/";
        public static final String GET_USER = "user/";
        public static final String GET_MAGAZINE = "m4free_custom_node_description";
        public static final String GET_MAGAZINES_SAME = "m4free_custom_list_magazine_below_description";
        public static final String GET_MAGAZINES_LIST = "m4free_custom_node_list_by_category";
        public static final String SEARCH_MAGAZINES_LIST = "m4free_custom_search_magazine";
        public static final String GET_USER_CATEGORIES_AND_LANGUAGE = "m4free_custom_user_interes_lang";
        public static final String TAXONOMY_COUNTRIES = "m4free_custom_term";
        public static final String TAXONOMY_CATEGORIES = "m4free_custom_term";
        public static final String REGISTER = "user/register";
        public static final String EDIT_PROFILE = "user/";
        public static final String EDIT_USER_CATEGORIES_AND_LANGUAGES = "m4free_custom_user_update_interes_lang/";
        public static final String LOGIN = "user/login";
        public static final String LOGOUT = "user/logout";
        public static final String LOGIN_SOCIAL = "m4free_custom_user/retrieve_user_by_email";
        public static final String TOKEN = "user/token";

        public static final String RATE = "m4free_custom_set_rating_fivestar/";

        public static String getAbsoluteUrl(String url) {
            return BASE_URL + url;
        }
    }


}
