package com.uran.magazines4free.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.uran.magazines4free.R;

import java.util.List;

/**
 * Created by ovitali on 26.03.2015.
 */
public class TwitterGetEmailDialog extends DialogFragment implements Validator.ValidationListener {

    public static TwitterGetEmailDialog newInstance(OnTwitterEmailEnterListener onTwitterEmailEnterListener) {
        TwitterGetEmailDialog twitterGetEmailDialog = new TwitterGetEmailDialog();
        twitterGetEmailDialog.mOnTwitterEmailEnterListener = onTwitterEmailEnterListener;
        return twitterGetEmailDialog;
    }

    private OnTwitterEmailEnterListener mOnTwitterEmailEnterListener;

    @Email
    private EditText mEmailEditText;

    private Validator mValidator;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_twitter_enter_email, null);
        mEmailEditText = (EditText) view.findViewById(R.id.twitter_enter_email);
        view.findViewById(R.id.ok_button).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mValidator.validate();
            }
        });

        mValidator = new Validator(this);
        mValidator.setValidationListener(this);

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setCancelable(false)
                .create();
    }

    @Override
    public void onValidationSucceeded() {
        mOnTwitterEmailEnterListener.onTwitterEmailEntered(mEmailEditText.getText().toString());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }


    public interface OnTwitterEmailEnterListener {
        public void onTwitterEmailEntered(String email);
    }
}
