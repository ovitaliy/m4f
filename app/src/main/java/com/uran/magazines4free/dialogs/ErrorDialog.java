package com.uran.magazines4free.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.uran.magazines4free.R;

/**
 * Created by ovitali on 26.03.2015.
 */
public class ErrorDialog {

    public static void show(Context context, int errorId) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_error, null);
        TextView errorTextView = (TextView) view.findViewById(R.id.error_message);
        errorTextView.setText(errorId);

        final Dialog dialog = new AlertDialog.Builder(context)
                .setView(view)
                .setCancelable(true)
                .create();

        view.findViewById(R.id.ok_button).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


}
