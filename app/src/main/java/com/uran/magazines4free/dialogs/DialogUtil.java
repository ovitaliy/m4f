package com.uran.magazines4free.dialogs;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by ovitali on 26.03.2015.
 */
public class DialogUtil {

    public static void show(FragmentManager fragmentManager, DialogFragment dialogFragment) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        cancel(fragmentManager);
        try {
            dialogFragment.show(fragmentTransaction, "dialog");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void cancel(FragmentManager fragmentManager) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        DialogFragment prev = (DialogFragment) fragmentManager.findFragmentByTag("dialog");
        if (prev != null) {
            prev.dismiss();
            fragmentTransaction.remove(prev);
        }
        fragmentTransaction.addToBackStack(null);

    }
}
