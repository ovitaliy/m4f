package com.uran.magazines4free.api.requests;

import com.uran.magazines4free.Constants;
import com.uran.magazines4free.api.responses.LoginResponse;

/**
 * Created by ovitali on 13.03.2015.
 */
public class LoginSocialRequest extends BasePostRequest<LoginResponse> {

    public LoginSocialRequest() {
        super(LoginResponse.class);
    }

    public LoginSocialRequest setEmail(String value) {
        mParams.put("email", value);
        return this;
    }

    public LoginSocialRequest setName(String value) {
        mParams.put("first_name", value);
        return this;
    }

    @Override
    protected String getUrl() {
        return Constants.REQUESTS.LOGIN_SOCIAL;
    }
}
