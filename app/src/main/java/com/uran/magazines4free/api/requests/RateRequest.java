package com.uran.magazines4free.api.requests;

import com.uran.magazines4free.Constants;
import com.uran.magazines4free.model.AppUser;
import com.uran.magazines4free.model.Rating;

/**
 * Created by ovitali on 19.05.2015.
 * Project is Magazines4Free
 */
public class RateRequest extends BasePutRequest<Rating> {

    private int mMagazineId;

    public RateRequest(int magazineId, int vote) {
        super(Rating.class);
        mMagazineId = magazineId;
        mParams.put("uid", AppUser.getUser().getUid());
        mParams.put("vote", vote);
    }

    @Override
    protected String getUrl() {
        return Constants.REQUESTS.RATE + mMagazineId;
    }
}
