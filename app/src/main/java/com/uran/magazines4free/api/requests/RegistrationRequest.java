package com.uran.magazines4free.api.requests;

import android.view.View;

import com.uran.magazines4free.Constants;
import com.uran.magazines4free.api.responses.RegistrationResponse;

/**
 * Created by ovitali on 13.03.2015.
 */
public class RegistrationRequest extends BaseSaveProfileRequest<RegistrationResponse> {

    public RegistrationRequest(View view) {
        super(RegistrationResponse.class, view);
    }

    @Override
    protected String getUrl() {
        return Constants.REQUESTS.REGISTER;
    }
}
