package com.uran.magazines4free.api.responses;

/**
 * Created by ovitali on 25.03.2015.
 */
public class RegistrationResponse {

    String uid;

    public int getUid() {
        return Integer.parseInt(uid);
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
