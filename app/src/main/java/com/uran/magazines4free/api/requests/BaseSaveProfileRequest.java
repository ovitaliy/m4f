package com.uran.magazines4free.api.requests;

import android.view.View;

import com.uran.magazines4free.R;
import com.uran.magazines4free.utils.UiUtil;

/**
 * Created by ovitali on 10.04.2015.
 * Project is Magazines4Free
 */
public abstract class BaseSaveProfileRequest<T> extends BasePostRequest<T> {
    protected BaseSaveProfileRequest(Class<T> clazz, View v) {
        super(clazz);

        setLogin(UiUtil.getTextValue(v, R.id.nickname));
        setEmail(UiUtil.getTextValue(v, R.id.email));
        setPassword(UiUtil.getTextValue(v, R.id.password));

        setFirstName(UiUtil.getTextValue(v, R.id.first_name));
        setLastName(UiUtil.getTextValue(v, R.id.last_name));
        setHouse(UiUtil.getIntValue(v, R.id.house_number));
        setZip(UiUtil.getIntValue(v, R.id.zip_code));
        setCity(UiUtil.getTextValue(v, R.id.city));
        setStreet(UiUtil.getTextValue(v, R.id.street));
    }

    public BaseSaveProfileRequest setEmail(String value) {
        mParams.put("mail", value);
        return this;
    }

    public BaseSaveProfileRequest setPassword(String value) {
        mParams.put("pass", value);
        return this;
    }

    public BaseSaveProfileRequest setLogin(String value) {
        mParams.put("name", value);
        return this;
    }

    public BaseSaveProfileRequest setFirstName(String value) {
        mParams.put("field_first_name[und][0][value]", value);
        return this;
    }

    public BaseSaveProfileRequest setLastName(String value) {
        mParams.put("field_last_name[und][0][value]", value);
        return this;
    }

    public BaseSaveProfileRequest setStreet(String value) {
        mParams.put("field_street[und][0][value]", value);
        return this;
    }

    public BaseSaveProfileRequest setHouse(int value) {
        mParams.put("field_number_house[und][0][value]", String.valueOf(value));
        return this;
    }

    public BaseSaveProfileRequest setZip(int value) {
        mParams.put("field_zip[und][0][value]", String.valueOf(value));
        return this;
    }

    public BaseSaveProfileRequest setCity(String value) {
        mParams.put("field_city[und][0][value]", value);
        return this;
    }

    public BaseSaveProfileRequest setCountry(int value) {
        mParams.put("field_country[und][values]", String.valueOf(value));
        return this;
    }

}
