package com.uran.magazines4free.api.responses;

/**
 * Created by ovitali on 13.03.2015.
 */
public class TokenResponse {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
