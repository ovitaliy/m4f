package com.uran.magazines4free.api.requests;

import com.uran.magazines4free.Constants;
import com.uran.magazines4free.api.responses.TokenResponse;

/**
 * Created by ovitali on 13.03.2015.
 */
public class TokenRequest extends BasePostRequest<TokenResponse> {

    public TokenRequest() {
        super(TokenResponse.class);
    }

    protected String getUrl() {
        return Constants.REQUESTS.TOKEN;
    }

}
