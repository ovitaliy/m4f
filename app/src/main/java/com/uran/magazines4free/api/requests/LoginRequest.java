package com.uran.magazines4free.api.requests;

import com.uran.magazines4free.Constants;
import com.uran.magazines4free.api.responses.LoginResponse;

/**
 * Created by ovitali on 13.03.2015.
 */
public class LoginRequest extends BasePostRequest<LoginResponse> {

    public LoginRequest() {
        super(LoginResponse.class);
    }

    public LoginRequest setEmail(String value) {
        mParams.put("username", value);
        return this;
    }

    public LoginRequest setPassword(String value) {
        mParams.put("password", value);
        return this;
    }

    @Override
    protected String getUrl() {
        return Constants.REQUESTS.LOGIN;
    }
}
