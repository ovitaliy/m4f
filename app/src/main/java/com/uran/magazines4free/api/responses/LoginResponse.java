package com.uran.magazines4free.api.responses;

import com.google.gson.annotations.SerializedName;
import com.uran.magazines4free.model.AppUser;

/**
 * Created by ovitali on 25.03.2015.
 */
public class LoginResponse {

    String token;

    @SerializedName("sessid")
    String sessionId;

    @SerializedName("session_name")
    String sessionName;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    AppUser user;

    public AppUser getUser() {
        return user;
    }

    public void setUser(AppUser user) {
        this.user = user;
    }
}
