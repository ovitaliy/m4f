package com.uran.magazines4free.api.requests;

import com.uran.magazines4free.Constants;
import com.uran.magazines4free.model.Magazine;

/**
 * Created by ovitali on 10.04.2015.
 * Project is Magazines4Free
 */
public class SearchMagazinesRequest extends BaseGetRequest<Magazine[]> {


    public SearchMagazinesRequest() {
        super(Magazine[].class);
        setLimit(Constants.LIMIT);
        setOffset(0);
    }
    public SearchMagazinesRequest(String query) {
        this();
        setQuery(query);
    }

    public SearchMagazinesRequest setOffset(int offset) {
        mParams.put("offset", offset);
        return this;
    }

    public SearchMagazinesRequest setQuery(String query) {
        mParams.put("search_string", query);
        return this;
    }

    /**
     * Sets required magazines count. Default is value @link Constants.LIMIT
     *
     * @param limit
     * @return
     */
    public SearchMagazinesRequest setLimit(int limit) {
        mParams.put("limit", limit);
        return this;
    }

    @Override
    protected String getUrl() {
        return Constants.REQUESTS.SEARCH_MAGAZINES_LIST;
    }
}
