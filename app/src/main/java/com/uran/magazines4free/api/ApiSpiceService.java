package com.uran.magazines4free.api;

import android.util.Log;

import com.google.api.client.http.apache.ApacheHttpTransport;
import com.octo.android.robospice.GsonGoogleHttpClientSpiceService;

public class ApiSpiceService extends GsonGoogleHttpClientSpiceService {
    @Override
    public void onCreate() {
        super.onCreate();

        roboguice.util.temp.Ln.getConfig().setLoggingLevel(Log.ERROR);

        httpRequestFactory = new ApacheHttpTransport().createRequestFactory();
    }

   /* @Override
    public RestTemplate createRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        //find more complete examples in RoboSpice Motivation app
        //to enable Gzip compression and setting request timeouts.

        // web services support json responses
        StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter();
        GsonHttpMessageConverter gsonHttpMessageConverter = new GsonHttpMessageConverter();

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AppUser.class, new AppUser.Deserializer());
        gsonHttpMessageConverter.setGson(gsonBuilder.create());

        final List<HttpMessageConverter<?>> listHttpMessageConverters = restTemplate.getMessageConverters();

        listHttpMessageConverters.add(new FormHttpMessageConverter());
        listHttpMessageConverters.add(stringHttpMessageConverter);
        listHttpMessageConverters.add(gsonHttpMessageConverter);

        restTemplate.setMessageConverters(listHttpMessageConverters);

        restTemplate.setErrorHandler(new ErrorHandler());

        return restTemplate;
    }*/


    /*private class ErrorHandler extends DefaultResponseErrorHandler {

        @Override
        public boolean hasError(ClientHttpResponse response) throws IOException {
            return super.hasError(response);
        }

        @Override
        public void handleError(ClientHttpResponse response) throws IOException {
            String result = IOUtils.toString(response.getBody());
            Context context = MagazineApplication.getInstance();

            if (result != null)
                Log.e("ApiSpiceService", "handleError: " + result);

            try {
                JSONObject responseJson = new JSONObject(result);
                if (responseJson.has("error")) {
                    List<Integer> errorList = new ArrayList<>(1);
                    String packageName = context.getPackageName();
                    JSONArray errors = responseJson.getJSONArray("error");
                    for (int i = 0; i < errors.length(); i++) {
                        String error = "error_" + errors.getString(i);
                        int errorId = context.getResources().getIdentifier(error, "string", packageName);
                        if (errorId > 0)
                            errorList.add(errorId);
                    }

                    throw new ApiException(response.getStatusCode(), errorList);
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }

            super.handleError(response);

        }
    }

*/
}
