package com.uran.magazines4free.api.responses;

/**
 * Created by ovitali on 13.03.2015.
 */
public class ResponseObject<T> {

    private int errorCode;
    private T response;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }
}
