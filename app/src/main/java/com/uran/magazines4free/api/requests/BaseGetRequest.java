package com.uran.magazines4free.api.requests;

import android.text.TextUtils;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;

import org.apache.commons.io.IOUtils;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by ovitali on 10.04.2015.
 * Project is Magazines4Free
 */
public abstract class BaseGetRequest<T> extends BaseRequest<T> {
    protected BaseGetRequest(Class<T> clazz) {
        super(clazz);
    }

    @Override
    public T loadDataFromNetwork() throws Exception {
        String url = String.format("%s%s", getUrl(), createQueryParams(mParams));

        HttpRequest httpRequest = getHttpRequestFactory().buildGetRequest(getAbsoluteUrl(url));
        httpRequest.setHeaders(getHeaders());

        HttpResponse httpResponse = httpRequest.execute();

        String result = IOUtils.toString(httpResponse.getContent());
        T t = getGson().fromJson(result, getResultType());

        return t;
    }


    public static String createQueryParams(HashMap<String, Object> params) {
        Set<String> keys = params.keySet();
        ArrayList<String> paramList = new ArrayList<>(params.size());
        for (String key : keys) {
            try {
                paramList.add(key + "=" + URLEncoder.encode(params.get(key).toString(), "UTF-8"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        String resultParams = "";
        if (paramList.size() > 0) {
            resultParams = "?" + TextUtils.join("&", paramList);
        }
        return resultParams;
    }
}
