package com.uran.magazines4free.api.requests;

import android.view.View;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.UrlEncodedContent;
import com.google.gson.Gson;
import com.uran.magazines4free.Constants;

import org.apache.commons.io.IOUtils;

/**
 * Created by ovitali on 10.04.2015.
 * Project is Magazines4Free
 */
public class EditProfileRequest extends BaseSaveProfileRequest<Object> {
    private int mUid;

    public EditProfileRequest(int uid, View v) {
        super(Object.class, v);
        mUid = uid;
    }

    @Override
    public Object loadDataFromNetwork() throws Exception {
        UrlEncodedContent urlEncodedContent = new UrlEncodedContent(mParams);

        HttpRequest httpRequest = getHttpRequestFactory().buildPutRequest(getAbsoluteUrl(getUrl() + mUid), urlEncodedContent);
        httpRequest.setHeaders(getHeaders());


        HttpResponse httpResponse = httpRequest.execute();

        String result = IOUtils.toString(httpResponse.getContent());
        Object o = new Gson().fromJson(result, getResultType());
        return o;
    }

    @Override
    protected String getUrl() {
        return Constants.REQUESTS.EDIT_PROFILE;
    }
}
