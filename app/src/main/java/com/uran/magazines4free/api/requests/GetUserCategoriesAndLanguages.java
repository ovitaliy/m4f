package com.uran.magazines4free.api.requests;

import com.uran.magazines4free.Constants;
import com.uran.magazines4free.dataSource.DatabaseManager;
import com.uran.magazines4free.dataSource.dao.DaoInterest;
import com.uran.magazines4free.dataSource.dao.DaoLanguage;
import com.uran.magazines4free.model.Interest;
import com.uran.magazines4free.model.Language;
import com.uran.magazines4free.model.UserLanguagesAndCategories;

import java.util.List;

/**
 * Created by ovitali on 20.04.2015.
 * Project is Magazines4Free
 */
public class GetUserCategoriesAndLanguages extends BaseGetRequest<UserLanguagesAndCategories> {
    public GetUserCategoriesAndLanguages() {
        super(UserLanguagesAndCategories.class);
    }

    public GetUserCategoriesAndLanguages setId(int uid) {
        mParams.put("uid", uid);
        return this;
    }

    @Override
    public UserLanguagesAndCategories loadDataFromNetwork() throws Exception {
        UserLanguagesAndCategories userLanguagesAndCategories = super.loadDataFromNetwork();

        List<Integer> mSelectedInterest = userLanguagesAndCategories.getCategories();
        if (mSelectedInterest != null && mSelectedInterest.size() > 0) {
            DaoInterest daoInterest = DatabaseManager.getInstance().getHelper().getInterestDao();
            List<Interest> interests = daoInterest.queryForAll();

            for (Interest interest : interests) {
                interest.setSelected(mSelectedInterest.contains(interest.getId()));
                daoInterest.update(interest);
            }
        }

        List<Integer> mSelectedLanguages = userLanguagesAndCategories.getLanguages();
        if (mSelectedLanguages != null && mSelectedLanguages.size() > 0) {
            DaoLanguage daoLanguage = DatabaseManager.getInstance().getHelper().getLanguageDao();
            List<Language> languages = daoLanguage.queryForAll();


            for (Language language : languages) {
                language.setSelected(mSelectedLanguages.contains(language.getId()));
                daoLanguage.update(language);
            }
        }
        return userLanguagesAndCategories;
    }

    @Override
    protected String getUrl() {
        return Constants.REQUESTS.GET_USER_CATEGORIES_AND_LANGUAGE;
    }
}
