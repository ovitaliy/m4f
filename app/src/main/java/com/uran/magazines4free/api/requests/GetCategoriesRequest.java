package com.uran.magazines4free.api.requests;

import com.uran.magazines4free.Constants;
import com.uran.magazines4free.dataSource.DatabaseManager;
import com.uran.magazines4free.dataSource.dao.DaoCategory;
import com.uran.magazines4free.dataSource.dao.DaoInterest;
import com.uran.magazines4free.model.Category;
import com.uran.magazines4free.model.Interest;

import java.util.ArrayList;

/**
 * Created by ovitali on 11.03.2015.
 */
public class GetCategoriesRequest extends BaseTaxanomyRequest<Category[]> {

    @Override
    public ArrayList<String> getParameters() {
        ArrayList<String> parameters = new ArrayList<>(3);
        parameters.add("language=de");
        parameters.add("vid=" + String.valueOf(getTaxanomyId()));
        parameters.add("field=" + getFields());
        return parameters;
    }

    public GetCategoriesRequest() {
        super(Category[].class);
    }

    @Override
    public int getTaxanomyId() {
        return 2;
    }

    @Override
    public String getFields() {
        return "field_logo_term";
    }

    @Override
    public Category[] loadDataFromNetwork() throws Exception {
        Category[] list = super.loadDataFromNetwork();

        DaoCategory daoCategory = DatabaseManager.getInstance().getHelper().getCategoryDao();
        for (Category category : list) {
            daoCategory.createIfNotExists(category);
        }
        DaoInterest daoInterest = DatabaseManager.getInstance().getHelper().getInterestDao();

        for (Category category : list) {
            Interest interest = new Interest();
            interest.setId(category.getId());
            interest.setIcon(category.getIcon());
            interest.setName(category.getName());
            daoInterest.createIfNotExists(interest);
        }

        return list;
    }

    @Override

    public String getUrl() {
        return Constants.REQUESTS.TAXONOMY_CATEGORIES;
    }
}
