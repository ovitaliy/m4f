package com.uran.magazines4free.api.requests;

import android.text.TextUtils;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.json.gson.GsonFactory;

import java.util.ArrayList;

/**
 * Created by ovitali on 11.03.2015.
 */
public abstract class BaseTaxanomyRequest<T> extends BaseGetRequest<T> {
    public BaseTaxanomyRequest(Class<T> T) {
        super(T);
    }

    public abstract int getTaxanomyId();

    public abstract String getFields();

    public abstract ArrayList<String> getParameters();

    public abstract String getUrl();


    @Override
    public T loadDataFromNetwork() throws Exception {
        String url = String.format("%s?%s", getUrl(), TextUtils.join("&", getParameters()));

        HttpRequest httpRequest = getHttpRequestFactory().buildGetRequest(getAbsoluteUrl(url));
        httpRequest.setHeaders(getHeaders());
        httpRequest.setParser(new GsonFactory().createJsonObjectParser());

        HttpResponse httpResponse = httpRequest.execute();

        //String result = IOUtils.toString(httpResponse.getContent());
        T t = httpResponse.parseAs(getResultType());

        return t;
    }


}
