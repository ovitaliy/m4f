package com.uran.magazines4free.api.requests;

import com.uran.magazines4free.Constants;
import com.uran.magazines4free.model.Magazine;

/**
 * Created by ovitali on 10.04.2015.
 * Project is Magazines4Free
 */
public class GetMagazineRequest extends BaseGetRequest<Magazine> {


    public GetMagazineRequest(int magazineId) {
        super(Magazine.class);
        mParams.put("nid", magazineId);

    }

    @Override
    protected String getUrl() {
        return Constants.REQUESTS.GET_MAGAZINE;
    }
}
