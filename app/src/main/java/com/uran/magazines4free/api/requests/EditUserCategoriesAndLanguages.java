package com.uran.magazines4free.api.requests;

import android.text.TextUtils;

import com.uran.magazines4free.Constants;

import java.util.List;

/**
 * Created by ovitali on 20.04.2015.
 * Project is Magazines4Free
 */
public class EditUserCategoriesAndLanguages extends BasePutRequest<Object> {

    private int mUid;

    public EditUserCategoriesAndLanguages(int uid) {
        super(Object.class);
        mUid = uid;
    }

    /**
     * @param type interests or languages
     */
    public EditUserCategoriesAndLanguages setType(String type) {
        mParams.put("type", type);
        return this;
    }

    public EditUserCategoriesAndLanguages setIds(List<Integer> ids) {
        mParams.put("tid", TextUtils.join(",", ids));
        return this;
    }

    @Override
    protected String getUrl() {
        return Constants.REQUESTS.EDIT_USER_CATEGORIES_AND_LANGUAGES + mUid;
    }

}
