package com.uran.magazines4free.api.requests;

import com.uran.magazines4free.Constants;
import com.uran.magazines4free.model.Country;

import java.util.ArrayList;

/**
 * Created by ovitali on 11.03.2015.
 */
public class CountriesRequest extends BaseTaxanomyRequest<Country.CountryList> {

    public CountriesRequest() {
        super(Country.CountryList.class);
    }

    @Override
    public int getTaxanomyId() {
        return 4;
    }

    @Override
    public ArrayList<String> getParameters() {
        ArrayList<String> parameters = new ArrayList<>(3);
        parameters.add("vid=" + String.valueOf(getTaxanomyId()));
        parameters.add("fields=" + getFields());
        return parameters;
    }

    @Override
    public String getFields() {
        return "tid,name";
    }

    @Override
    public String getUrl() {
        return Constants.REQUESTS.TAXONOMY_COUNTRIES;
    }
}
