package com.uran.magazines4free.api.requests;

import android.util.Log;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by ovitali on 26.03.2015.
 */

public abstract class BaseRequestListener<T> implements RequestListener<T> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {

        Log.d(this.toString(), "spiceException", spiceException);

    }
}
