package com.uran.magazines4free.api.requests;

import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.UrlEncodedContent;
import com.uran.magazines4free.Constants;

import org.apache.commons.io.IOUtils;

/**
 * Created by ovitali on 13.03.2015.
 */
public class LogoutRequest extends BaseRequest<Boolean> {


    private String mSession;
    private String mToken;

    public LogoutRequest(String session, String token) {
        super(Boolean.class);

        mSession = session;
        mToken = token;
    }

    @Override
    public Boolean loadDataFromNetwork() throws Exception {

        UrlEncodedContent urlEncodedContent = new UrlEncodedContent(mParams);

        HttpRequest httpRequest = getHttpRequestFactory().buildPostRequest(getAbsoluteUrl(getUrl()), urlEncodedContent);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.put("X-CSRF-Token", mToken);
        httpHeaders.setCookie(mSession);

        httpRequest.setHeaders(httpHeaders);

        HttpResponse httpResponse = httpRequest.execute();

        String result = IOUtils.toString(httpResponse.getContent());
        Boolean t = getGson().fromJson(result, getResultType());

        return t;
    }

    @Override
    protected String getUrl() {
        return Constants.REQUESTS.LOGOUT;
    }
}
