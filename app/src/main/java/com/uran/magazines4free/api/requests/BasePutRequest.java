package com.uran.magazines4free.api.requests;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.UrlEncodedContent;
import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;

import java.util.HashMap;

/**
 * Created by ovitali on 19.05.2015.
 * Project is Magazines4Free
 */
public abstract class BasePutRequest<T> extends BaseRequest<T> {

    protected BasePutRequest(Class<T> clazz) {
        super(clazz);
    }

    @Override
    public T loadDataFromNetwork() throws Exception {
        UrlEncodedContent urlEncodedContent = new UrlEncodedContent(new HashMap<>(mParams));
        String url = getUrl() + BaseGetRequest.createQueryParams(mParams);
        HttpRequest httpRequest = getHttpRequestFactory().buildPutRequest(getAbsoluteUrl(url), urlEncodedContent);
        httpRequest.setHeaders(getHeaders());

        HttpResponse httpResponse = httpRequest.execute();

        String resultData = IOUtils.toString(httpResponse.getContent());
        T result = new Gson().fromJson(resultData, getResultType());

        return result;
    }
}
