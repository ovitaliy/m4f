package com.uran.magazines4free.api.requests;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;
import com.octo.android.robospice.retry.DefaultRetryPolicy;
import com.uran.magazines4free.Constants;
import com.uran.magazines4free.MagazineApplication;
import com.uran.magazines4free.model.AppUser;

import java.util.HashMap;

/**
 * Created by ovitali on 13.03.2015.
 */
public abstract class BaseRequest<T> extends GoogleHttpClientSpiceRequest<T> {

    protected HashMap<String, Object> mParams;


    protected BaseRequest(Class<T> clazz) {
        super(clazz);
        setRetryPolicy(new DefaultRetryPolicy(0, 1000, 1f));
        mParams = new HashMap<>();
    }

    public HttpHeaders getHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        if (MagazineApplication.AUTH_TOKEN != null) {
            httpHeaders.put("X-CSRF-Token", MagazineApplication.AUTH_TOKEN);
        }

        if (MagazineApplication.SESSION != null) {
            httpHeaders.setCookie(MagazineApplication.SESSION);
        }

        return httpHeaders;
    }

    protected abstract String getUrl();

    public GenericUrl getAbsoluteUrl(String url) {
        return new GenericUrl(Constants.REQUESTS.BASE_URL + url);
    }

    protected Gson getGson() {

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AppUser.class, new AppUser.Deserializer());

        return gsonBuilder.create();
    }

}
