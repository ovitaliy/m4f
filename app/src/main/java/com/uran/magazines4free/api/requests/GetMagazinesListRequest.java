package com.uran.magazines4free.api.requests;

import com.uran.magazines4free.Constants;
import com.uran.magazines4free.model.Magazine;
import com.uran.magazines4free.model.OrderType;

/**
 * Created by ovitali on 10.04.2015.
 * Project is Magazines4Free
 */
public class GetMagazinesListRequest extends BaseGetRequest<Magazine[]> {


    public GetMagazinesListRequest() {
        super(Magazine[].class);
        setLimit(Constants.LIMIT);
        setOffset(0);
        setOrder(OrderType.getOrder());
    }

    public GetMagazinesListRequest setCategory(int cid){
        mParams.put("tid", cid);
        return this;
    }

    public GetMagazinesListRequest setOffset(int offset){
        mParams.put("offset", offset);
        return this;
    }
    public GetMagazinesListRequest setOrder(String order){
        mParams.put("sort", order);
        return this;
    }

    /**
     * Sets required magazines count. Default is value @link Constants.LIMIT
     * @param limit
     * @return
     */
    public GetMagazinesListRequest setLimit(int limit){
        mParams.put("limit", limit);
        return this;
    }

    @Override
    protected String getUrl() {
        return Constants.REQUESTS.GET_MAGAZINES_LIST;
    }
}
