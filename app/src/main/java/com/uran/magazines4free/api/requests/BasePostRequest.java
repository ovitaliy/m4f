package com.uran.magazines4free.api.requests;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.UrlEncodedContent;

import org.apache.commons.io.IOUtils;

/**
 * Created by ovitali on 10.04.2015.
 * Project is Magazines4Free
 */
public abstract class BasePostRequest<T> extends BaseRequest<T> {
    protected BasePostRequest(Class<T> clazz) {
        super(clazz);
    }

    @Override
    public T loadDataFromNetwork() throws Exception {
        UrlEncodedContent urlEncodedContent = new UrlEncodedContent(mParams);

        HttpRequest httpRequest = getHttpRequestFactory().buildPostRequest(getAbsoluteUrl(getUrl()), urlEncodedContent);
        httpRequest.setHeaders(getHeaders());

        HttpResponse httpResponse = httpRequest.execute();

        String result = IOUtils.toString(httpResponse.getContent());
        T t = getGson().fromJson(result, getResultType());

        return t;
    }
}
