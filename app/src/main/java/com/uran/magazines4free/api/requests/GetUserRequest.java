package com.uran.magazines4free.api.requests;

import com.uran.magazines4free.Constants;
import com.uran.magazines4free.model.AppUser;

/**
 * Created by ovitali on 10.04.2015.
 * Project is Magazines4Free
 */
public class GetUserRequest extends BaseGetRequest<AppUser> {

    private int mUid;

    public GetUserRequest() {
        super(AppUser.class);
    }

    public GetUserRequest setUid(int uid) {
        //mParams.add("uid", uid);
        mUid = uid;
        return this;
    }


    @Override
    protected String getUrl() {
        return Constants.REQUESTS.GET_USER + mUid;
    }
}
