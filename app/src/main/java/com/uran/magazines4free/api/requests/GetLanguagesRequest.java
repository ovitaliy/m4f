package com.uran.magazines4free.api.requests;

import com.uran.magazines4free.Constants;
import com.uran.magazines4free.dataSource.DatabaseManager;
import com.uran.magazines4free.dataSource.dao.DaoLanguage;
import com.uran.magazines4free.model.Language;

import java.util.ArrayList;

/**
 * Created by ovitali on 11.03.2015.
 */
public class GetLanguagesRequest extends BaseTaxanomyRequest<Language[]> {

    @Override
    public ArrayList<String> getParameters() {
        ArrayList<String> parameters = new ArrayList<>(2);
        parameters.add("language=de");
        parameters.add("vid=" + String.valueOf(getTaxanomyId()));
        return parameters;
    }

    public GetLanguagesRequest() {
        super(Language[].class);
    }

    @Override
    public int getTaxanomyId() {
        return 5;
    }

    @Override
    public String getFields() {
        return "field_logo_term";
    }

    @Override
    public Language[] loadDataFromNetwork() throws Exception {
        Language[] list = super.loadDataFromNetwork();

        DaoLanguage daoLanguage = DatabaseManager.getInstance().getHelper().getLanguageDao();
        for (Language language : list) {
            daoLanguage.createIfNotExists(language);
        }

        return list;
    }

    @Override

    public String getUrl() {
        return Constants.REQUESTS.TAXONOMY_CATEGORIES;
    }
}
