package com.uran.magazines4free.behaviour;

import com.uran.magazines4free.model.AppUser;

public interface OnAuthListener {
    void onAuthComplete(AppUser appUser);
}
