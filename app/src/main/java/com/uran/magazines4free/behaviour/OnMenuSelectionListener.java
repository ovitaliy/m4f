package com.uran.magazines4free.behaviour;

/**
 * Created by ovitali on 17.04.2015.
 * Project is Magazines4Free
 */
public interface OnMenuSelectionListener {

    void onMenuSelected(int id);
}
