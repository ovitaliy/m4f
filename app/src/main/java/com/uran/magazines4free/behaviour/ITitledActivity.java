package com.uran.magazines4free.behaviour;

import com.uran.magazines4free.model.MenuItem;

/**
 * Created by ovitali on 17.04.2015.
 * Project is Magazines4Free
 */
public interface ITitledActivity {


    /**
     * hied all text and shows logo image
     */
    void setTitle();

    void setTitle(CharSequence title);

    void setTitle(int title);

    void setTitle(int selectedPosition, MenuItem... items);
}
