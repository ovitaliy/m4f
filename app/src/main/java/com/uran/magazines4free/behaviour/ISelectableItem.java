package com.uran.magazines4free.behaviour;

/**
 * Created by ovitali on 20.04.2015.
 * Project is Magazines4Free
 */
public interface ISelectableItem {

    void setSelected(boolean selected);

    boolean isSelected();

}
