package com.uran.magazines4free.behaviour;

/**
 * Created by ovitali on 12.05.2015.
 * Project is Magazines4Free
 */
public interface OnCategorySelectListener {
    public void onSelectCategory(int id, String name);
}
