package com.uran.magazines4free.behaviour;

/**
 * Created by ovitali on 25.05.2015.
 * Project is Magazines4Free
 */
public interface OnOrderChangeListener {
    void onOrderChanged(String order);
}
