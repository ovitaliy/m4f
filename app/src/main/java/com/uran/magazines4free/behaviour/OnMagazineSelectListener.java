package com.uran.magazines4free.behaviour;

import com.uran.magazines4free.model.Magazine;

/**
 * Created by ovitali on 19.02.2015.
 */
public interface OnMagazineSelectListener {
    void onMagazineSelected(Magazine magazine);
    void onViewMagazineSelected(Magazine magazine);
}
