package com.uran.magazines4free.behaviour;

import android.graphics.Bitmap;
import android.view.View;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * Created by ovitali on 27.05.2015.
 * Project is Magazines4Free
 */
public abstract class DefaultImageLoadingListener implements ImageLoadingListener{
    @Override
    public void onLoadingStarted(String imageUri, View view) {
    }

    @Override
    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
    }


    @Override
    public void onLoadingCancelled(String imageUri, View view) {
    }
}
