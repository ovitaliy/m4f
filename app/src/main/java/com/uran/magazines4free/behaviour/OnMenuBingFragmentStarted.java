package com.uran.magazines4free.behaviour;

/**
 * Created by ovitali on 26.05.2015.
 * Project is Magazines4Free
 */

public class OnMenuBingFragmentStarted {

    public OnMenuBingFragmentStarted(int itemId) {
        this.itemId = itemId;
    }

    public int getItemId() {
        return itemId;
    }

    int itemId;

}
