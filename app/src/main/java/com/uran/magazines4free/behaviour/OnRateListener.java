package com.uran.magazines4free.behaviour;

/**
 * Created by ovitali on 19.05.2015.
 * Project is Magazines4Free
 */
public interface OnRateListener {

    void onRate(int value);

}
