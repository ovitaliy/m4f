package com.uran.magazines4free.behaviour;

import android.widget.SeekBar;

/**
 * Created by ovitali on 22.04.2015.
 * Project is Magazines4Free
 * <p/>
 * Overrides all interface methods with empty bodies. Eliminates the need to override unused events. Keeps code cleaner.
 */
public class DefaultOnSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
